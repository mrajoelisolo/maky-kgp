package lwcomponents.canvas;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.BasicStroke;

/**
 *
 * @author FireWolf
 */
public class LWEllipse extends LWShape {
    private Rectangle bounds;
    private Color brushColor = Color.BLUE;
    private boolean isFilled = true;
    
    public LWEllipse(Point pos, Dimension dim) {   
        bounds = new Rectangle(pos, dim);
    }
    
    public LWEllipse(int x, int y, int width, int height) {
        bounds = new Rectangle(new Point(x, y), new Dimension(width, height));
    }
    
    public void draw(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        
        if(isFilled) {
            g2.setColor(brushColor);
            g2.fillOval(bounds.x, bounds.y, bounds.width, bounds.height);
        }
        
        g2.setColor(penColor);
        g2.setStroke(new BasicStroke(penWidth));
        g2.drawOval(bounds.x, bounds.y, bounds.width, bounds.height);
    }
    
    /**
     * Obtenir le cadre entourant la forme
     * @return
     * java.awt.Rectangle
     */
    public Rectangle getBounds() {
        return (Rectangle) bounds.clone();
    }
    
    /**
     * Redéfinir le cadre qui entoure la forme
     * La modification de ce cadre modifie également
     * la position et les dimensions de la forme
     * @param bounds
     * java.awt.Rectangle
     */
    public void setBounds(Rectangle bounds) {
        this.bounds = bounds;
    }
    
    /**
     * Définir la couleur de remplissage de la forme
     * @param brushColor
     * Couleur de remplissage de type java.awt.Color
     */
    public void setBrushColor(Color brushColor) {
        this.brushColor = brushColor;
    }
    
    /**
     * Active ou désactive le remplissage de la forme
     * @param isFilled
     * true - la forme est remplie
     * false - la forme n'est pas remplie
     */
    public void setBrushMode(boolean isFilled) {
        this.isFilled = isFilled;
    }
}
