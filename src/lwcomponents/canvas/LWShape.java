package lwcomponents.canvas;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;

/**
 *
 * @author FireWolf
 */
public abstract class LWShape {
    protected Color penColor = Color.BLACK;
    protected int penWidth = 1;
    
    public abstract void draw(Graphics g);
    
    /**
     * Définir la couleur du contourage
     * @param penColor
     * Couleur du contour de type java.awt.Color
     */
    public void setPenColor(Color penColor) {
        this.penColor = penColor;
    }
    
    /**
     * Définir l'épaisseur du contourage
     * @param penWidth
     * Epaisseur du contourage de type int
     */
    public void setPenWidth(int penWidth) {
        this.penWidth = penWidth;
    }
    
    public abstract Rectangle getBounds();
}
