package lwcomponents.canvas;

import java.awt.Point;

/**
 *
 * @author FireWolf
 */
public class LWPoint extends Point {
    public LWPoint(int x, int y) {
        this.x = x;
        this.y = y;
    }
    
    public void setPos(int x, int y) {
        this.x = x;
        this.y = y;
    }
}
