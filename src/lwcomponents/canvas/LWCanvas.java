package lwcomponents.canvas;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.List;
import java.util.LinkedList;
import java.util.Iterator;
import javax.swing.JPanel;
import java.awt.RenderingHints;
import java.util.ArrayList;

/**
 *
 * @author FireWolf
 */
public class LWCanvas extends JPanel {
    private LWPoint origin = new LWPoint(0, 0);
    
    protected List shapes = new LinkedList();
    
    protected int renderQuality = 0;
    public static int RENDER_LOW_QUALITY = 0;
    public static int RENDER_HIGH_QUALITY = 1;
    
    protected int antialiasingMode = 0;
    public static int ANTIALIASING_OFF = 0;
    public static int ANTIALIASING_ON = 1;
    
    public LWCanvas() {
        super();
    }
     
    /**
     * Insérer le canevas dans un container
     * @return
     * javax.swing.JPanel
     */
    public JPanel getCanvas() {
        super.setBounds(0, 0, 200, 100);
        return this;
    }
    
    /**
     * Insérer le canevas dans un container
     * @return
     * javax.swing.JPanel
     */
    public JPanel getCanvas(int x, int y, int wdt, int hgt) {
        super.setBounds(x, y, wdt, hgt);
        return this;
    }
    
    /**
     * Insérer le canevas dans un JPanel
     * @return
     * javax.swing.JPanel
     */
    public JPanel getCanvas(JPanel panel) {
        Rectangle rect = panel.getBounds();
        super.setBounds(1, 1, rect.width - 1, rect.height - 1);
        return this;
    }
    
    public void setCanvasBounds(Rectangle bounds) {
        super.setBounds(bounds);
    }
    
    @Override
    public void paint(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        
        //Définir la qualité du rendu
        if(renderQuality == RENDER_LOW_QUALITY )
            g2.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        else if(renderQuality == RENDER_HIGH_QUALITY)
            g2.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_SPEED);
        else
            g2.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_DEFAULT);
        
        //Définir le mode antialiasing
        if(antialiasingMode == 0) {
            g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);
            g2.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_OFF);
        }
        else if(antialiasingMode == 1) {
            g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            g2.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        }
        else {
            g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_DEFAULT);
            g2.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_DEFAULT);
        }
        
        //g2.setStroke(new BasicStroke(1, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
        
        //Déssiner d'abord le fond
        paintBackground(g);
        
        //super.paint(g);
        
        //Déssine les formes
        for(Iterator iter = shapes.iterator(); iter.hasNext();) {
            LWShape sh = (LWShape) iter.next();
            sh.draw(g2);
        }
    }
    
    /**
     * Déssiner le fond du canevas; Cette méthode ne contient aucune
     * implémentation par défaut, il faut donc le redéfinir
     * @param g
     * Contexte graphique
     */
    public void paintBackground(Graphics g) {}
    
    /**
     * Ajouter une forme dans le canevas
     * @param sh
     * Objet héritant de la classe LWShape
     * ex: LWRectangle, LWEllipse, LWPolygon, LWPolyline...
     */
    public void addShape(LWShape sh) {
        shapes.add(sh);
        repaint();
    }
    
    /**
     * Enlever une forme du canevas
     * @param sh
     * Objet héritant de la classe LWShape
     * ex: LWRectangle, LWEllipse, LWPolygon, LWPolyline...
     */
    public void removeShape(LWShape sh) {
        shapes.remove(sh);
        repaint();
    }
    
    /**
     * Vider le canevas
     */
    public void clearShapes() {
        shapes.clear();
        
        /*Dimension d = this.getSize();
        super.setSize(0, 0);
        super.setSize(d);*/
        
        super.repaint();
    }
    
    /**
     * Sélectionner une forme pointé par les coordonnées x, y du point p
     * @param p
     * Instance de type java.awt.Point
     */
    public List findShapes(Point p) {
        List l = new ArrayList();
        
        for(Iterator iter = shapes.iterator(); iter.hasNext();) {
            LWShape element = (LWShape) iter.next();
            if(element.getBounds().contains(p)) {
                l.add(element);
            }
        }
        return l;
    }
    
    /**
     * Définir la qualité du rendu
     * @param value
     * Constante entier définie dans la classe LWCanvas
     * RENDER_HIGH_QUALITY - améliore la qualité au détriment de la vitesse de rendu
     * RENDER_LOW_QUALITY - améliore la vitesse de rendu au détriment de la qualité
     */
    public void setRenderQuality(int value) {
        renderQuality = value;
    }
    
    /**
     * Active ou non le mode antialiasing ou effet escalier
     * @param value
     * Constante entier définie dans la classe LWCanvas
     * ANTIALIASING_OFF - désactive le mode antialiasing
     * ANTIALIASINT_ON - active le mode antialiasing
     * 
     */
    public void setAntialiasing(int value) {
        antialiasingMode  = value;
    }
    
    @Override
    public void setBackground(Color bg) {
        super.setBackground(bg);
    }
    
    @Override
    public void setBounds(int x, int y, int width, int height) {
        super.setBounds(x, y, width, height);
    }
    
    /**
     * Définit la position de l'origine du canevas
     * @param origin
     */
    public void setOrigin(int x, int y) {
        this.origin.x = x;
        this.origin.y = y;
        this.setBounds(x, y, this.getBounds().width, this.getBounds().height);
        this.repaint();
    }
    
    /**
     * Obtenir la position de l'origine du canevas
     * @return
     */
    public LWPoint getOrigin() {
        return origin;
    }
}