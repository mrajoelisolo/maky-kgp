package lwcomponents.canvas;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.BasicStroke;
import java.awt.Dimension;
import java.awt.Point;

/**
 *
 * @author FireWolf
 */
public class LWPolyline extends LWShape {
    private Rectangle bounds;
    private int[] xTab;
    private int[] yTab;
    private int nCount;
    
    public LWPolyline(LWPoint[] pts) {   
        decomposeNodes(pts);
        setBounds(pts);
    }
    
    //Décompose le tableau de points en tableau d'abscisses et d'ordonnées
    private void decomposeNodes(LWPoint[] pts) {
        nCount = pts.length;
        xTab = new int[nCount];
        yTab = new int[nCount];
        
        for(int i = 0; i < nCount; i++) {
            xTab[i] = pts[i].x;
            yTab[i] = pts[i].y;
        }
    }
    
    //Définir le cadre entourant la forme à partir des noeuds
    private void setBounds(LWPoint[] pts) {
        int xMin = pts[0].x, xMax = pts[0].x;
        int yMin = pts[0].y, yMax = pts[0].y;
        
        for(int i = 1; i < pts.length; i++) {
            if(xMin > pts[i].x) xMin = pts[i].x;
            if(xMax < pts[i].x) xMax = pts[i].x;
            
            if(yMin > pts[i].y) yMin = pts[i].y;
            if(yMax < pts[i].y) yMax = pts[i].y;
        }
        
        Rectangle res = new Rectangle(new Point(xMin, xMin), new Dimension(xMax - xMin, yMax - yMin));
        bounds = res;
    }
    
    public void draw(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        
        g2.setColor(penColor);
        g2.setStroke(new BasicStroke(penWidth));
        g2.drawPolyline(xTab, yTab, nCount);
    }
    
    /**
     * Obtenir le cadre entourant la forme
     * @return
     * java.awt.Rectangle
     */
    public Rectangle getBounds() {
        return (Rectangle) bounds.clone();
    }
    
    /**
     * Définir les noeuds qui constituent la forme
     * @param pts
     * Tableau de points de type LWPoint[]
     */
    public void setNodes(LWPoint[] pts) {
        decomposeNodes(pts);
        setBounds(pts);
    }
}
