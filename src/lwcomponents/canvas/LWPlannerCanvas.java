package lwcomponents.canvas;

import lwcomponents.util.planner.*;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import mapping.Tache;
import utile.ConversionDate;

/**
 *
 * @author FireWolf
 */
public class LWPlannerCanvas extends LWCanvas {
    private static final int H_ADJUST = 0;
    private static int Y_POS = 0;
    private List taches;
    private List tachesBar = new LinkedList();
    private Date dateDebut;
    
    /**
     * Constructeur, construit une canvas ayant un fond de calendrier
     * @param dateDebut
     * Date de début
     */
    public LWPlannerCanvas(Date dateDebut) {
        setDateDebut(dateDebut);
    }
    
    public LWPlannerCanvas(Date dateDebut, List taches) {
        setDateDebut(dateDebut);
        this.taches = taches;
        
        for(Iterator iter = taches.iterator(); iter.hasNext();) {
            Tache t = (Tache) iter.next();
            
            LWTaskBar tb = new LWTaskBar(0, 0, 0, 0, t.getNomTache(), t);
            tachesBar.add(tb);
        }
    }
    
    /**
     * Redéfinition de la fonction qui déssine le fond du canevas
     * @param g
     */
    @Override
    public void paintBackground(Graphics g) {
        LWPlanner.dessinerCalendrier(this, g,getDateDebut());
        
        //On déssine les tâches après
        dessinerTaches(g);
    }

    
    /**
     * Obtenir la date de début du planning
     * @return
     */
    public Date getDateDebut() {
        return dateDebut;
    }

    /**
     * Définir la date de début du planning
     * @param dateDebut
     */
    public void setDateDebut(Date dateDebut) {
        Calendar cld = Calendar.getInstance();
        cld.setTime(dateDebut);
        cld.set(Calendar.HOUR, H_ADJUST);
        cld.set(Calendar.MINUTE, 0);
        cld.set(Calendar.SECOND, 0);
        
        this.dateDebut = cld.getTime();
    }
    
    private void dessinerTaches(Graphics g) {
        int yPos = LWPlanner.DHEIGHT * 2 + 5 + Y_POS;
        int hTache = LWPlanner.DHEIGHT;
        
        for(Iterator iter = tachesBar.iterator(); iter.hasNext();) {
            LWTaskBar tacheBar = (LWTaskBar) iter.next();
            Tache t = tacheBar.getTache();
                //On effectue un décalage de 17 heures pour corriger le décalage graphique
            Calendar cld1 = Calendar.getInstance();
            Calendar cld2 = Calendar.getInstance();
            cld1.setTime(t.getDebutTache());
            cld1.set(Calendar.HOUR, H_ADJUST);
            cld1.set(Calendar.MINUTE, 0);
            cld1.set(Calendar.SECOND, 0);
            cld2.setTime(t.getFinTache());
            cld2.set(Calendar.HOUR, H_ADJUST);
            cld2.set(Calendar.MINUTE, 0);
            cld2.set(Calendar.SECOND, 0);
       
            int xTache = ConversionDate.nbJours(dateDebut, cld1.getTime(), LWPlanner.SPACING);
            int lTache = ConversionDate.nbJours(cld1.getTime(), cld2.getTime(), LWPlanner.SPACING);
            
            tacheBar.setBounds(xTache, yPos, lTache, hTache); //Modifie les dimensions
            
            //System.out.println("DEBUT :" + dateDebut + " DE " + cld1.getTime() + " A " + cld2.getTime());
            
            //Déssine la barre
            if((yPos >= LWPlanner.DHEIGHT * 2 + 5) && (yPos < this.getBounds().height)) {
                tacheBar.draw(g); 
            }
            
            yPos += hTache + 5;
        }
    }

    public List getTaches() {
        return taches;
    }

    public void setTaches(List taches) {
        this.taches = taches;
        tachesBar.clear();
        
        for(Iterator iter = taches.iterator(); iter.hasNext();) {
            Tache t = (Tache) iter.next();
            LWTaskBar tb = new LWTaskBar(0, 0, 0, 0, t.getNomTache(), t);
            tachesBar.add(tb);
        }
    }
    
    public static int getYPos() {
        return Y_POS;
    }
    
    public static void setYPos(int yPos) {
        Y_POS = yPos;
    }
    
    /**
     * Descendre l'affichage
     */
    public void scrollUp() {
        Y_POS += LWPlanner.DHEIGHT;
        this.repaint();
    }
    
    /**
     * Monter l'affichage
     */
    public void scrollDown() {
        Y_POS -= LWPlanner.DHEIGHT;
        this.repaint();
    }
    
    /**
     * @since 29/12/08
     * Permet d'atteindre un certain emplacement du planning à partir d'une date
     * @param date
     * Instance de la classe java.util.Date
     */
    public void moveTo(Date date) {
        setDateDebut(date);
        repaint();
    }
}
