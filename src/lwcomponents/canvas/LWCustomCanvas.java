package lwcomponents.canvas;

import java.awt.Graphics;
import java.awt.Rectangle;
import java.util.LinkedList;
import java.util.List;
import javax.swing.JPanel;

/**
 *
 * @author FireWolf
 */ 

public abstract class LWCustomCanvas extends JPanel {
    private List tasks = new LinkedList();
    
    public JPanel getCanvas(Rectangle bounds) {
        super.setBounds(0, 0, bounds.width, bounds.height);
        return this;
    }
    
    @Override
    public abstract void paint(Graphics g);
}
