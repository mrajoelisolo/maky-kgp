/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package lwcomponents.treetable;

import java.util.Vector;

/**
 *
 * @author FireWolf
 */
public class LWColumn {
    public static final int TEXTE = 0;
    public static final int COMBO = 1;
    public static final int CHECKBOX = 2;

    private String headerText;
    private String nom = "";
    private boolean editable = true;
    private int width = 100;
    private int columnType = 0;
    private Vector<String> listCombo;
    private Class classType = String.class;

    
    public String getHeaderText() {
        return headerText;
    }

    public void setHeaderText(String alias) {
        this.headerText = alias;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public boolean isEditable() {
        return editable;
    }

    public void setEditable(boolean editable) {
        this.editable = editable;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getColumnType() {
        return columnType;
    }

    public void setColumnType(int columnType) {
        this.columnType = columnType;
    }

    public Vector<String> getListCombo() {
        return listCombo;
    }

    public void setListCombo(Vector<String> listCombo) {
        if(columnType == COMBO){
            this.listCombo = listCombo;
        }
    }

    public Class getClassType() {
        return classType;
    }

    public void setClassType(Class classType) {
        this.classType = classType;
    }
}
