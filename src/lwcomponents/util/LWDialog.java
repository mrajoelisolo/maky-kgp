/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package lwcomponents.util;

import java.awt.Frame;
import java.awt.Graphics;
import javax.swing.JDialog;
import lwcomponents.canvas.LWCanvas;

/**
 *
 * @author FireWolf
 */
public class LWDialog extends JDialog{
    private LWCanvas canvas;
    
    public LWDialog(Frame owner, String titre) {
        super(owner, true);
        setTitle(titre);
        canvas = new LWCanvas() {
            @Override
            public void paintBackground(Graphics g) {
                drawBackground(g);
            }
        };
        add(canvas);
        this.setBounds(100, 100, 640, 480);
    }
    
    @Override
    public void setBounds(int x, int y, int wdt, int hgt) {
        super.setBounds(x, y, wdt, hgt);
        canvas.setBounds(0, 0, wdt, hgt);
    }
    
    public LWCanvas getCanvas() {
        return canvas;
    }
    
    public void drawBackground(Graphics g) {
    }
}
