/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package lwcomponents.util.planner;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.util.Calendar;
import java.util.Date;
import javax.swing.JPanel;
import utile.CalendarEngine;
import lwcomponents.util.LWImgUtil;

/**
 *
 * @author FireWolf
 */
public abstract class LWPlanner {
    public static int SPACING = 30;
    public static int DHEIGHT = 15;
    public static int WDHEIGHT = 15;
    public static int FONTSIZE = 11;
    private static String[] sem = {"Lun", "Mar", "Mer", "Jeu", "Ven", "Sam", "Dim"};
    private static BufferedImage dCase = LWImgUtil.loadImage("img\\components\\planner\\dCase.jpg");
    private static BufferedImage wdCase = LWImgUtil.loadImage("img\\components\\planner\\wdCase.jpg");
    private static BufferedImage wCase = LWImgUtil.loadImage("img\\components\\planner\\wCase.jpg");
    private static BufferedImage dBar = LWImgUtil.loadImage("img\\components\\planner\\dBar.jpg");    
    private static BufferedImage wdBar = LWImgUtil.loadImage("img\\components\\planner\\wdBar.jpg");    
    
    /**
     * Compte le nombre de jours restant avant la fin de la semaine
     * Si la date spécifié est un Lundi, alors il renvoie 7, si la
     * date spécifié est un Dimanche alors, il renvoie 0, ... etc
     * @param j
     * @param m
     * @param a
     * @return
     * Nombre de jours restants avant la fin de semaine
     */
    private static int nbJoursRestant(int j, int m, int a) {
        int iSem;
        
        Calendar cld = Calendar.getInstance();
        cld.set(a, m, j);
        iSem = cld.get(Calendar.DAY_OF_WEEK);      
        
        return (7 - iSem);
    }
    
    public static void dessinerCalendrier(JPanel container, Graphics g, Date debut) {
        Graphics2D g2 =(Graphics2D) g;
        
        int x = 0, compteur = 0;
       
        Font myFont = new Font("Arial", Font.BOLD, FONTSIZE);
        g2.setColor(new Color(0, 0, 0));
        g2.setFont(myFont);
        
        Calendar cld = Calendar.getInstance();
        cld.setTime(debut);
        
        int jour = cld.get(Calendar.DAY_OF_MONTH);
        int mois = cld.get(Calendar.MONTH);
        int annee = cld.get(Calendar.YEAR);
        
        while(x < container.getBounds().width) {
            //Si le jour est un weekend
            String nomMois = CalendarEngine.obtenirNomMoisRaccourci(mois);
            if(cld.get(Calendar.DAY_OF_WEEK) == 1) {
                if(compteur != 0) {
                    g2.drawImage(wCase, x, 0, SPACING * 7, DHEIGHT, null);
                    g2.drawRect(x, 0, SPACING * 7, DHEIGHT);
                }
                
                g2.drawString("" + (jour+1) + " " + nomMois + " " + annee, x + 5, FONTSIZE);
            }
            
            //Le début du calendrier
            if(compteur == 0) {
                int jRest = nbJoursRestant(jour, mois, annee) + 1;
                g2.drawImage(wCase, x, 0, jRest * SPACING, DHEIGHT, null);
                g2.drawString("" + jour + " " + nomMois + " " + annee, x + 5, FONTSIZE);
                g2.drawRect(x, 0, jRest * SPACING, DHEIGHT);
            }
            
            jour = cld.get(Calendar.DAY_OF_MONTH);
            mois = cld.get(Calendar.MONTH);
            annee = cld.get(Calendar.YEAR);
            int iSem = CalendarEngine.obtenirIndiceSem(jour, mois, annee);

            Rectangle b = container.getBounds();
            
            if(cld.get(Calendar.DAY_OF_WEEK) <= 5) {
                //En-tête jour
                g2.drawImage(dCase, x, DHEIGHT, SPACING, DHEIGHT, null);
                //Barre verticale
                g2.drawImage(dBar, x, 2 * DHEIGHT, SPACING, b.height - SPACING, null);
                Color c = g2.getColor();
                g2.setColor(new Color(0, 0, 0));
                g2.drawRect(x, 2 * DHEIGHT, SPACING, b.height - SPACING);
                g2.setColor(c);
            }
            else
            {
                //En-tête jour weekend
                g2.drawImage(wdCase, x, DHEIGHT, SPACING, DHEIGHT, null);
                
                //Barre verticale
                g2.drawImage(wdBar, x, 2 * DHEIGHT, SPACING, b.height - SPACING, null);
                Color c = g2.getColor();
                g2.setColor(new Color(0, 0, 0));
                g2.drawRect(x, 2 * DHEIGHT, SPACING, b.height - SPACING);
                g2.setColor(c);
            }

            g2.drawString(sem[iSem], x + 2, DHEIGHT + FONTSIZE);

             //Incrémentation  
            cld.add(Calendar.DAY_OF_MONTH, 1);
            x += SPACING;
            compteur++;
        }
        g2.drawRect(0, DHEIGHT, container.getBounds().width, DHEIGHT);
    }
}
