/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package lwcomponents.util.planner;

import java.awt.AlphaComposite;
import lwcomponents.util.*;
import lwcomponents.canvas.*;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import mapping.Tache;

/**
 * C'est un composant totalement graphique
 * @author Administrateur
 */
public class LWTaskBar extends LWShape {
    private Rectangle bounds;
    private boolean focusState = false;
    private Color gainedFocusColor = new Color(128, 255, 128);
    private Color lostFocusColor = new Color(64, 128, 64);
    private String caption;
    private LWMouseListener mouseListener;
    private int[] xTab;
    private int[] yTab;
    private int nCount;
    private Tache tache;
    private BufferedImage img;

    public LWTaskBar(int x, int y, int wdt, int hgt, String caption, Tache tache) {
        super();
        this.caption = caption;
        bounds = new Rectangle(new Point(x, y), new Dimension(wdt, hgt));
        this.tache = tache;
        initShape();
        
        if(tache.getNiveau() == 1)
            img = LWImgUtil.loadImage("img\\components\\planner\\bar01.jpg");
        else if(tache.getNiveau() == 2)
            img = LWImgUtil.loadImage("img\\components\\planner\\bar02.jpg");
        else if(tache.getNiveau() == 3)
            img = LWImgUtil.loadImage("img\\components\\planner\\bar03.jpg");
        else if(tache.getNiveau() == 4)
            img = LWImgUtil.loadImage("img\\components\\planner\\bar04.jpg");
        else if(tache.getNiveau() == 5)
            img = LWImgUtil.loadImage("img\\components\\planner\\bar05.jpg");
        else if(tache.getNiveau() >= 6)
            img = LWImgUtil.loadImage("img\\components\\planner\\bar06.jpg");
    }
    
    //Initialiser la forme de la barre
    private void initShape() {
        nCount = 4;
        
        xTab = new int[nCount];
        yTab = new int[nCount];
        
        xTab[0] = bounds.x; 
        yTab[0] = bounds.y;
        
        xTab[1] = bounds.x + bounds.width;
        yTab[1] = bounds.y;
        
        xTab[2] = bounds.x + bounds.width;
        yTab[2] = bounds.y + bounds.height;
        
        xTab[3] = bounds.x;
        yTab[3] = bounds.y + bounds.height;
    }
    
    @Override
    public void draw(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        
        if(focusState) {
            penWidth = 2;
            g2.setColor(gainedFocusColor);
        } else {
            penWidth = 1;
            g2.setColor(lostFocusColor);
        }
        
        g2.fillPolygon(xTab, yTab, nCount);
        g2.setColor(penColor);
        g2.setStroke(new BasicStroke(penWidth));
        g2.drawPolygon(xTab, yTab, nCount);
        
        g2.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.8F));
        
        g2.drawImage(img, bounds.x, bounds.y, bounds.width, bounds.height, null);
        
        Font myFont = new Font("Arial", Font.BOLD, 10);
        g2.setFont(myFont);
        g2.drawString(caption, bounds.x + 10, bounds.y + bounds.height / 2);
    }

    @Override
    public Rectangle getBounds() {
        return bounds;
    }
    
    /**
     * Définir l'emplacement de la barre dans le canevas
     * @param x - abscisse
     * @param y - ordonnée
     */
    public void setBounds(int x, int y, int wdt, int hgt) {
        bounds.x = x; bounds.y = y;
        bounds.width = wdt; bounds.height = hgt;
        initShape();
    }
    
    /**
     * Obtenir l'état de la barre c.a.d si elle gagne ou perd le focus
     * @return
     */
    public boolean getFocusState() {
        refreshListener();
        return focusState;
    }
    
    /**
     * Définir l'écouteur d'évenements de la barre
     * @param mouseListener - instance de la classe LWMouseListener
     */
    public void setMouseListener(LWMouseListener mouseListener) {
        this.mouseListener = mouseListener;
    } 
     
    public void refreshListener() {
        LWPoint pt = mouseListener.getCoords();
        
        if(bounds.contains(pt.x, pt.y)) {  
            focusState = true;        
        } else{
            focusState = false;
        }
    } 
    
    public Tache getTache() {
        return tache;
    }
    
    public void setTache(Tache tache) {
        this.tache = tache;
    }
}
