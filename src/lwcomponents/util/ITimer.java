package lwcomponents.util;

import javax.swing.Timer;

/**
 *
 * @author FireWolf
 */
public interface ITimer {
    public Timer createTimer();
    public void timerEvent();
}
