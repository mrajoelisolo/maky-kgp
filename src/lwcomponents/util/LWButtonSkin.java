package lwcomponents.util;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

/**
 *
 * @author FireWolf
 */
public class LWButtonSkin {
    private BufferedImage pressedBtnImg;
    private BufferedImage gainedFocusImg;
    private BufferedImage lostFocusImg;
    
    public LWButtonSkin(String root, String skin1, String skin2, String skin3) {
        String img1 = root + "\\" + skin1 + ".jpg";
        String img2 = root + "\\" + skin2 + ".jpg";
        String img3 = root + "\\" + skin3 + ".jpg";
        lostFocusImg = loadImage(img1);
        gainedFocusImg = loadImage(img2);
        pressedBtnImg = loadImage(img3);
    }
    
    public void modifySkin(String root, String skin1, String skin2, String skin3) {
        String img1 = root + "\\" + skin1 + ".jpg";
        String img2 = root + "\\" + skin2 + ".jpg";
        String img3 = root + "\\" + skin3 + ".jpg";
        lostFocusImg = loadImage(img1);
        gainedFocusImg = loadImage(img2);
        pressedBtnImg = loadImage(img3);
    }
    
    private static BufferedImage loadImage(String fileName) {
        BufferedImage img = null;
        
        try{
            img = ImageIO.read(new File(fileName));
            
        } catch(IOException e) {
            e.printStackTrace();
        }
        
        return img;
    }

    public BufferedImage getPressedBtnImg() {
        return pressedBtnImg;
    }

    public BufferedImage getGainedFocusImg() {
        return gainedFocusImg;
    }

    public BufferedImage getLostFocusImg() {
        return lostFocusImg;
    }
}
