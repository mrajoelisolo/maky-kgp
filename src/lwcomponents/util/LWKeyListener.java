package lwcomponents.util;

import java.awt.event.KeyEvent;
import lwcomponents.canvas.LWCanvas;

/**
 *
 * @author Administrateur
 */
public class LWKeyListener extends LWCanvasKeyAdapter {
    private StringBuffer typedString = new StringBuffer(255);
    
    public LWKeyListener(LWCanvas canvas) {
        super(canvas);
    }
    
    @Override
    public void keyPressed(KeyEvent e) {
        if(e.getKeyChar() != KeyEvent.CHAR_UNDEFINED)
            typedString.append(e.getKeyChar());
        System.out.println("You pressed a key");
    }
    
    public String getTypedString() {
        return typedString.toString();
    }
}
