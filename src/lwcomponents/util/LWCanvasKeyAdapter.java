package lwcomponents.util;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import lwcomponents.canvas.LWCanvas;

/**
 *
 * @author Administrateur
 */
public class LWCanvasKeyAdapter extends KeyAdapter {
    protected LWCanvas canvas;
    
    public LWCanvasKeyAdapter(LWCanvas canvas) {
        this.canvas = canvas;
        canvas.addKeyListener(this);
    }
    
    @Override
    public void keyPressed(KeyEvent e) {}
}
