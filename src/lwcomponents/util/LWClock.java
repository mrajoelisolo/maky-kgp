package lwcomponents.util;

import java.awt.AlphaComposite;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.geom.Ellipse2D;
import java.awt.image.BufferedImage;
import lwcomponents.canvas.LWEllipse;
import lwcomponents.canvas.LWShape;
import lwcomponents.canvas.LWPoint;

/**
 *
 * @author FireWolf
 * @since 14/12/2008
 * @version 1.00
 * Cette classe permet d'afficher l'heure sous forme d'une montre analogique
 */
public class LWClock extends LWShape {
    private int hour;
    private int minute;
    private int sec;
    private Rectangle bounds = new Rectangle(0, 0, 0, 0);
    private BufferedImage img = LWImgUtil.loadImage("img\\components\\Clock.bmp");
    private BufferedImage img2 = LWImgUtil.loadImage("img\\components\\Glass.bmp");
    private double minAngle;
    private double hAngle;
    private double secAngle;
    private LWPoint p1 = new LWPoint(0, 0);
    private LWPoint p2 = new LWPoint(0, 0);
    private LWPoint p3 = new LWPoint(0, 0);
    private int[] xPt = new int[3];
    private int[] yPt = new int[3];
    private LWPoint origin = new LWPoint(0, 0);
    
    public LWClock() {
        hour = 0;
        minute = 0;
        sec = 0;
        hAngle = getAngleHour(0);
        minAngle = getAngleMin(0);
        secAngle = getAngleSec(0);
    }
    
    public LWClock(int hour, int minute, int sec) {
        this.hour = hour % 24;
        this.minute = minute % 60;
        this.sec = sec % 60;
           
        hAngle = getAngleHour(this.hour);
        minAngle = getAngleMin(this.minute);
        secAngle = getAngleSec(this.sec);
    }
    
    public void setHour(int hour, int minute, int sec) {
        this.hour = hour % 24;
        this.minute = minute % 60;
        this.sec = sec % 60;
        
        hAngle = getAngleHour(this.hour);
        minAngle = getAngleMin(this.minute);
        secAngle = getAngleSec(this.sec);
    }
    
    public int getHour() {
        return hour;
    }
    
    public int getMinute() {
        return minute;
    }
    
    public int getSec() {
        return sec;
    }

    private double getAngleSec(int sec) {
        double res = 0;
        
        res = (2 * Math.PI / 60) * sec - Math.PI / 2;
        
        return res;
    }
    
    private double getAngleMin(int min) {
        double res = 0;
        
        res = (2 * Math.PI / 3600) * (min * 60 + sec) - Math.PI / 2;
        //res = 2 * Math.PI / 60 * min;
        
        return res;
    }
    
    private double getAngleHour(int hour) {
        double res = 0;
        
        res = (2 * Math.PI / 720) * (hour * 60 + minute) - Math.PI / 2;
        //res = 2 * Math.PI / 24 * hour;
        
        return res;
    }
    
    private void paintNeedles(Graphics g) {
        int r = 3 * bounds.width / 8;
        int d = bounds.width / 60;
        Graphics2D g2 = (Graphics2D) g;
        
        g2.setStroke(new BasicStroke(1));
        
        origin.x = (int) (bounds.x + bounds.width / 2);
        origin.y = (int) (bounds.y + bounds.height / 2);
        
        //Aiguille Heure
        d = bounds.width / 50;
        
        p1.x = origin.x;
        p1.y = origin.y;
        p1 = LWFct.angularTranslation(p1, 3*r/4, 0, hAngle);
        
        p2.x = origin.x;
        p2.y = origin.y;
        p2 = LWFct.angularTranslation(p2, -3*d/4, 3*d/4, hAngle);
        
        p3.x = origin.x;
        p3.y = origin.y;
        p3 = LWFct.angularTranslation(p3, -3*d/4, -3*d/4, hAngle);
        
        xPt[0] = p1.x; yPt[0] = p1.y;
        xPt[1] = p2.x; yPt[1] = p2.y;
        xPt[2] = p3.x; yPt[2] = p3.y;
        
        g2.setColor(new Color(0, 0, 153));
        g2.fillPolygon(xPt, yPt, 3);
        g2.setColor(new Color(153, 153, 255));
        g2.drawPolygon(xPt, yPt, 3);
        g2.drawLine(p1.x, p1.y, origin.x, origin.y);
        
        //Aiguille minute
        p1.x = origin.x;
        p1.y = origin.y;
        p1 = LWFct.angularTranslation(p1, r, 0, minAngle);
        
        p2.x = origin.x;
        p2.y = origin.y;
        p2 = LWFct.angularTranslation(p2, -3*d/6, 3*d/6, minAngle);
        
        p3.x = origin.x;
        p3.y = origin.y;
        p3 = LWFct.angularTranslation(p3, -3*d/4, -3*d/4, minAngle);
        
        xPt[0] = p1.x; yPt[0] = p1.y;
        xPt[1] = p2.x; yPt[1] = p2.y;
        xPt[2] = p3.x; yPt[2] = p3.y;
        
        g2.setColor(Color.DARK_GRAY);
        g2.fillPolygon(xPt, yPt, 3);
        g2.setColor(Color.BLACK);
        g2.drawPolygon(xPt, yPt, 3);
        g2.drawLine(p1.x, p1.y, origin.x, origin.y);
        
        //Aiguille secondes
        p1.x = origin.x; p1.y = origin.y;
        
        p2.x = origin.x; p2.y = origin.y;
        p2 = LWFct.angularTranslation(p2, r, 0, secAngle);
        
        g2.setColor(new Color(0, 0, 192));
        g2.drawLine(p1.x, p1.y, p2.x, p2.y);
    }
    
    @Override
    public void draw(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        g2.setColor(new Color(0, 0, 0));
        
        //Dessine le cadre et le cadran
        Ellipse2D b1 = new Ellipse2D.Float();
        b1.setFrame(bounds);
        g2.setClip(b1);
        
        g2.drawImage(img, bounds.x, bounds.y, bounds.width, bounds.height, null);
        
        paintNeedles(g2);
        
        //Dessine le contour
        LWEllipse el = new LWEllipse(bounds.x + 1, bounds.y + 1, bounds.width - 2, bounds.height - 2);
        el.setBrushMode(false);
        el.setPenColor(new Color(0, 0, 0));
        el.setPenWidth(1);
        el.draw(g2);
        
        //Dessine le glass
       /* g2.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.5F));
        Ellipse2D b2 = new Ellipse2D.Float();
        int vBorder = bounds.width / 13;
        b2.setFrame(bounds.x + vBorder, bounds.y + vBorder, bounds.width - 2*vBorder, bounds.height - 2*vBorder);
        g2.setClip(b2);
        g2.drawImage(img2, bounds.x + vBorder, bounds.y + vBorder, bounds.width - 2*vBorder, bounds.height - 2*vBorder, null);*/
    }
    
    @Override
    public Rectangle getBounds() {
        return (Rectangle) bounds.clone();
    }
    
    public void setBounds(int x, int y, int wdt, int hgt) {
        bounds.setBounds(x, y, wdt, hgt);
    }
}
