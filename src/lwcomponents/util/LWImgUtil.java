package lwcomponents.util;

import java.awt.AWTException;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.color.ColorSpace;
import java.awt.image.BufferedImage;
import java.awt.image.ColorConvertOp;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.JPanel;

/**
 * Cette classe permet de gérer les fichiers images de type JPG, PNG ou GIF.
 * Elle incorpore des méthodes permettant de charger des images et de les 
 * stocker dans des variables de type java.awt.BufferedImage. Outre elle peut
 * effectuer des modifications tels que la desaturation
 * @author Administrateur
 */
public class LWImgUtil {
    /**
     * Permet de désaturer l'image c'est-à-dire que l'image est transformé
     * en niveaux de gris
     * @param img
     * L'image a désaturer
     * @return
     * Retourne une copie de l'image mais desaturée
     */
    public static BufferedImage desaturate(BufferedImage img) {
        BufferedImage imgDest = img;
        
        ColorSpace gris = ColorSpace.getInstance(ColorSpace.CS_GRAY);
        ColorSpace couleur = ColorSpace.getInstance(ColorSpace.CS_sRGB);
        ColorConvertOp ccOp = new ColorConvertOp(couleur, gris, null);
        ccOp.filter(imgDest, imgDest);
        
        return imgDest;
    }
    
    /**
     * Permet de charger une image de type JPG, PNG ou GIF
     * @param fileName - le fichier à charger. Si vous ne spécifiez directement
     * le fichier sans préciser la racine ou le dossier où il se trouve, alors
     * l'emplacement par défaut sera le dossier où se trouve votre projet
     * @return
     * Une instance de type java.awt.BufferedImage contenant l'image
     */
    public static BufferedImage loadImage(String fileName) {
        BufferedImage img = null;
        
        try{
            img = ImageIO.read(new File(fileName));
            
        } catch(IOException e) {
            e.printStackTrace();
        }
        
        return img;
    }
    
    public static byte[] imgToBytes(BufferedImage img) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        try{
            ImageIO.write(img, "jpg", baos);
        } catch(IOException e) {
            e.printStackTrace();
        }
        return baos.toByteArray();
    }
    
  /*  public static BufferedImage bytesToImg(byte[] bytes)
            BufferedImage bufferedImage = new BufferedImage (1024, 1024, BufferedImage.TYPE_INT_RGB);
            File destImage = new File("img\\Hinata.jpg");
            ImageIO.setUseCache(false);
            bufferedImage = ImageIO.read (new ByteArrayInputStream(bytes));
            System.out.println("\n Before file write");
            ImageIO.write(bufferedImage, "JPEG", destImage); 
            
            try{
                
            }*/
    
    public static BufferedImage capturePanel(JPanel panel) {
        Robot robot;
        BufferedImage img = null;
        
        try {
            robot = new Robot();
            img = robot.createScreenCapture(panel.getBounds());
            
        } catch (AWTException ex) {
            Logger.getLogger(LWImgUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return img;
    }
}  
