package lwcomponents.util;

import lwcomponents.canvas.LWPoint;

/**
 *
 * @author FireWolf
 * @since 14/12/2008
 */
public abstract class LWFct {
    public static double TOLERANCE = 0.1f;
    
    public static LWPoint angularTranslation(LWPoint pt, int r1, int r2, double theta) {
        LWPoint res = new LWPoint(0, 0);
        
        res.x = (int) (pt.x + (r1 * Math.cos(theta) - r2 * Math.sin(theta)));
        res.y = (int) (pt.y + (r1 * Math.sin(theta) + r2 * Math.cos(theta)));
        
        return res;
    }
    
    public static double getDist(LWPoint p1, LWPoint p2) {
        double val = Math.sqrt(Math.pow((p1.x - p2.x), 2) + Math.pow(p1.y - p2.y, 2));
        
        return val;
    }
    
    public static void moveTo(LWPoint pt, LWPoint trg, int spd) {
        int dx = trg.x - pt.x;
        int dy = trg.y - pt.y;
        double vx = 0, vy = 0;
        double dist, tol = TOLERANCE;
        double xRes = pt.x, yRes = pt.y;
        
       dist = getDist(pt, trg);
       if(dist > spd) {
           if(Math.abs(dx) > tol) {
               vx = spd * dx / dist;
               xRes = xRes + vx;
               System.out.println("Moving hor, vx=" + vx);
           }
           if(Math.abs(dy) > tol) {
               vy = spd * dy / dist;
               yRes = yRes + vy;
               System.out.println("Moving ver, vy=" + vy);
           }
           pt.setPos((int)xRes, (int)yRes);
       }else{
           pt.setPos(trg.x, trg.y);
           System.out.println("Targeted to " + trg);
       }
    }
}
