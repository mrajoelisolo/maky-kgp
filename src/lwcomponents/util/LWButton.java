package lwcomponents.util;

import lwcomponents.canvas.*;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;

/**
 * C'est un composant totalement graphique
 * @author FireWolf
 */
public class LWButton extends LWShape {
    private Rectangle bounds;
    private boolean focusState = false;
    private Color gainedFocusColor = new Color(128, 128, 255);
    private Color lostFocusColor = new Color(64, 64, 128);
    private String caption;
    private LWMouseListener mouseListener;
    private int[] xTab;
    private int[] yTab;
    private int nCount;

    public LWButton(int x, int y, String caption) {
        super();
        this.caption = caption;
        bounds = new Rectangle(new Point(x, y), new Dimension(100, 30));
        initShape();
    }
    
    //Initialiser la forme du bouton
    private void initShape() {
        nCount = 5;
        
        xTab = new int[nCount];
        yTab = new int[nCount];
        
        xTab[0] = bounds.x; 
        yTab[0] = bounds.y;
        
        xTab[1] = bounds.x + bounds.width;
        yTab[1] = bounds.y;
        
        xTab[2] = bounds.x + bounds.width;
        yTab[2] = bounds.y + 3*bounds.height/4;
        
        xTab[3] = bounds.x + bounds.width - bounds.height/4;
        yTab[3] = bounds.y + bounds.height;
        
        xTab[4] = bounds.x;
        yTab[4] = bounds.y + bounds.height;
    }
    
    @Override
    public void draw(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        
        if(focusState) {
            penWidth = 2;
            g2.setColor(gainedFocusColor);
        } else {
            penWidth = 1;
            g2.setColor(lostFocusColor);
        }
        
        g2.fillPolygon(xTab, yTab, nCount);
        g2.setColor(penColor);
        g2.setStroke(new BasicStroke(penWidth));
        g2.drawPolygon(xTab, yTab, nCount);
        
        Font myFont = new Font("Arial", Font.BOLD, 15);
        g2.setFont(myFont);
        g2.drawString(caption, bounds.x + 10, bounds.y + bounds.height / 2);
    }

    @Override
    public Rectangle getBounds() {
        return bounds;
    }
    
    /**
     * Définir l'emplacement du bouton dans le canevas
     * @param x - abscisse
     * @param y - ordonnée
     */
    public void setBounds(int x, int y) {
        bounds.x = x; bounds.y = y;
        initShape();
    }
    
    /**
     * Obtenir l'état du bouton c.a.d si elle gagne ou perd le focus
     * @return
     */
    public boolean getFocusState() {
        refreshListener();
        return focusState;
    }
    
    /**
     * Définir l'écouteur d'évenements du bouton
     * @param mouseListener - instance de la classe LWMouseListener
     */
    public void setMouseListener(LWMouseListener mouseListener) {
        this.mouseListener = mouseListener;
    } 
     
    public void refreshListener() {
        LWPoint pt = mouseListener.getCoords();
        
        if(bounds.contains(pt.x, pt.y)) {  
            focusState = true;        
        } else{
            focusState = false;
        }
    } 
}
