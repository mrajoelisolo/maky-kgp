package lwcomponents.util;

import javax.swing.UIManager;

/**
 *
 * @author Administrateur
 */
public class MakeNimbusLookNFeel {
    public static void makeIt() {
        try {
            UIManager.setLookAndFeel(
            UIManager.getCrossPlatformLookAndFeelClassName());
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
        } catch (Exception e) { }
    }
}
