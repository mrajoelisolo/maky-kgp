/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package lwcomponents.util;

import java.awt.BasicStroke;
import java.awt.Color;
import lwcomponents.canvas.LWShape;
import lwcomponents.canvas.LWCanvas;
import java.awt.Rectangle;
import java.awt.Graphics;
import java.awt.Graphics2D;
import javax.swing.JPanel;

/**
 *
 * @author FireWolf
 * Cette fonctionnalité permet de dessiner une réticule dans le canevas
 */
public class LWReticle extends LWShape {
    private Rectangle bounds = new Rectangle();
    private int size = 100;
    private int focusSize = 100;
    
    public LWReticle() {
        bounds.setBounds(0, 0, size, size);
        penColor = Color.GREEN;
        penWidth = 2;
    }
    
    public void draw(Graphics g) {
        Graphics2D g2 = (Graphics2D) g; 
        
        g2.setColor(penColor);
        g2.setStroke(new BasicStroke(penWidth));
        
        //Partie interne
        g2.drawArc(bounds.x - size / 4, bounds.y - size / 4, size / 2, size / 2, 20, 50);
        g2.drawArc(bounds.x - size / 4, bounds.y - size / 4, size / 2, size / 2, 110, 50);
        g2.drawArc(bounds.x - size / 4, bounds.y - size / 4, size / 2, size / 2, 200, 50);
        g2.drawArc(bounds.x - size / 4, bounds.y - size / 4, size / 2, size / 2, 290, 50);
        g2.fillOval(bounds.x - size / 32, bounds.y - size / 32, size / 16, size / 16);
        
        //Partie externe
        g2.drawArc(bounds.x - size / 2, bounds.y - size / 2, size, size, 0, 270);
        
        g2.setStroke(new BasicStroke(penWidth * 3));
        int offset = penWidth * 2;
        g2.drawArc(bounds.x - size / 2, bounds.y - size / 2, focusSize - offset, focusSize - offset, 280, 70);
        
    }
    
    public void setBounds(int x, int y, int wdt, int hgt) {
        bounds.setBounds(x, y, wdt, hgt);
    }
    
    public void setPos(int x, int y) {
        bounds.setBounds(x, y, size, size);
    }
    
    public Rectangle getBounds() {
        return bounds;
    }
    
    public void setReticleSize(int size) {
        this.size = size;
    }
    
    public void setFocusSize(int focusSize) {
        this.focusSize = focusSize;
    }
    
    public int getFocusSize() {
        return focusSize;
    }
}
