package lwcomponents.util;

import lwcomponents.canvas.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.SwingUtilities;

/**
 *
 * @author FireWolf
 */
public abstract class LWCanvasMouseAdapter extends MouseAdapter {
    protected LWCanvas canvas;
    
    public LWCanvasMouseAdapter(LWCanvas canvas) {
        super();
        this.canvas = canvas;
        canvas.addMouseListener(this);
        canvas.addMouseMotionListener(this);
    }
    
    @Override
    public void mousePressed(MouseEvent e) {
        if(SwingUtilities.isLeftMouseButton(e))
            leftClickAction(e);
        else if(SwingUtilities.isRightMouseButton(e))
            rightClickAction(e);
    }

    protected abstract void leftClickAction(MouseEvent e);

    protected abstract void rightClickAction(MouseEvent e);
}
