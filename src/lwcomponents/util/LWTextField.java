package lwcomponents.util;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import lwcomponents.canvas.LWPoint;
import lwcomponents.canvas.LWShape;

/**
 *
 * @author FireWolf
 */
public class LWTextField extends LWShape {
    private Rectangle bounds;
    private boolean focusState = false;
    private Color gainedFocusColor = new Color(128, 128, 224);
    private Color lostFocusColor = new Color(64, 64, 96);
    private LWMouseListener mouseListener;
    private LWKeyListener keyListener;
    private boolean isTypeable = false;
    private String text = "";
    private String caption = "";
   
    public LWTextField(int x, int y) {
        super();
        bounds = new Rectangle(new Point(x, y), new Dimension(100, 30));
    }
    
    public LWTextField(int x, int y, String caption) {
        super();
        this.caption = caption;
        bounds = new Rectangle(new Point(x, y), new Dimension(100, 30));
    }
    
    @Override
    public void draw(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        
        if(focusState || isTypeable) {
            penWidth = 2;
            g2.setColor(gainedFocusColor);
        } else {
            penWidth = 1;
            g2.setColor(lostFocusColor);
        }
        
        g2.fillRect(bounds.x, bounds.y, bounds.width, bounds.height);
        g2.setColor(penColor);
        g2.setStroke(new BasicStroke(penWidth));
        g2.drawRect(bounds.x, bounds.y, bounds.width, bounds.height);
        
        Font myFont = new Font("Arial", Font.BOLD, 18);
        g2.setFont(myFont);
        g2.drawString(caption, bounds.x, bounds.y - bounds.height / 6);
        g2.setColor(Color.LIGHT_GRAY);
        g2.drawString(text, bounds.x + 10, bounds.y + 3 * bounds.height / 4);
    }

    @Override
    public Rectangle getBounds() {
        return bounds;
    }
    
    /**
     * Définir l'emplacement du bouton dans le canevas
     * @param x - abscisse
     * @param y - ordonnée
     */
    public void setBounds(int x, int y, int wdt, int hgt) {
        bounds.x = x; bounds.y = y;
        bounds.width = wdt; bounds.height = hgt;
    }
    
    /**
     * Obtenir l'état du bouton c.a.d si elle gagne ou perd le focus
     * @return
     */
    public boolean getFocusState() {
        refreshListener();
        return focusState;
    }
    
    /**
     * Définir l'écouteur d'évenements du bouton
     * @param mouseListener - instance de la classe LWMouseListener
     */
    public void setMouseListener(LWMouseListener mouseListener) {
        this.mouseListener = mouseListener;
    } 
     
    public void refreshListener() {
        LWPoint pt = mouseListener.getCoords();
        
        if(bounds.contains(pt.x, pt.y)) {  
            focusState = true;        
        } else{
            focusState = false;
        }
    } 
    
    public void setEnabled(boolean isTypeable) {
        this.isTypeable = isTypeable;
    }
    
    public boolean isTypeable() {
        return isTypeable;
    }
    
    public void setKeyListener(LWKeyListener keyListener) {
        this.keyListener = keyListener;
    }
    
    public String getText() {
        return text;
    }
    
    public void setText(String text) {
        this.text = text;
    }
    
    public void setCaption(String caption) {
        this.caption = caption;
    }
}
