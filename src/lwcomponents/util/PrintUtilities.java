package lwcomponents.util;

import java.awt.*;
import javax.swing.*;
import java.awt.print.*;

public class PrintUtilities implements Printable 
{

private Component infoprint;
/************************************************************/
    public static void printComponent(Component c) 
    {
        new PrintUtilities(c).print();
    }
/************************************************************/
    public PrintUtilities(Component infoprint) 
    {
        ha=infoprint.getHeight();
        this.infoprint=infoprint;
    }
/************************************************************/
    public void print() 
    {
        PrinterJob printJob = PrinterJob.getPrinterJob();
        printJob.setPrintable(this);
        if (printJob.printDialog())
            try 
            {
                printJob.print();
            } 
            catch(PrinterException pe) 
            {
                System.out.println("Error printing: " + pe);
            }
    }
/************************************************************/
    public int print(Graphics g, PageFormat pageFormat, int pageIndex) 
    {
        Graphics2D g2d = (Graphics2D)g;
        g2d.translate(pageFormat.getImageableX(), pageFormat.getImageableY());
        double pageHeight = pageFormat.getImageableHeight();//hauteur d'un page
        double pageWidth = pageFormat.getImageableWidth();//largeur d'un page
        double scale = 1;
        int totalNumPages=1; //nombre des pages
        totalNumPages=(int)Math.ceil((double)ha /(double)pageHeight);
        //pageIndex indique lengthn� de page � imprimer ; il commence par zero
	if (pageIndex >= totalNumPages)
        {
		return NO_SUCH_PAGE;
        }
        else
        {
            g2d.translate(0f, -pageIndex*pageHeight);
            g2d.setClip(0, (int)Math.ceil(pageHeight * pageIndex), (int)Math.ceil(pageWidth), (int)Math.ceil(pageHeight));
            disableDoubleBuffering(infoprint);
            infoprint.paint(g2d);
        }
        return Printable.PAGE_EXISTS;       
    }
/************************************************************/
    public static void disableDoubleBuffering(Component c) 
    {
        RepaintManager currentManager = RepaintManager.currentManager(c);
        currentManager.setDoubleBufferingEnabled(false);
    }
/************************************************************/
    public static void enableDoubleBuffering(Component c) 
    {
        RepaintManager currentManager = RepaintManager.currentManager(c);
        currentManager.setDoubleBufferingEnabled(true);
    }
/************************************************************/
    private int ha;//hauteur total des compasants � imprimer
}

