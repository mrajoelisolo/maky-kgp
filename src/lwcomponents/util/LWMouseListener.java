package lwcomponents.util;

import lwcomponents.canvas.*;
import java.awt.event.MouseEvent;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author FireWolf
 */
public class LWMouseListener extends LWCanvasMouseAdapter {
    private LWPoint coords = new LWPoint(0, 0);
    private List selection = new LinkedList();
    private boolean isClicked = false;
    
    public LWMouseListener(LWCanvas canvas) {
        super(canvas);
    }
    
    @Override
    public void leftClickAction(MouseEvent e) {
        isClicked = true;
    }
    
    @Override
    public void rightClickAction(MouseEvent e) {
        
    }
    
    @Override
    public void mouseReleased(MouseEvent e) {
        selection = canvas.findShapes(e.getPoint());
        isClicked = false;
    }
    
    @Override
    public void mouseMoved(MouseEvent e) {
        coords.x = e.getPoint().x;
        coords.y = e.getPoint().y;
        super.canvas.repaint();
    }
    
    /**
     * Obtenir les coordonnées du pointeur sur le canevas
     * @return
     * Instance de la classe LWPoint
     */
    public LWPoint getCoords() {
         return (LWPoint) coords.clone();
    }
    
    /**
     * Obtenir l'objet séléctionné par le pointeur
     * @return
     * Instance de la classe LWShape
     */
    public LWShape getSelection() {
        if(selection.size() == 0) return null;
        else return (LWShape) selection.get(0);
    }
    
    /**
     * Effacer l'objet séléctionné par le pointeur
     */
    public void clearSelection() {
        selection.clear();
    }
    
    public boolean isMouseDown() {
        return isClicked;
    }
}
