package lwcomponents.util;

import java.awt.AlphaComposite;
import lwcomponents.canvas.*;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

/**
 * C'est un composant totalement graphique
 * @author FireWolf
 * NB: Ce bouton utilise une image jpg
 */
public class LWButton2 extends LWShape {
    private Rectangle bounds;
    private boolean focusState = false;
    private String caption;
    private LWMouseListener mouseListener;
    private BufferedImage gainedFocusImg;
    private BufferedImage lostFocusImg;
    private BufferedImage img;
    private BufferedImage pressedBtnImg;
    private boolean pressed = false;

    public LWButton2(int x, int y, String caption) {
        super();
        this.caption = caption;
        bounds = new Rectangle(new Point(x, y), new Dimension(100, 30));
        lostFocusImg = loadImage("img\\components\\btn0.jpg");    
        gainedFocusImg = loadImage("img\\components\\btn1.jpg");
        pressedBtnImg = loadImage("img\\components\\btn2.jpg");
        img = null;
    } 
    
    @Override
    public void draw(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        
        if(focusState) {
            if(pressed) 
                img = pressedBtnImg;
            else
                img = gainedFocusImg;
            mouseListener.canvas.repaint();
        } else {
            img = lostFocusImg;
        }
        
        g2.drawImage(img, bounds.x, bounds.y, bounds.width, bounds.height, null);
        
        Font myFont = new Font("Book Antiqua", Font.BOLD, 15);
        g2.setColor(new Color(0, 0, 0));
        g2.setFont(myFont);
        g2.drawString(caption, bounds.x + 10, bounds.y + 20);
    }

    @Override
    public Rectangle getBounds() {
        return bounds;
    }
    
    /**
     * Définir l'emplacement du bouton dans le canevas
     * @param x - abscisse
     * @param y - ordonnée
     */
    public void setBounds(int x, int y, int wdt, int hgt) {
        bounds.x = x; bounds.y = y;
    }
    
    public void setSize(int wdt, int hgt) {
        bounds.width = wdt; bounds.height = hgt;
    }
    
    /**
     * Obtenir l'état du bouton c.a.d si elle gagne ou perd le focus
     * @return
     */
    public boolean getFocusState() {
        refreshListener();
        return focusState;
    }
    
    /**
     * Définir l'écouteur d'évenements du bouton
     * @param mouseListener - instance de la classe LWMouseListener
     */
    public void setMouseListener(LWMouseListener mouseListener) {
        this.mouseListener = mouseListener;
    } 
     
    public void refreshListener() {
        LWPoint pt = mouseListener.getCoords();
        
        if(bounds.contains(pt.x, pt.y)) {  
            focusState = true;       
            
            if(mouseListener.isMouseDown())
               pressed = true;
            else
                pressed = false;
        } else{
            focusState = false;
        }
    } 
    
    private static BufferedImage loadImage(String fileName) {
        BufferedImage img = null;
        
        try{
            img = ImageIO.read(new File(fileName));
            
        } catch(IOException e) {
            e.printStackTrace();
        }
        
        return img;
    }
    
    /**
     * Pour modifier l'apparence par défaut du bouton
     * Les premier paramètre spécifie le dossier où se trouvent les fichiers images.
     * Pour les 3 autres paramètres, ils spécifient l'apparence du bouton, ils
     * correspondent aux images que vous avez spécifiés dans le dossier et
     * doivent êtres de type Jpeg, vous n'avez pas besoin de spécifier l'extension (.jpg)
     * NB: le dossier doit se trouver dans le répértoire du projet
     * Si vous spécifiez plusieurs dossiers, spéparez les par 2 slash
     * ex: <dossier1>\\<dossier2>
     * @param skin1
     * Apparence du bouton à l'état normal
     * @param skin2
     * Apparence du bouton lorsqu'il est pointé par la souris
     * @param skin3
     * Apparence du bouton lorsqu'il est cliqué par la souris
     */
    public void modifySkin(String root, String skin1, String skin2, String skin3) {
        String img1 = root + "\\" + skin1 + ".jpg";
        String img2 = root + "\\" + skin2 + ".jpg";
        String img3 = root + "\\" + skin3 + ".jpg";
        img = loadImage(img1);
        gainedFocusImg = loadImage(img2);
        lostFocusImg = loadImage(img3);
    }
    
    /**
     * Modifier le skin du bouton à partir d'un style spécifié
     * @param skin
     * Instance de la classe LWButtonSkin
     */
    public void setSkin(LWButtonSkin skin) {
        lostFocusImg = skin.getLostFocusImg();
        gainedFocusImg = skin.getGainedFocusImg();
        pressedBtnImg = skin.getPressedBtnImg();
    }
}
 