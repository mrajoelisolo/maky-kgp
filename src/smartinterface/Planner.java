/*
 * Planner.java
 *
 * Created on 7 novembre 2008, 14:57
 */

package smartinterface;

import java.awt.AlphaComposite;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;
import java.util.List;
import java.util.Iterator;
import java.util.LinkedList;
import javax.swing.JPanel;
import lwcomponents.canvas.LWCanvas;
import lwcomponents.util.planner.LWTaskBar;
import mapping.Tache;
import utile.ConversionDate;
import javax.swing.Timer;
import lwcomponents.util.ITimer;
import lwcomponents.util.LWMouseListener;

/**
 *
 * @author  FireWolf
 */
public class Planner extends javax.swing.JFrame implements ITimer {
    private LWCanvas mainCanvas;
    private LWMouseListener mouseListener;
    private Timer timer;
    
    /** Creates new form Planner */
    public Planner() {
        initComponents();
        
        timer = createTimer();
        timer.start();
        
        mainCanvas = new LWCanvas() {
            @Override
            public void paintBackground(Graphics g) {
                Graphics2D g2 = (Graphics2D) g;
                
                int hDiv = 30;
                int wdt = jPanel1.getBounds().width;
                int hgt = jPanel1.getBounds().height;
                int sp = wdt / hDiv;
                int x = 0;
                
                for(int i = 0; i < hDiv; i++) {
                    
                    if(i % 2 == 0)
                        g2.setColor(new Color(192, 192, 255));
                    else
                        g2.setColor(new Color(168, 168, 224));
                    g2.fillRect(x, 0, sp, hgt);
                    
                    g2.setColor(Color.BLACK);
                    g2.drawRect(x, 0, sp, hgt);
                    x += sp;
                }
                
                g2.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.95F));
            }
        };
        
        mouseListener = new LWMouseListener(mainCanvas);
        
        jPanel1.add(mainCanvas.getCanvas(jPanel1));
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jButton1 = new javax.swing.JButton();
        jTextField1 = new javax.swing.JTextField();
        jTextField2 = new javax.swing.JTextField();
        jButton2 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(640, 480));
        setResizable(false);
        getContentPane().setLayout(null);

        jPanel1.setBackground(new java.awt.Color(102, 153, 255));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 540, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 430, Short.MAX_VALUE)
        );

        getContentPane().add(jPanel1);
        jPanel1.setBounds(90, 10, 540, 430);

        jButton1.setText("Valider");
        getContentPane().add(jButton1);
        jButton1.setBounds(350, 450, 67, 23);

        jTextField1.setText("03-11-2008");
        getContentPane().add(jTextField1);
        jTextField1.setBounds(140, 450, 80, 20);

        jTextField2.setText("07-11-2008");
        getContentPane().add(jTextField2);
        jTextField2.setBounds(552, 450, 80, 20);

        jButton2.setText("jButton2");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton2);
        jButton2.setBounds(10, 20, 73, 23);

        pack();
    }// </editor-fold>//GEN-END:initComponents

private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
// TODO add your handling code here:
    List liste = new LinkedList();
    
    Tache t1 = new Tache();
    t1.setNomTache("Tâche1");
    t1.setDebutTache(ConversionDate.creerDate(4, 11, 2008));
    t1.setFinTache(ConversionDate.creerDate(17, 11, 2008));
    liste.add(t1);
    
    Tache t2 = new Tache();
    t2.setNomTache("Tâche2");
    t2.setDebutTache(ConversionDate.creerDate(2, 11, 2008));
    t2.setFinTache(ConversionDate.creerDate(20, 11, 2008));
    liste.add(t2);
    
    Tache t3 = new Tache();
    t3.setNomTache("Tâche3");
    t3.setDebutTache(ConversionDate.creerDate(6, 11, 2008));
    t3.setFinTache(ConversionDate.creerDate(30, 11, 2008));
    liste.add(t3);
    
    Tache t4 = new Tache();
    t4.setNomTache("Tâche4");
    t4.setDebutTache(ConversionDate.creerDate(6, 11, 2008));
    t4.setFinTache(ConversionDate.creerDate(25, 11, 2008));
    liste.add(t4);
    
    Tache t5 = new Tache();
    t5.setNomTache("Tâche4");
    t5.setDebutTache(ConversionDate.creerDate(15, 11, 2008));
    t5.setFinTache(ConversionDate.creerDate(23, 11, 2008));
    liste.add(t5);
    
    //afficherTaches(jPanel1, liste, ConversionDate.creerDate(1, 11, 2008), ConversionDate.creerDate(30, 11, 2008));
    initTaches(mainCanvas, liste, ConversionDate.creerDate(1, 11, 2008), ConversionDate.creerDate(30, 11, 2008));
}//GEN-LAST:event_jButton2ActionPerformed

    /**
    * @param args the command line arguments
    */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Planner().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTextField jTextField2;
    // End of variables declaration//GEN-END:variables

    public void afficherTaches(JPanel panel, List taches, Date debut, Date fin) {
        Graphics2D g2 = (Graphics2D) panel.getGraphics();
        int yPos = 0;
        int hTache = 30;
        int divisions = ConversionDate.nbJours(debut, fin);
        int ech = panel.getBounds().width / divisions;
        
        g2.setColor(new Color(255, 128, 64));
        for(Iterator iter = taches.iterator(); iter.hasNext();) {
            Tache tache = (Tache) iter.next();
            
            int xTache = ConversionDate.nbJours(debut, tache.getDebutTache()) * ech;
            int lTache = ConversionDate.nbJours(tache.getDebutTache(), tache.getFinTache()) * ech;
            
            g2.fillRect(xTache, yPos, lTache, hTache);
            yPos += hTache + 5;
        }
    }
    
    public void initTaches(LWCanvas canvas, List taches, Date debut, Date fin) {   
        int yPos = 0;
        int hTache = 30;
        int divisions = ConversionDate.nbJours(debut, fin);
        int ech = canvas.getBounds().width / divisions;
        
        for(Iterator iter = taches.iterator(); iter.hasNext();) {
            Tache tache = (Tache) iter.next();
            
            int xTache = ConversionDate.nbJours(debut, tache.getDebutTache()) * ech;
            int lTache = ConversionDate.nbJours(tache.getDebutTache(), tache.getFinTache()) * ech;
            
            LWTaskBar barre = new LWTaskBar(xTache, yPos,lTache, hTache, tache.getNomTache(), tache);
            canvas.addShape(barre);

            yPos += hTache + 5;
        }
    }
    
    public void timerEvent() {
        
    }
    
    public Timer createTimer() {
        ActionListener action = new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                timerEvent();
            }
        };
        return new Timer(10, action);
    }
}
