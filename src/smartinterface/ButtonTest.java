/*
 * InteractionForm.java
 *
 * Created on 31 octobre 2008, 10:14
 */

package smartinterface;

import java.awt.AlphaComposite;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.util.List;
import lwcomponents.canvas.*;
import lwcomponents.util.*;
import javax.swing.Timer;
import javax.swing.UIManager;

/**
 *
 * @author  Administrateur
 */
public class ButtonTest extends javax.swing.JFrame implements ITimer {
    private LWCanvas mainCanvas;
    private LWMouseListener mouseListener;
    private Timer timer;
    private LWButton2 b1; 
    private LWButton2 b2;
    private List currentQueryResult = null;
    private BufferedImage img = LWImgUtil.loadImage("Img\\Hinata.jpg");
    
    /** Creates new form InteractionForm */
    public ButtonTest() {
        initComponents();
        
        img = LWImgUtil.desaturate(img);
        
        mainCanvas = new LWCanvas() {
            @Override
            public void paintBackground(Graphics g) {
                Graphics2D g2 = (Graphics2D) g;
                
                g2.drawImage(img, 10, 10, 150, 140, null);
                
                g2.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.6F));
                
                /*if(currentQueryResult != null) {
                    Font myFont = new Font("Arial", Font.BOLD, 15);
                    g2.setFont(myFont);
                    
                    for(Iterator iter = currentQueryResult.iterator(); iter.hasNext();) {
                        Object o = iter.next();
                        g2.drawString(o.toString(), 10, 150);
                    }
                }*/
            }
        };
        
        mouseListener = new LWMouseListener(mainCanvas);
        timer = createTimer();
        timer.start();
        
        b1 = new LWButton2(50, 120, "Executer");
        b1.setMouseListener(mouseListener);
        
        mainCanvas.addShape(b1);
        
        b2 = new LWButton2(50, 160, "Quitter");
        b2.setMouseListener(mouseListener);
        mainCanvas.addShape(b2);
        
        jPanel1.add(mainCanvas.getCanvas(jPanel1));
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        jComboBox1 = new javax.swing.JComboBox();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jProgressBar1 = new javax.swing.JProgressBar();
        jCheckBox1 = new javax.swing.JCheckBox();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTree1 = new javax.swing.JTree();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(640, 480));

        jPanel1.setBackground(new java.awt.Color(204, 204, 255));
        jPanel1.setMinimumSize(new java.awt.Dimension(640, 480));
        jPanel1.setPreferredSize(new java.awt.Dimension(640, 480));
        jPanel1.setLayout(null);

        jButton1.setText("jButton1");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton1);
        jButton1.setBounds(310, 50, 220, 23);

        jButton2.setText("jButton2");
        jPanel1.add(jButton2);
        jButton2.setBounds(310, 90, 220, 23);

        jButton3.setText("jButton3");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton3);
        jButton3.setBounds(310, 130, 220, 23);

        jTextArea1.setColumns(20);
        jTextArea1.setRows(5);
        jScrollPane1.setViewportView(jTextArea1);

        jPanel1.add(jScrollPane1);
        jScrollPane1.setBounds(300, 180, 280, 76);

        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        jPanel1.add(jComboBox1);
        jComboBox1.setBounds(430, 290, 140, 20);

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane2.setViewportView(jTable1);

        jPanel1.add(jScrollPane2);
        jScrollPane2.setBounds(50, 380, 390, 100);
        jPanel1.add(jProgressBar1);
        jProgressBar1.setBounds(50, 210, 210, 19);

        jCheckBox1.setText("jCheckBox1");
        jPanel1.add(jCheckBox1);
        jCheckBox1.setBounds(510, 380, 81, 23);

        jLabel1.setText("jLabel1");
        jPanel1.add(jLabel1);
        jLabel1.setBounds(510, 340, 34, 14);

        jScrollPane3.setViewportView(jTree1);

        jPanel1.add(jScrollPane3);
        jScrollPane3.setBounds(60, 260, 210, 110);

        getContentPane().add(jPanel1, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
// TODO add your handling code here:
}//GEN-LAST:event_jButton1ActionPerformed

private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
// TODO add your handling code here:
        jProgressBar1.setValue(80);
}//GEN-LAST:event_jButton3ActionPerformed

    /**
    * @param args the command line arguments
    */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    UIManager.setLookAndFeel(
                    UIManager.getCrossPlatformLookAndFeelClassName());
                    UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
                } catch (Exception e) { }
        
                
                new ButtonTest().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JCheckBox jCheckBox1;
    private javax.swing.JComboBox jComboBox1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JProgressBar jProgressBar1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTable jTable1;
    private javax.swing.JTextArea jTextArea1;
    private javax.swing.JTree jTree1;
    // End of variables declaration//GEN-END:variables

    public Timer createTimer() {
        ActionListener action = new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                timerEvent();
            }
        };
        return new Timer(10, action);
    }
    
    public void timerEvent() {
        if(b1.getFocusState()) {
            
                LWShape sel = mouseListener.getSelection();
                if(sel != null)
                    if(sel.equals(b1)) {
                        
                        System.out.println("vous avez cliqué sur: " + b1);
                        
                        mouseListener.clearSelection();
                    }
        }
        
        if(b2.getFocusState()) {
            
                LWShape sel = mouseListener.getSelection();
                if(sel != null)
                    if(sel.equals(b2)) {
                        
                        System.exit(0);
                        
                        mouseListener.clearSelection();
                    }
        } 
    }
}
