package smartinterface;

import java.awt.AlphaComposite;
import java.awt.Color;
import lwcomponents.util.LWImgUtil;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import lwcomponents.canvas.*;
import javax.swing.JFrame;
import javax.swing.Timer;
import java.awt.image.BufferedImage;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

/**
 *
 * @author Administrateur
 */
public class Animation extends JFrame {
    private LWCustomCanvas myCanvas1;
    private Timer timer;
    private int xPos, yPos, vx, vy;
    private LWEllipse ellipse;
    private BufferedImage img = LWImgUtil.loadImage("img\\Hinata.jpg");
    
    public Animation() {  
        timer = createTimer();
        timer.start();
        xPos = 20; yPos = 20; vx = 1; vy = 1;
        
        ellipse = new LWEllipse(new Point(10, 10), new Dimension(100, 150));
        ellipse.setBrushColor(Color.ORANGE);
        
        img = LWImgUtil.desaturate(img);
        
        myCanvas1 = new LWCustomCanvas() {
            public void paint(Graphics g) {
                Graphics2D g2 = (Graphics2D) g;
                g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
                    
                if(img != null)
                    g2.drawImage(img, 0, 0, 340, 500, null);
                
                g2.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.6F));
                
                ellipse.draw(g2);
                
                Font myFont = new Font("Arial", Font.BOLD, 15);
                g2.setFont(myFont);
                g2.drawString("Retouch an image with Java, 'Beautiful Hinata'", xPos, yPos);
            }
        };
        
        Rectangle bounds = new Rectangle(new Point(0, 0), new Dimension(this.getBounds().width, this.getBounds().height));
        getContentPane().add(myCanvas1.getCanvas(bounds));
        
        setSize(640, 480);
        setTitle("Fenetre");
        setVisible(true);
    }
    
    public void timerEvent() {
        xPos += vx; yPos += vy;
        
        if(xPos > 200) vx = -vx;
        if(xPos < 10) vx = -vx;
        if(yPos > 100) vy = -vy;
        if(yPos < 10) vy = -vy;
        
        this.repaint();
    }
    
    public Timer createTimer() {
        ActionListener action = new ActionListener() {
            public void actionPerformed(ActionEvent e) {
               timerEvent();
            }
        };
        return new Timer(5, action);
    }
    
    public static void main(String[] args) {
        Animation f = new Animation();
    }
}
