/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package smartinterface;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.Timer;
import lwcomponents.util.ITimer;

/**
 *
 * @author Administrateur
 */
public class Test extends JFrame implements ITimer {
    private Timer timer = createTimer();
    private static StringBuffer strBf = new StringBuffer(255);
    
    public Test() {
        timer = createTimer();
        //timer.start();
        
        setTitle("Ma fenetre"); setSize(320, 240);
        setVisible(true);
    }
    
    public static void main(String[] args) {
        Test c = new Test();
        strBf.append('H');
        strBf.append('i');
        strBf.append('n');
        strBf.append('a');
        strBf.append('t');
        strBf.append('a');
        System.out.println(strBf.toString());
    }
    
    public Timer createTimer() {
        ActionListener action = new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                timerEvent();
            }
        };
        return new Timer(1000, action);
    }
    
    public void timerEvent() {
        System.out.println("Hello!");
    }
}
