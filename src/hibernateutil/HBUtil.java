package hibernateutil;

import cfg.HibernateUtil;
import java.util.List;
import org.hibernate.Transaction;
import org.hibernate.HibernateException;
import org.hibernate.Session;

/**
 *
 * @author FireWolf
 */
public class HBUtil {
    public static List showQuery(String query) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction tx = null;
        
        try {
            
            tx = session.beginTransaction();
            
            List resultat = session.createQuery(query).list();
            tx.commit();
            
            return resultat;
            
        } catch(HibernateException e) {
            e.printStackTrace();
            
            if(tx != null && tx.isActive())
                tx.rollback();
            
            return null;
        }
    }
}
