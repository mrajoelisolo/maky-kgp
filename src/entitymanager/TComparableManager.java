package entitymanager;

import org.hibernate.Transaction;
import cfg.HibernateUtil;
import java.util.Date;
import java.util.List;
import java.util.Iterator;
import java.util.LinkedList;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JTree;
import javax.swing.tree.TreePath;
import mapping.*;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author FireWolf
 */
public abstract class TComparableManager {
    public static void afficher() {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        
        try{
            session.beginTransaction();
            List res = session.createQuery("from TComparable").list();
            for(Iterator iter = res.iterator(); iter.hasNext();) {
                TComparable t = (TComparable) iter.next();
                System.out.println(t.getIdTComparable() + " " + t.getValeurId());
            }
            session.beginTransaction().commit();
        }catch(HibernateException e) {
            e.printStackTrace();
        }
    }
    
    public static TComparable charger(int id) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        TComparable res = null;
        
        try{
            session.beginTransaction();
            
            res = (TComparable) session.load(TComparable.class, id);
            
        }catch(HibernateException e) {
            e.printStackTrace();
        }
        return res;
    }
    
    public static void creer(TComparable t) {
        Transaction tx = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        
        try{
            tx = session.beginTransaction();
            
            session.save(t);
            
            tx.rollback();
        }catch(HibernateException e) {
            e.printStackTrace();
            
            if(tx != null && tx.isActive())
                tx.rollback();
        }
    }
    
    public static void modifier(TComparable t) {
        Transaction tx = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        
        try{
            tx = session.beginTransaction();
            
            session.update(t);
            
            tx.rollback();
        }catch(HibernateException e) {
            e.printStackTrace();
            
            if(tx != null && tx.isActive())
                tx.rollback();
        }
    }
    
    public static void supprimer(TComparable t) {
        Transaction tx = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        
        try{
            tx = session.beginTransaction();
            
            session.delete(t);
            
            tx.rollback();
        }catch(HibernateException e) {
            e.printStackTrace();
            
            if(tx != null && tx.isActive())
                tx.rollback();
        }
    }
    
    /**
     * Charger tous les identifiants non encore utilisés
     * @param cb
     * JComboBox
     * @param idProjet
     * Identité du projet
     */
    public static void chargerIdComparaison(JComboBox cb, int idProjet) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        
        DefaultComboBoxModel cm = new DefaultComboBoxModel();
        
        try{
            session.beginTransaction();
            
            for(int i = 1; i <= 100; i++) {
                List res = session.createQuery("from TComparable as t where t.projet.idProjet = " + idProjet + " " +
                        "and t.valeurId = " + i).list();
                if(res.size() == 0)
                    cm.addElement(i);
            }
            cb.setModel(cm);
            
            session.getTransaction().commit();
        }catch(HibernateException e) {
            e.printStackTrace();
        }
    }
    
    public static Tache getIdTache(int idComp) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Tache res = null;
        
        try{
            session.beginTransaction();
            
            res = (Tache) session.load(Tache.class, idComp);
            
            session.getTransaction().commit();
        }catch(HibernateException e) {
            e.printStackTrace();
        }
        return res;
    }
}
