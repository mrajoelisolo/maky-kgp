package entitymanager;

import cfg.HibernateUtil;
import mapping.*;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.HibernateException;
import java.util.List;
import java.util.Iterator;
import java.util.LinkedList;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JTable;
import utile.MyRecordSet;
import utile.TableModel;

/**
 *
 * @author FireWolf
 */
public abstract class ARessourceManager {
    private static List buffer = new LinkedList();
    
    public static void clearBuffer() {
        buffer.clear();
    }
    
    /**
     * Retourne le contenu de la mémoire tampon sous forme de liste
     * @return
     */
    public List getBuffer() {
        return buffer;
    }
    
    /**
     * Afficher les autres ressources de la base
     */
    public static void afficher() {
        Transaction tx = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        
        try{
            tx = session.beginTransaction();
            
            List res = session.createQuery("from ARessource").list();
            for(Iterator iter = res.iterator(); iter.hasNext();) {
                ARessource r = (ARessource) iter.next();
                System.out.println(r.getNomARessource());
            }
            
            tx.commit();
            
        }catch(HibernateException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Charger une ressource de la base
     * @param id
     * Identité de la ressource
     * @return
     * Instance de l'objet
     */
    public static ARessource chargerARessource(int id) {
        ARessource res = null;
        Transaction tx = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        
        try{
            tx = session.beginTransaction();
            
            res = (ARessource) session.load(ARessource.class, id);
            
        }catch(HibernateException e) {
            e.printStackTrace();
        }
        
        return res;
    }
    
    /**
     * Créer une ressource et l'ajouter dans la base
     * @param r
     * Instance de l'objet
     */
    public static void creerARessource(ARessource r) {
        Transaction tx = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        
        try{
            tx = session.beginTransaction();
            
            session.save(r);
            
            tx.commit();
        }catch(HibernateException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Modifier une ressource dans la base
     * @param r
     * Instance de l'objet
     */
    public static void modifierARessource(ARessource r) {
        Transaction tx = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();

        try {
            tx = session.beginTransaction();
            session.update(r);
            tx.commit();
        } catch(HibernateException e) {
            e.printStackTrace();
            
            if(tx != null && tx.isActive())
                tx.rollback();
        }
    }
    
    /**
     * Supprimer une ressource dans la base
     * @param r
     * Instance de la ressource représentant la ressource dans la base
     */
    public static void supprimerARessource(ARessource r) {
        Transaction tx = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();

        try {
            tx = session.beginTransaction();
            session.delete(r);
            tx.commit();
        } catch(HibernateException e) {
            e.printStackTrace();
            
            if(tx != null && tx.isActive())
                tx.rollback();
        }
    }
    
    public static int chargerARessources(JTable maTable, MyRecordSet resultat) {
        Transaction tx = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        int nbLignes = 0;
        
        try {
            tx = session.beginTransaction();
            
            List res = session.createQuery("from ARessource").list();
            nbLignes = res.size();
            Object[][] aressources = new Object[res.size()][3];
            
            int i = 0;
            for(Iterator iter = res.iterator(); iter.hasNext();) {
                ARessource r = (ARessource) iter.next();
                
                aressources[i][0] = r.getIdARessource();
                aressources[i][1] = r.getNomARessource();
                aressources[i][2] = r.getCoutARessource();
                i++;
            }
            
            //Chargement des données dans la table
            //Prévention contre les erreurs
            if(aressources.length == 0)
                aressources = new Object[1][3];
            
            String[] titres = {"identifiant", "Nom", "Coût"};
            TableModel modele = new TableModel(aressources, titres);
            
            maTable.setModel(modele);
            maTable.setRowSelectionAllowed(true);
            maTable.setRowSelectionInterval(0, 0);
            maTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
            maTable.setPreferredSize(maTable.getMaximumSize());
            
            //Chargement des données dans le recordset
            Object[] o = res.toArray();
            resultat.setValue(o);
            
            tx.commit();
        } catch(HibernateException e) {
            e.printStackTrace();
            
            if(tx != null && tx.isActive())
                tx.rollback();
        }
        return nbLignes;
    }
    
    public static int chargerARessources(JTable maTable, MyRecordSet resultat,
            String critere) {
        Transaction tx = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        int nbLignes = 0;
        
        try {
            tx = session.beginTransaction();
            
            String req = "from ARessource as r where r.nomARessource like '" + critere + "'";
            List res = session.createQuery(req).list();
            nbLignes = res.size();
            Object[][] aressources = new Object[res.size()][3];
            
            int i = 0;
            for(Iterator iter = res.iterator(); iter.hasNext();) {
                ARessource r = (ARessource) iter.next();
                
                aressources[i][0] = r.getIdARessource();
                aressources[i][1] = r.getNomARessource();
                aressources[i][2] = r.getCoutARessource();
                i++;
            }
            
            //Chargement des données dans la table
            //Prévention contre les erreurs
            if(aressources.length == 0)
                aressources = new Object[1][3];
            
            String[] titres = {"identifiant", "nom", "Coût"};
            TableModel modele = new TableModel(aressources, titres);
            
            maTable.setModel(modele);
            maTable.setRowSelectionAllowed(true);
            maTable.setRowSelectionInterval(0, 0);
            maTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
            maTable.setPreferredSize(maTable.getMaximumSize());
            
            //Chargement des données dans le recordset
            Object[] o = res.toArray();
            resultat.setValue(o);
            
            tx.commit();
        } catch(HibernateException e) {
            e.printStackTrace();
            
            if(tx != null && tx.isActive())
                tx.rollback();
        }
        return nbLignes;
    }
    
    /**
     * Afficher les ressources matérielles dans un comboBox
     * @param jCombo
     * Le JComboBox a remplir
     */
    public static void chargerARessources(JComboBox jCombo) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        DefaultComboBoxModel modele = new DefaultComboBoxModel();
        
        try{
            session.beginTransaction();
            
            List res = session.createQuery("from ARessource").list();
            
            for(Iterator iter = res.iterator(); iter.hasNext();) {
                ARessource r = (ARessource) iter.next();
                modele.addElement(r.getNomARessource());
            }
            
            jCombo.setModel(modele);
            
            session.getTransaction().commit();
        }catch(HibernateException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Charge l'instance d'une ressource matérielle à partir de son pseudo
     * @param pseudo
     * Pseudo corresponant de la ressource
     * @return
     * Instance de la ressource
     */
    public static ARessource chargerARessource(String nom) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        ARessource res = null;
        
        try{
            session.beginTransaction();
            
            res = (ARessource) session.createQuery("from ARessource where nomARessource like '" + nom +"'").uniqueResult();
            
            session.getTransaction().commit();
        }catch(HibernateException e) {
            e.printStackTrace();
        }
        return res;
    }
    
    /**
     * Met à jour l'affectation des ressources matérielles sur les tâches mères
     * lorsqu'on affecte une ressource à une tâche fille
     * @param t
     * La tâche fille
     * @param r
     * La ressource matérielle à affecter
     */
    public static void mettreAJourAffectationDesc(Tache t, ARessource r) {
        if(t.getNiveau() != 1) {
            Tache mere = TacheManager.chargerTache(t.getPred());
            //Mises à jour            
            if(!ARessourceManager.isARessourceAffectee(mere, r.getNomARessource())) {
                mere.getAressources().add(r); //Affectation
                TacheManager.modifierTache(mere);
            }
            mettreAJourAffectationDesc(mere, r);
        }
    }
    
    /**
     * Vérifie si la ressource est déja affectée à la tâche
     * NB : La tâche doit être préalablement initialisée, afin d'éviter les
     * problèmes de proxy (problèmes de session)
     * @param t
     * La tâche qui affecte les ressources
     * @param r
     * La ressource à vérifier
     * @return
     * true si la ressource est déja affectée, false dans le cas echéant
     */
    public static boolean isARessourceAffectee(Tache tache, String r) {
        boolean res = false;
        
        for(Iterator iter = tache.getAressources().iterator(); iter.hasNext();) {
            ARessource aRessource = (ARessource) iter.next();
            if(aRessource.getNomARessource().equals(r))
                res = true;
        }
        
        return res;
    }
    
    /**
     * Desaffecter une ressource matérielle d'une tâche. Cela engendre également
     * le même effet pour les tâches filles. NB : Cette méthode n'a aucune
     * effet si la tâche n'a pas été initialisée avec la methode creerArborescence()
     * (This method has no effect if you don't initialize the Tache with creerArborescence()).
     * Si la ressource à desaffecter n'éxiste pas, la méthode ne fait rien
     * @param t
     */
    public static void desaffecterArborescence(Tache t, int idARessource) {
        clearBuffer();
        Tache racine = TacheManager.creerArborescence(t.getProjet().getIdProjet());
        Noeud rech = new Noeud();
        TacheManager.rechercher(racine, rech, t.getIdTache());
        obtenirArbre(rech.getTache(), idARessource);
        buffer.add(t); //Ajouter la mère, elle fait partie des tâches à desaffecter

        //Désaffecter les tâches filles
        for(Iterator iter = buffer.iterator(); iter.hasNext();) {
            Tache tSup = (Tache) iter.next();
            
            ARessourceManager.desaffecterARessource(tSup, idARessource);
        }
        clearBuffer();
    }
    
    //is OK
    private static void obtenirArbre(Tache t, int idARessource) {
        if(t.getTachesFilles().size() != 0) {
            for(Iterator iter = t.getTachesFilles().iterator(); iter.hasNext();) {
                Tache fille = (Tache) iter.next();
                //System.out.println("A supprimer : " + fille.getNomTache());
                buffer.add(fille);
                
                obtenirArbre(fille, idARessource);
            }
        }
    }
    
    /**
     * Desaffecte une ressource matérielle d'une tâche. La méhode ne fait rien
     * si la ressource n'est pas trouvée
     * Etat : OK
     * @param t
     * La tâche à desaffecter la ressource
     * @param r
     * La ressource à desaffecters
     */
    public static void desaffecterARessource(Tache t, int idARessource) {
        ARessource rech = null;
        
            for(Iterator iter = t.getAressources().iterator(); iter.hasNext();) {
                ARessource res = (ARessource) iter.next();
                if(idARessource == res.getIdARessource()) {
                    rech = res;
                    System.out.println("Desaffecté : " + res.getNomARessource());
                    break;
                }
            }
            
            t.getAressources().remove(rech);
            TacheManager.modifierTache(t);
        System.out.println(t.getAressources());
    }
}
