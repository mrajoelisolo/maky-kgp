package entitymanager;

/**
 *
 * @author FireWolf
 * Cette classe regroupe les constantes à utiliser lors de transactions
 * avec la base de données
 */
public abstract class EntityManager {
    public static final String AJOUT = "AJOUT";
    public static final String SUPPRESSION = "SUPPRESSION";
    public static final String MODIFICATION = "MODIFICATION";
    public static final String AUCUNE = "AUCUNE";
    public static final String FONT_TITRE = "Arial Narrow";
    public static final int TAILLE_FONT_LIGNE = 15;
    public static final String FONT_LIGNE = "Arial Narrow";
    public static final int TAILLE_FONT_TITRE = 15;
    public static final String UT_MONETAIRE = "Ar";
    public static final String FONT_LIBELLE = "Arial Rounded MT Bold";
    public static int TAILLE_FONT_LIBELLE = 22;
    public static String ETAT_TACHE = "EtatTache";
    public static String ETAT_RESSOURCE = "EtatRessource";
    public static String ETAT_ARESSOURCE = "EtatARessource";
}
