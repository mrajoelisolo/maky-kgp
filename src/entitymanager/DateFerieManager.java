package entitymanager;

import cfg.HibernateUtil;
import mapping.*;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.HibernateException;
import java.util.List;
import java.util.Iterator;
import javax.swing.JTable;
import utile.CalendarEngine;
import utile.MyRecordSet;
import utile.TableModel;

/**
 *
 * @author FireWolf
 */
public abstract class DateFerieManager {
    /**
     * Afficher toutes les dates fériés repertoriées
     */
    public static void afficher() {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        
        try{
            session.beginTransaction();
            
            List res = session.createQuery("from DateFerie").list();
            
            for(Iterator iter = res.iterator(); iter.hasNext();) {
                DateFerie d = (DateFerie) iter.next();
                System.out.println(d.getNomDateFerie() + " " + d.getJourFerie() + "/" + d.getMoisFerie());
            }
            
            session.getTransaction().commit();
        }catch(HibernateException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Change une date férié de la base
     * @param id
     * Identité de la date férié
     * @return
     * Instance de la date férié
     */
    public static DateFerie chargerDateFerie(int id) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        DateFerie res = null;
        
        try{
            session.beginTransaction();
            
            res = (DateFerie) session.load(DateFerie.class, id);
            
        }catch(HibernateException e) {
            e.printStackTrace();
        }
        return res;
    }
    
    /**
     * Créer une date férié
     * @param d
     */
    public static void creerDateFerie(DateFerie d) {
        Transaction tx = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        
        try {
            tx = session.beginTransaction();
            
            session.save(d);
            
            tx.commit();
        }catch(HibernateException e) {
            e.printStackTrace();
            
            if(tx != null && tx.isActive())
                tx.rollback();
        }
    }
    
    /**
     * Modifier une date férié de la base
     * @param d
     * Instance de la date férié
     */
    public static void modifierDateFerie(DateFerie d) {
        Transaction tx = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        
        try {
            tx = session.beginTransaction();
            
            session.update(d);
            
            tx.commit();
        }catch(HibernateException e) {
            e.printStackTrace();
            
            if(tx != null && tx.isActive())
                tx.rollback();
        }
    }
    
    /**
     * Supprimer une date férié
     * @param d
     * Instance de la date férié
     */
    public static void supprimerDateFerie(DateFerie d) {
        Transaction tx = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        
        try {
            tx = session.beginTransaction();
            
            session.delete(d);
            
            tx.commit();
        }catch(HibernateException e) {
            e.printStackTrace();
            
            if(tx != null && tx.isActive())
                tx.rollback();
        }
    }
    
    /**
     * Charger les jours feriés dans un JTable et dans un MyRecordSet
     * @param maTable
     * Instance d'un JTable
     * @param resultat
     * Instance d'un MyRecordSet
     * @return
     * Nombre d'enregistrements chargées
     */
    public static int chargerDateFeries(JTable maTable, MyRecordSet resultat) {
        Transaction tx = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        int nbLignes;
        
        try {
            tx = session.beginTransaction();
            
            List res = session.createQuery("from DateFerie").list();
            Object[][] dates = new Object[res.size()][3];
            nbLignes = res.size();
            
            int i = 0;
            for(Iterator iter = res.iterator(); iter.hasNext();) {
                DateFerie d = (DateFerie) iter.next();
                
                dates[i][0] = d.getIdDateFerie();
                dates[i][1] = d.getNomDateFerie();
                dates[i][2] = d.getJourFerie() + " " + CalendarEngine.obtenirNomMois(d.getMoisFerie() - 1);
                i++;
            }
            
            //Chargement des données dans la table
            //S'il n'y a aucun résultat : prévention contre les erreurs
            if(dates.length == 0)
                dates = new Object[1][3];
            
            String[] titres = {"identifiant", "Nom", "Date"};
            TableModel modele = new TableModel(dates, titres);
            
            maTable.setModel(modele);
            maTable.setRowSelectionAllowed(true);
            maTable.setRowSelectionInterval(0, 0);
            maTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
            maTable.setPreferredSize(maTable.getMaximumSize());
            
            //Chargement des données dans le recordset
            Object[] o = res.toArray();
            resultat.setValue(o);
            
            tx.commit();
        } catch(HibernateException e) {
            e.printStackTrace();
            
            if(tx != null && tx.isActive())
                tx.rollback();
            
            nbLignes = 0;
        }
        return nbLignes;
    }
    
    public static int chargerDateFeries(JTable maTable, MyRecordSet resultat,
            String critere) {
        Transaction tx = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        int nbLignes;
        
        try {
            tx = session.beginTransaction();
            
            String req = "from DateFerie as p where p.nomDateFerie like '" + critere + "'";
            List res = session.createQuery(req).list();
            Object[][] dates = new Object[res.size()][3];
            nbLignes = res.size();
            
            int i = 0;
            for(Iterator iter = res.iterator(); iter.hasNext();) {
                DateFerie d = (DateFerie) iter.next();
                
                dates[i][0] = d.getIdDateFerie();
                dates[i][1] = d.getNomDateFerie();
                dates[i][2] = d.getJourFerie() + "/" + d.getMoisFerie();
                i++;
            }
            
            //Chargement des données dans la table
            //S'il n'y a aucun résultat : prévention contre les erreurs
            if(dates.length == 0)
                dates = new Object[1][3];
            
            String[] titres = {"identifiant", "login", "privilège"};
            
            TableModel modele = new TableModel(dates, titres);
            
            maTable.setModel(modele);
            maTable.setRowSelectionAllowed(true);
            maTable.setRowSelectionInterval(0, 0);
            maTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
            maTable.setPreferredSize(maTable.getMaximumSize());
            
            //Chargement des données dans le recordset
            Object[] o = res.toArray();
            resultat.setValue(o);
            
            tx.commit();
        } catch(HibernateException e) {
            e.printStackTrace();
            
            if(tx != null && tx.isActive())
                tx.rollback();
            
            nbLignes = 0;
        }
        return nbLignes;
    }
}
