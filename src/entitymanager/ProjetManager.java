package entitymanager;

import cfg.HibernateUtil;
import etat.EtatARessources;
import etat.EtatRessources;
import etat.EtatTaches;
import java.util.ArrayList;
import java.util.Date;
import mapping.*;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.HibernateException;
import java.util.List;
import java.util.Iterator;
import java.util.LinkedList;
import javax.swing.JTable;
import org.hibernate.criterion.Restrictions;
import utile.ConversionDate;
import utile.TableModel;
import utile.MyRecordSet;

/**
 *
 * @author FireWolf
 */
public abstract class ProjetManager {
    /**
     * Affiche tous les projets de la base de données
     */
    public static void afficher() {
        Transaction tx = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        
        try{
            tx = session.beginTransaction();
            
            List res = session.createQuery("from Projet as p").list();
            for(Iterator iter = res.iterator(); iter.hasNext();) {
                Projet p = (Projet) iter.next();
                System.out.println(p.getNomProjet());
            }
            
            tx.commit();
        }catch(HibernateException e) {
            e.printStackTrace();
            
            if(tx != null && tx.isActive()) {
                tx.rollback();
            }
        }
    }
    
    /**
     * Charge une instance d'un projet de la base
     * @param idProjet
     * @return
     */
    public static Projet chargerProjet(int idProjet) {
        Transaction tx = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Projet res = null;
        
        try{
            tx = session.beginTransaction();
            
            res = (Projet) session.load(Projet.class, idProjet);
            
        }catch(HibernateException e) {
            e.printStackTrace();                             
        }
        
        return res;
    }
    
    /**
     * Supprime un projet de la base
     * @param p
     * Instance du projet à supprimer
     */
    public static void supprimerProjet(Projet p) {
        Transaction tx = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        
        try {
            tx = session.beginTransaction();
            session.delete(p);
            tx.commit();
        } catch(HibernateException e) {
            e.printStackTrace();
            
            if(tx != null && tx.isActive()) {
                tx.rollback();
            }
        }
    }
    
    /**
     * Crée un projet dans la base
     * @param p
     * Instance du projet à créer
     */
    public static void creerProjet(Projet p) {
        Transaction tx = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();

        try {
            tx = session.beginTransaction();
            
            //Crée la tâche racine du projet créé
            Tache t = new Tache();
            t.setNomTache(p.getNomProjet());
            t.setDebutTache(p.getDebutProjet());
            t.setFinTache(p.getFinProjet());
            t.setProjet(p);
            t.setPred(0);
            t.setNiveau(1);
            p.getTaches().add(t);
            
            session.save(p);
            tx.commit();
        } catch(HibernateException e) {
            e.printStackTrace();
            
            if(tx != null && tx.isActive())
                tx.rollback();
        }
    }
    
    /**
     * Modifier un projet de la base. NB: Le nom de la tâche racine est également
     * modifié (La tâche de niveau 1)
     * @param p
     * Instance du projet à modifier
     */
    public static void modifierProjet(Projet p) {
        Transaction tx = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();

        try {
            tx = session.beginTransaction();
            
            session.update(p);
            String req = "update Tache set nomTache = '" + p.getNomProjet() + "' where niveau = 1 and idProjet = " + p.getIdProjet();
            int upd = session.createQuery(req).executeUpdate();
            
            tx.commit();
        } catch(HibernateException e) {
            e.printStackTrace();
            
            if(tx != null && tx.isActive())
                tx.rollback();
        }
    }
    
    /**
     * Afficher les projets de la base dans un JTable et stocker les résultats 
     * dans un MyRecordSet. 
     * @param maTable
     * Le composant JTable à afficher les projets
     * @param resultat
     * Instance d'un MyRecordSet pour stocker les résultats
     * @param profil
     * Profil correspondant aux projets à charger
     * @return
     */
    public static int chargerProjets(JTable maTable, MyRecordSet resultat, Profil profil) {
        Transaction tx = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        int nbLignes;
        
        try {
            tx = session.beginTransaction();
            
            List res = session.createQuery("from Projet where idProfil = " + profil.getIdProfil()).list();
            Object[][] projets = new Object[res.size()][4];
            nbLignes = res.size();
            
            int i = 0;
            for(Iterator iter = res.iterator(); iter.hasNext();) {
                Projet p = (Projet) iter.next();
                
                projets[i][0] = p.getIdProjet();
                projets[i][1] = p.getNomProjet();
                projets[i][2] = ConversionDate.dateToString(p.getDebutProjet());
                projets[i][3] = ConversionDate.dateToString(p.getFinProjet());
                i++;
            }
            
            //Chargement des données dans la table
            //S'il n'y a aucun résultat : prévention contre les erreurs
            if(projets.length == 0)
                projets = new Object[1][3];
            
            String[] titres = {"identifiant", "Nom", "Début", "Fin"};
            TableModel modele = new TableModel(projets, titres);
            
            maTable.setModel(modele);
            maTable.setRowSelectionAllowed(true);
            maTable.setRowSelectionInterval(0, 0);
            maTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
            maTable.setPreferredSize(maTable.getMaximumSize());
            
            //Chargement des données dans le recordset
            Object[] o = res.toArray();
            resultat.setValue(o);
            
            tx.commit();
        } catch(HibernateException e) {
            e.printStackTrace();
            
            if(tx != null && tx.isActive())
                tx.rollback();
            
            nbLignes = 0;
        }
        return nbLignes;
    }
    
    /**
     * Charge les projets dont le nom répond à un certain critère
     * @param maTable
     * Composant table
     * @param resultat
     * Ensemble de résultats, instance de la classe MyRecordSet
     * @param profil
     * Profil correspondant au projet
     * @param critere
     * Critère, utiliser '%'
     * @return
     * Nombre d'entregistrements
     */
    public static int chargerProjets(JTable maTable, MyRecordSet resultat, Profil profil
            , String critere) {
        Transaction tx = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        int nbLignes;
        
        try {
            tx = session.beginTransaction();
            
            String req = "from Projet as p where p.nomProjet like '" + critere + "' and idProfil = " + profil.getIdProfil();
            List res = session.createQuery(req).list();
            Object[][] projets = new Object[res.size()][4];
            nbLignes = res.size();
            
            int i = 0;
            for(Iterator iter = res.iterator(); iter.hasNext();) {
                Projet p = (Projet) iter.next();
                
                projets[i][0] = p.getIdProjet();
                projets[i][1] = p.getNomProjet();
                projets[i][2] = p.getDebutProjet();
                projets[i][3] = p.getFinProjet();
                i++;
            }
            
            //Chargement des données dans la table
            if(projets.length == 0)
                projets = new Object[1][3];
            
            String[] titres = {"identifiant", "Nom", "Début", "Fin"};
            TableModel modele = new TableModel(projets, titres);
            
            maTable.setModel(modele);
            maTable.setRowSelectionAllowed(true);
            maTable.setRowSelectionInterval(0, 0);
            maTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
            maTable.setPreferredSize(maTable.getMaximumSize());
            
            //Chargement des données dans le recordset
            Object[] o = res.toArray();
            resultat.setValue(o);
            
            tx.commit();
        } catch(HibernateException e) {
            e.printStackTrace();
            
            if(tx != null && tx.isActive())
                tx.rollback();
            
            nbLignes = 0;
        }
        return nbLignes;
    }
    
    /**
     * //Créée le 11/12/2008
     * Charger les projets correspondant compris entre une intervalle de dates
     * @param maTable
     * Composant table
     * @param resultat
     * Ensemble de résultats, instance de la classe MyRecordSet
     * @param profil
     * Profil correspondant au projet
     * @param date1
     * Date de début de l'intervalle
     * @param date2
     * Date de fin de l'intervalle
     * @return
     * Nombre d'enregistrements
     */
    public static int chargerProjets(JTable maTable, MyRecordSet resultat, Profil profil
            ,Date date1, Date date2) {
        Transaction tx = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        int nbLignes;
        
        try {
            tx = session.beginTransaction();
            
           List res = session.createCriteria(Projet.class).add(Restrictions.between("debutProjet", date1, date2))
                   .add(Restrictions.between("finProjet", date1, date2)).list();

           System.out.println("" + date1 + " " + date2);
           
            Object[][] projets = new Object[res.size()][4];
            nbLignes = res.size();
            
            int i = 0;
            for(Iterator iter = res.iterator(); iter.hasNext();) {
                Projet p = (Projet) iter.next();
                
                projets[i][0] = p.getIdProjet();
                projets[i][1] = p.getNomProjet();
                projets[i][2] = p.getDebutProjet();
                projets[i][3] = p.getFinProjet();
                i++;
            }
            
            //Chargement des données dans la table
            if(projets.length == 0)
                projets = new Object[1][3];
            
            String[] titres = {"id", "Nom", "Début", "Fin"};
            TableModel modele = new TableModel(projets, titres);
            
            maTable.setModel(modele);
            maTable.setRowSelectionAllowed(true);
            maTable.setRowSelectionInterval(0, 0);
            maTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
            maTable.setPreferredSize(maTable.getMaximumSize());
            
            //Chargement des données dans le recordset
            Object[] o = res.toArray();
            resultat.setValue(o);
            
            tx.commit();
        } catch(HibernateException e) {
            e.printStackTrace();
            
            if(tx != null && tx.isActive())
                tx.rollback();
            
            nbLignes = 0;
        }
        return nbLignes;
    }
    
    /**
     * Met à jour le nom de la tâche racine du projet.
     * Le nom de cette tâche est le même que celui du projet
     * @param p
     * Instance d'un projet
     */
    public static void mettreAJourTacheRacine(Projet p) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction tx = null;
        
        try{
            tx = session.beginTransaction();
            
            String req = "update Tache set nomTache = '" + p.getNomProjet() + "' where niveau = 1 and idProjet = " + p.getIdProjet();
            int upd = session.createQuery(req).executeUpdate();
            
            tx.commit();
        }catch(HibernateException e) {
            e.printStackTrace();
            
            if(tx != null && tx.isActive()) 
                tx.rollback();
        }
    }
    
    /**
     * @since 15/12/2008
     * @deprecated 
     * Calcule la valeur des charges amputées à un projet. Prend en compte les
     * ressources de travail et les ressources matérielles
     * Méthode 1 : parcours de toutes les tâches avec prise en compte de la
     * priorité des tâches filles (les détails sont implémentés dans le manuel)
     * @param idProjet
     * @return
     * Le cout du projet, esimé en Ar
     */
    public static float obtenirCoutChargeProjet(int idProjet) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction tx =null;
        
        float res = 0;
        int duree = 0;
        
        try{
            tx = session.beginTransaction();
            
            List taches = session.createQuery("from Tache as t where t.projet.idProjet = " + idProjet).list();
            for(Iterator iter = taches.iterator(); iter.hasNext();) {
                Tache tache = (Tache) iter.next();
                duree = TacheManager.compterJoursSansDateFerie(tache);
                
                Iterator iter2 = ProjetManager.obtenirRessourcesPropres(tache.getIdTache()).iterator();
                for(;iter2.hasNext();) {
                    Integer idRes = (Integer) iter2.next();
                    Ressource ressource = RessourceManager.chargerRessource(idRes);             
                    
                    float cout = ressource.getPoste().getCoutPoste() * duree;
                    res += cout;
                }
            
                iter2 = ProjetManager.obtenirARessourcesPropres(tache.getIdTache()).iterator();
                for(;iter2.hasNext();){
                    Integer idRes = (Integer) iter2.next();
                    ARessource aRessource = ARessourceManager.chargerARessource(idRes);
                    
                    float cout = aRessource.getCoutARessource() * duree;
                    res += cout;
                }
            }
            
        }catch(HibernateException e) {
            e.printStackTrace();
            
            if(tx != null && tx.isActive())
                tx.rollback();
        }
        
        return res;
    }
    
    /**
     * @since 15/12/2008
     * @deprecated
     * Calcule la valeur des charges amputées à un projet. Prend en compte les
     * ressources de travail et les ressources matérielles
     * Méthode 2 : parcours de toutes les tâches avec prise en compte de la
     * priorité des tâches filles (les détails sont implémentés dans le manuel) et 
     * prise en compte des sélections
     * @param idProjet
     * @return
     * Le cout du projet, esimé en Ar
     */
    public static float obtenirCoutChargeProjet2(int idProjet) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction tx =null;
        
        float res = 0;
        int duree = 0;
        
        try{
            tx = session.beginTransaction();
            
            List taches = session.createQuery("from Tache as t where t.projet.idProjet = " + idProjet).list();
            for(Iterator iter = taches.iterator(); iter.hasNext();) {
                Tache tache = (Tache) iter.next();
                if(tache.getEtat() == true) {
                    duree = TacheManager.compterJoursSansDateFerie(tache);

                    Iterator iter2 = ProjetManager.obtenirRessourcesPropres(tache.getIdTache()).iterator();
                    for(;iter2.hasNext();) {
                        Integer idRes = (Integer) iter2.next();
                        Ressource ressource = RessourceManager.chargerRessource(idRes);

                        float cout = ressource.getPoste().getCoutPoste() * duree;
                        res += cout;
                    }

                    iter2 = ProjetManager.obtenirARessourcesPropres(tache.getIdTache()).iterator();
                    for(;iter2.hasNext();){
                        Integer idRes = (Integer) iter2.next();
                        ARessource aRessource = ARessourceManager.chargerARessource(idRes);         

                        float cout = aRessource.getCoutARessource() * duree;
                        res += cout;
                    }
                }
            }
            
        }catch(HibernateException e) {
            e.printStackTrace();
            
            if(tx != null && tx.isActive())
                tx.rollback();
        }
        
        return res;
    }
    
    /**
     * @Since 16/12/2008
     * Retourne les propres ressources d'une tâche donnée, c'est à dire les ressources
     * qui ne sont pas affectées aux tâches filles
     * @param idTache
     * @return
     * Retourne la liste des identifiants et non les instances
     */
    public static List obtenirRessourcesPropres(int idTache) {
        List res = new ArrayList();
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction tx = null;
        
        try{
            tx = session.beginTransaction();
            
            List filles = session.createQuery("from Tache as t where t.pred = " + idTache).list();
            List idRessM = new ArrayList(); //Ressources de la tâche mère
            List idRessF = new ArrayList(); //Ressources des tâches filles
            
            //Liste toutes les ressources affectées à la tâche mère
            Tache tacheM = (Tache) session.load(Tache.class, idTache);
            for(Iterator iter = tacheM.getRessources().iterator(); iter.hasNext();) {
                Ressource r = (Ressource) iter.next();
                idRessM.add(r.getIdRessource());
            }
            
            //Parcourir les tâches filles du niveau suivant
            for(Iterator iter = filles.iterator(); iter.hasNext();) {
                Tache fille = (Tache) iter.next();
                //Liste toutes les ressources de la tâche fille
                for(Iterator iter2 = fille.getRessources().iterator(); iter2.hasNext();) {
                    Ressource ressource = (Ressource) iter2.next();
                    //Si la ressource à été déja listée alors inutile de l'ajouter
                    if(idRessF.contains(ressource.getIdRessource()) == false)
                        idRessF.add(ressource.getIdRessource());
                }
            }
            
            //Enlève les ressources qui sont dans les tâches filles
            for(Iterator iter = idRessF.iterator(); iter.hasNext();) {
                Integer idRess = (Integer) iter.next();
                
                if(idRessM.contains(idRess)) {
                    idRessM.remove(idRess);
                }
            }
            
            res = idRessM;
            
            tx.commit();
        }catch(HibernateException e) {
            e.printStackTrace();
        }
        
        return res;
    }
    
    /**
     * Obtenir l'identifiantd des ressources affectées à la tâche, on ne
     * prend pas en compte des affectations aux tâches filles
     * @param idTache
     * Identifiant de la tache
     * @return
     * Liste d'identifiants, à convertir en Integer
     */
    public static List obtenirRessources(int idTache) {
        List res = new ArrayList();
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction tx = null;
        
        try{
            tx = session.beginTransaction();
            
            Tache tache = (Tache) session.load(Tache.class, idTache);
            Iterator iter = tache.getRessources().iterator();
            for(;iter.hasNext();) {
                Ressource ressource = (Ressource) iter.next();
                res.add(ressource.getIdRessource());
            }
            
            tx.commit();
        }catch(HibernateException e) {
            e.printStackTrace();
        }
        
        return res;
    }
    
    /**
     * @Since 16/12/2008
     * Retourne les propres ressources matérielles d'une tâche donnée, c'est à dire les ressources
     * qui ne sont pas affectées aux tâches filles
     * @param idTache
     * @return
     * Retourne la liste des identifiants et non les instances
     */
    public static List obtenirARessourcesPropres(int idTache) {
        List res = new ArrayList();
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction tx = null;
        
        try{
            tx = session.beginTransaction();
            
            List filles = session.createQuery("from Tache as t where t.pred = " + idTache).list();
            List idRessM = new ArrayList(); //Ressources de la tâche mère
            List idRessF = new ArrayList(); //Ressources des tâches filles
            
            //Liste toutes les ressources affectées à la tâche mère
            Tache tacheM = (Tache) session.load(Tache.class, idTache);
            for(Iterator iter = tacheM.getAressources().iterator(); iter.hasNext();) {
                ARessource r = (ARessource) iter.next();
                idRessM.add(r.getIdARessource());
            }
            
            //Parcourir les tâches filles du niveau suivant
            for(Iterator iter = filles.iterator(); iter.hasNext();) {
                Tache fille = (Tache) iter.next();
                //Liste toutes les ressources de la tâche fille
                for(Iterator iter2 = fille.getAressources().iterator(); iter2.hasNext();) {
                    ARessource ressource = (ARessource) iter2.next();
                    //Si la ressource à été déja listée alors inutile de l'ajouter
                    if(idRessF.contains(ressource.getIdARessource()) == false)
                        idRessF.add(ressource.getIdARessource());
                }
            }
            
            //Enlève les ressources qui sont dans les tâches filles
            for(Iterator iter = idRessF.iterator(); iter.hasNext();) {
                Integer idRess = (Integer) iter.next();
                
                if(idRessM.contains(idRess)) {
                    idRessM.remove(idRess);
                }
            }
            
            res = idRessM;
            
            tx.commit();
        }catch(HibernateException e) {
            e.printStackTrace();
        }
        
        return res;
    }
    
    /**
     * Obtenir l'identifiantd des ressources matérielles affectées à la tâche, on ne
     * prend pas en compte des affectations aux tâches filles
     * @param idTache
     * Identifiant de la tache
     * @return
     * Liste d'identifiants, à convertir en Integer
     */
    public static List obtenirARessources(int idTache) {
        List res = new ArrayList();
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction tx = null;
        
        try{
            tx = session.beginTransaction();
            
            Tache tache = (Tache) session.load(Tache.class, idTache);
            Iterator iter = tache.getAressources().iterator();
            for(;iter.hasNext();) {
                ARessource ressource = (ARessource) iter.next();
                res.add(ressource.getIdARessource());
            }
            
            tx.commit();
        }catch(HibernateException e) {
            e.printStackTrace();
        }
        
        return res;
    }
    
    /**
     * Evalue un état des tâches ainsi que les coûts correspondants
     * @param idProjet
     * Identifiant du projet
     * @param etat
     * Instance d'une classe EtatTaches
     */
    public static void EvaluerEtatTaches(int idProjet, EtatTaches etat) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        
        try{
            session.beginTransaction();
            
            List taches = session.createQuery("from Tache as t where t.projet.idProjet = " + idProjet).list();
            for(Iterator iter = taches.iterator(); iter.hasNext();) {
                Tache tache = (Tache) iter.next();
                float coutTache = TacheManager.getCoutTache(tache);
                
                if(coutTache != 0) {
                    EtatTache etatTache = new EtatTache();
                
                    etatTache.setNomTache(tache.getNomTache());
                    etatTache.setDebutTache(tache.getDebutTache());
                    etatTache.setFinTache(tache.getFinTache());
                    etatTache.setCout(coutTache);
                
                    etat.addValue(etatTache);
                }
            }
            
        }catch(HibernateException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Evalue les résultats de l'état des ressources de travail
     * @param idProjet
     * Identifiant du projet
     * @param etat
     * Instance d'une classe EtatRessources
     */
    public static void EvaluerEtatRessources(int idProjet, EtatRessources etat) {
        int duree = 0;
        float coutRes = 0;

        List res = ProjetManager.obtenirIdRessources(idProjet);
        for(Iterator iter = res.iterator(); iter.hasNext();) {
            Integer idRes = (Integer) iter.next();
            Ressource ressource = RessourceManager.chargerRessource(idRes);

            EtatRessource etatRessource = new EtatRessource();

            etatRessource.setNomRessource(ressource.getPseudo());
            duree = ProjetManager.obtenirTotalJourHomme(idProjet, ressource.getIdRessource());

            Poste poste = ressource.getPoste();
            coutRes = duree * PosteManager.chargerPoste(poste.getIdPoste()).getCoutPoste();
            etatRessource.setDuree(duree);
            etatRessource.setCout(coutRes);

            etat.addValue(etatRessource);
        }
    }
    
    /**
     * Evalue les résultats de l'état des ressources matérielles
     * @param idProjet
     * Identifiant du projet
     * @param etat
     * Instance d'une classe EtatRessources
     */
    public static void EvaluerEtatARessources(int idProjet, EtatARessources etat) {
        int duree = 0;
        float coutRes = 0;

        List res = ProjetManager.obtenirIdARessources(idProjet);
        for(Iterator iter = res.iterator(); iter.hasNext();) {
            Integer idRes = (Integer) iter.next();
            ARessource ressource = ARessourceManager.chargerARessource(idRes);

            EtatARessource etatARessource = new EtatARessource();

            etatARessource.setNomARessource(ressource.getNomARessource());
            duree = ProjetManager.obtenirTotalDureeUtilisation(idProjet, ressource.getIdARessource());

            coutRes = duree * ressource.getCoutARessource();
            etatARessource.setDuree(duree);
            etatARessource.setCout(coutRes);

            etat.addValue(etatARessource);
        }
    }
    
    /**
     * Evalue un état des tâches ainsi que les coûts correspondants.
     * Sauf qu'il effectue en plus le calcul des tâches qui one été selectionnées
     * @param idProjet
     * Identifiant du projet
     * @param etat
     * Instance d'une classe EtatTaches
     */
    public static void EvaluerEtatTachesParSelection(int idProjet, EtatTaches etat) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        
        try{
            session.beginTransaction();
            
            List taches = session.createQuery("from Tache as t where t.projet.idProjet = " + idProjet).list();
            for(Iterator iter = taches.iterator(); iter.hasNext();) {
                Tache tache = (Tache) iter.next();
                float coutTache = 0;
                
                if(tache.getEtat() == false)
                    coutTache = TacheManager.getCoutTache(tache);
                else
                    coutTache = TacheManager.getCoutTacheParSelection(tache);
                    
                if(coutTache != 0) {
                    EtatTache etatTache = new EtatTache();
                
                    etatTache.setNomTache(tache.getNomTache());
                    etatTache.setDebutTache(tache.getDebutTache());
                    etatTache.setFinTache(tache.getFinTache());
                    etatTache.setCout(coutTache);
                
                    etat.addValue(etatTache);
                }
            }
            
        }catch(HibernateException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Obtenir l'id de toutes les ressources affectées à un projet
     * @param idProjet
     * Identifiant du projet
     * @return
     * List des identifiants et non la liste des instances
     */
    public static List obtenirIdRessources(int idProjet) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        List idRes = new LinkedList();
        
        try{
            session.beginTransaction();
            
            List taches = session.createQuery("from Tache as t where t.projet.idProjet = " + idProjet).list();
            
            for(Iterator iter = taches.iterator(); iter.hasNext();) {
                Tache tache = (Tache) iter.next();
                
                for(Iterator iter2 = tache.getRessources().iterator(); iter2.hasNext();) {
                    Ressource ressource = (Ressource) iter2.next();
                    
                    //Si la ressource n'a pas encore été ajoutée, alors on l'ajoute
                    if(idRes.contains(ressource.getIdRessource()) == false)
                        idRes.add(ressource.getIdRessource());
                }
            }
            
        }catch(HibernateException e) {
            e.printStackTrace();
        }
        
        return idRes;
    }
    
    /**
     * Obtenir l'id de toutes les ressources matérielles affectées à un projet
     * @param idProjet
     * Identifiant du projet
     * @return
     * List des identifiants et non la liste des instances
     */
    public static List obtenirIdARessources(int idProjet) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        List idRes = new LinkedList();
        
        try{
            session.beginTransaction();
            
            List taches = session.createQuery("from Tache as t where t.projet.idProjet = " + idProjet).list();
            
            for(Iterator iter = taches.iterator(); iter.hasNext();) {
                Tache tache = (Tache) iter.next();
                
                for(Iterator iter2 = tache.getAressources().iterator(); iter2.hasNext();) {
                    ARessource ressource = (ARessource) iter2.next();
                    
                    //Si la ressource n'a pas encore été ajoutée, alors on l'ajoute
                    if(idRes.contains(ressource.getIdARessource()) == false)
                        idRes.add(ressource.getIdARessource());
                }
            }
            
        }catch(HibernateException e) {
            e.printStackTrace();
        }
        
        return idRes;
    }
    
    /**
     * Obtenir le total de la durée des tâches d'une ressource
     * @param idProjet
     * idenfitiant du projet
     * @param idRessource
     * Identifiant de la ressource
     * @return
     * Durée évalué en jour/homme
     */
    public static int obtenirTotalJourHomme(int idProjet, int idRessource) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        int res = 0;
        
        try{
            session.beginTransaction();
            
            List taches = session.createQuery("from Tache as t where t.projet.idProjet = " + idProjet).list();
            for(Iterator iter = taches.iterator(); iter.hasNext();) {
                Tache tache = (Tache) iter.next();
                
                List ressources = ProjetManager.obtenirRessourcesPropres(tache.getIdTache());
                for(Iterator iter2 = ressources.iterator(); iter2.hasNext();) {
                    Integer idRes = (Integer) iter2.next();
                    
                    if(idRes == idRessource) {
                        res += TacheManager.compterJoursSansDateFerie(tache);
                    }
                }
            }
            
        }catch(HibernateException e) {
            e.printStackTrace();
        }
        
        return res;
    }
    
    /**
     * Obtenir le total de la durée d'utilisation d'une ressource matérielle
     * @param idProjet
     * @param idARessource
     * @return
     */
    public static int obtenirTotalDureeUtilisation(int idProjet, int idARessource) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        int res = 0;
        
        try{
            session.beginTransaction();
            
            List taches = session.createQuery("from Tache as t where t.projet.idProjet = " + idProjet).list();
            for(Iterator iter = taches.iterator(); iter.hasNext();) {
                Tache tache = (Tache) iter.next();
                
                List ressources = ProjetManager.obtenirARessourcesPropres(tache.getIdTache());
                for(Iterator iter2 = ressources.iterator(); iter2.hasNext();) {
                    Integer idRes = (Integer) iter2.next();
                    
                    if(idRes == idARessource) {
                        res += TacheManager.compterJoursSansDateFerie(tache);
                    }
                }
            }
            
        }catch(HibernateException e) {
            e.printStackTrace();
        }
        
        return res;
    }
}
