/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package entitymanager;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.HibernateException;
import java.util.List;
import java.util.Iterator;        
import cfg.HibernateUtil;
import mapping.ExceptionRessource;

/**
 *
 * @author FireWolf
 * @since 05/01/08
 */
public abstract class ExceptionRessourceManager {
    public static void afficher() {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        
        try{
            session.beginTransaction();
            List res = session.createQuery("from ExceptionRessource").list();
            
            for(Iterator iter = res.iterator(); iter.hasNext();) {
                ExceptionRessource er = (ExceptionRessource) iter.next();
                System.out.println(" " + er.getIdRessource() + " " + er.getIdTache());
            }
        }catch(HibernateException e) {
            e.printStackTrace();
        }
    }
    
    public static void ajouter(ExceptionRessource er) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction tx = null;
        
        try{
            tx = session.beginTransaction();
            
            session.save(er);
            
            tx.commit();
            
        }catch(HibernateException e) {
            e.printStackTrace();
        }
    }
    
    public static void modifier(ExceptionRessource er) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction tx = null;
        
        try{
            tx = session.beginTransaction();
            
            session.update(er);
            
            tx.commit();
            
        }catch(HibernateException e) {
            e.printStackTrace();
        }
    }
    
    public static void supprimer(ExceptionRessource er) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction tx = null;
        
        try{
            tx = session.beginTransaction();
            
            session.delete(er);
            
            tx.commit();
            
        }catch(HibernateException e) {
            e.printStackTrace();
        }
    }
    
    public static ExceptionRessource charger(int id) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction tx = null;
        ExceptionRessource res = null;
        
        try{
            tx = session.beginTransaction();
            
            res = (ExceptionRessource) session.load(ExceptionRessource.class, id);
            
            tx.commit();
        }catch(HibernateException e) {
            e.printStackTrace();
        }
        
        return res;
    }
    
    public static void enleverException(int idTache, int idRessource) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction tx = null;
        
        try{
            tx = session.beginTransaction();
            
            String req = "from ExceptionRessource as e where e.idRessource = " + idRessource + " and " +
                    "idTache = " + idTache;
            ExceptionRessource er = (ExceptionRessource) session.createQuery(req).uniqueResult();
            
            if(er != null)
                session.delete(er);
            
            tx.commit();
        }catch(HibernateException e) {
            e.printStackTrace();
        }
    }
}
