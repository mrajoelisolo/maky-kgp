/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package entitymanager;

import cfg.HibernateUtil;
import java.util.List;
import java.util.Iterator;
import mapping.EtatARessource;
import mapping.EtatRessource;
import mapping.EtatTache;
import org.hibernate.HibernateException;
import org.hibernate.Transaction;
import org.hibernate.Session;

/**
 *
 * @author FireWolf
 */
public abstract class EtatARessourceManager {
    public static void afficherEtat() {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction tx = null;
        
        try{
            tx = session.beginTransaction();
            
            List res = session.createQuery("from EtatARessource").list();
            for(Iterator iter = res.iterator(); iter.hasNext();) {
                EtatTache etat = (EtatTache) iter.next();
                System.out.println("" + etat.getNomTache());
            }
            
            tx.commit();
        }catch(HibernateException e) {
            e.printStackTrace();
        }
    }
    
    public static void ajouter(EtatARessource etat) {
        Transaction tx = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        
        try{
            tx = session.beginTransaction();
            
            session.save(etat);
            
            tx.commit();
        }catch(HibernateException e) {
            e.printStackTrace();
            
            if(tx != null && tx.isActive())
                tx.rollback();
        }
    }
}
