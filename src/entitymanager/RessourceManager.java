package entitymanager;

import cfg.HibernateUtil;
import mapping.*;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.HibernateException;
import java.util.List;
import java.util.Iterator;
import java.util.LinkedList;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JTable;
import utile.MyRecordSet;
import utile.TableModel;

/**
 *
 * @author FireWolf
 */
public abstract class RessourceManager {
    private static List buffer = new LinkedList();
    
    public static void clearBuffer() {
        buffer.clear();
    }
    
    /**
     * Retourne le contenu de la mémoire tampon sous forme de liste
     * @return
     */
    public List getBuffer() {
        return buffer;
    }
    
    public static void afficher() {
        Transaction tx = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        
        try {
            tx = session.beginTransaction();
            
            List res = session.createQuery("from Ressource as r").list();
            for(Iterator iter = res.iterator(); iter.hasNext();) {
                Ressource r = (Ressource) iter.next();
                System.out.println(r.getIdRessource() + " " + r.getPseudo());
            }
            
            //tx.commit();
        }catch(HibernateException e) {
            e.printStackTrace();
        }
    }
    
    public static List chargerRessources() {
       Transaction tx= null;
       Session session = HibernateUtil.getSessionFactory().getCurrentSession();
       
       try{
           tx = session.beginTransaction();
           List res = session.createQuery("from Ressource").list();
           //tx.commit();
           
           return res;
       } catch(HibernateException e) {
           e.printStackTrace();
           
           if(tx != null && tx.isActive())
               tx.rollback();
           
           return null;
       }
    }
    
    public static void supprimerRessource(Ressource r) {
        Transaction tx = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        
        try {
            tx = session.beginTransaction();
            session.delete(r);
            tx.commit();
        } catch(HibernateException e) {
            e.printStackTrace();
            
            if(tx != null && tx.isActive()) {
                tx.rollback();
            }
        }
    }
    
    public static void creerRessource(Ressource r) {
        Transaction tx = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();

        try {
            tx = session.beginTransaction();
            session.save(r);
            tx.commit();
        } catch(HibernateException e) {
            e.printStackTrace();
            
            if(tx != null && tx.isActive())
                tx.rollback();
        }
    }
    
    public static void modifierRessource(Ressource r) {
        Transaction tx = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();

        try {
            tx = session.beginTransaction();
            session.update(r);
            tx.commit();
        } catch(HibernateException e) {
            e.printStackTrace();
            
            if(tx != null && tx.isActive())
                tx.rollback();
        }
    }
    
    public static Ressource chargerRessource(int id) {
        Transaction tx = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Ressource res = null;
        
        try {
            tx = session.beginTransaction();
            
            List r = session.createQuery("from Ressource as r where r.idRessource=" + id).list();
            res = (Ressource) r.get(0);
            
            //tx.commit();
        } catch(HibernateException e) {
           e.printStackTrace();
        }
        return res;
    }
    
    /**
     * Charge l'instance d'une ressource à partir de son pseudo
     * @param pseudo
     * Pseudo corresponant de la ressource
     * @return
     * Instance de la ressource
     */
    public static Ressource chargerRessource(String pseudo) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Ressource res = null;
        
        try{
            session.beginTransaction();
            
            res = (Ressource) session.createQuery("from Ressource where pseudo like '" + pseudo +"'").uniqueResult();
            
            session.getTransaction().commit();
        }catch(HibernateException e) {
            e.printStackTrace();
        }
        return res;
    }
    
    public static int chargerRessources(JTable maTable, MyRecordSet resultat) {
        Transaction tx = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        int nbLignes = 0;
        
        try {
            tx = session.beginTransaction();
            
            List res = session.createQuery("from Ressource").list();
            nbLignes = res.size();
            Object[][] ressources = new Object[res.size()][3];
            
            int i = 0;
            for(Iterator iter = res.iterator(); iter.hasNext();) {
                Ressource r = (Ressource) iter.next();
                
                ressources[i][0] = r.getIdRessource();
                ressources[i][1] = r.getPseudo();
                ressources[i][2] = r.getPoste().getNomPoste();
                i++;
            }
            
            //Chargement des données dans la table
            //Prévention contre les erreurs
            if(ressources.length == 0)
                ressources = new Object[1][3];
            
            String[] titres = {"identifiant", "Pseudo", "Poste"};
            TableModel modele = new TableModel(ressources, titres);
            
            maTable.setModel(modele);
            maTable.setRowSelectionAllowed(true);
            maTable.setRowSelectionInterval(0, 0);
            maTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
            maTable.setPreferredSize(maTable.getMaximumSize());
            
            //Chargement des données dans le recordset
            Object[] o = res.toArray();
            resultat.setValue(o);
            
            tx.commit();
        } catch(HibernateException e) {
            e.printStackTrace();
            
            if(tx != null && tx.isActive())
                tx.rollback();
        }
        return nbLignes;
    }
    
    public static int chargerRessources(JTable maTable, MyRecordSet resultat,
            String critere) {
        Transaction tx = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        int nbLignes = 0;
        
        try {
            tx = session.beginTransaction();
            
            String req = "from Ressource as r where r.pseudo like '" + critere + "'";
            List res = session.createQuery(req).list();
            nbLignes = res.size();
            Object[][] ressources = new Object[res.size()][3];
            
            int i = 0;
            for(Iterator iter = res.iterator(); iter.hasNext();) {
                Ressource r = (Ressource) iter.next();
                
                ressources[i][0] = r.getIdRessource();
                ressources[i][1] = r.getPseudo();
                ressources[i][2] = r.getPoste().getNomPoste();
                i++;
            }
            
            //Chargement des données dans la table
            //Prévention contre les erreurs
            if(ressources.length == 0)
                ressources = new Object[1][3];
            
            String[] titres = {"identifiant", "Pseudo", "Poste"};
            TableModel modele = new TableModel(ressources, titres);
            
            maTable.setModel(modele);
            maTable.setRowSelectionAllowed(true);
            maTable.setRowSelectionInterval(0, 0);
            maTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
            maTable.setPreferredSize(maTable.getMaximumSize());
            
            //Chargement des données dans le recordset
            Object[] o = res.toArray();
            resultat.setValue(o);
            
            tx.commit();
        } catch(HibernateException e) {
            e.printStackTrace();
            
            if(tx != null && tx.isActive())
                tx.rollback();
        }
        return nbLignes;
    }
    
    /**
     * Afficher les ressources dans un comboBox
     * @param jCombo
     * Le JComboBox a remplir
     */
    public static void chargerRessources(JComboBox jCombo) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        DefaultComboBoxModel modele = new DefaultComboBoxModel();
        
        try{
            session.beginTransaction();
            
            List res = session.createQuery("from Ressource").list();
            
            for(Iterator iter = res.iterator(); iter.hasNext();) {
                Ressource r = (Ressource) iter.next();
                modele.addElement(r.getPseudo());
            }
            
            jCombo.setModel(modele);
            
            session.getTransaction().commit();
        }catch(HibernateException e) {
            e.printStackTrace();
        }
    }
    
    //Ajouté le 30/11/2008
    /**
     * Vérifier si le pseudo éxiste déja dans la base
     * @param pseudo
     * Nom du pseudo
     * @return
     * Retourne true si le pseudo éxiste déja, false dans le cas echéant
     */
    public static boolean isPseudoExiste(String pseudo) {
        boolean res = false;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        
        try{
            session.beginTransaction();
            
            List resQuery = session.createQuery("from Ressource as r where r.pseudo like '%" + pseudo + "%'").list();
            if(!resQuery.isEmpty()) 
                res = true;
            
            session.getTransaction().commit();
        }catch(HibernateException e) {
                e.printStackTrace();  
        }
        
        return res;
    }
    
    /**
     * Vérifie si la ressource est déja affectée à la tâche
     * NB : La tâche doit être préalablement initialisée, afin d'éviter les
     * problèmes de proxy (problèmes de session)
     * @param t
     * La tâche qui affecte les ressources
     * @param r
     * La ressource à vérifier
     * @return
     * true si la ressource est déja affectée, false dans le cas echéant
     */
    public static boolean isRessourceAffectee(Tache tache, String r) {
        boolean res = false;
        
        for(Iterator iter = tache.getRessources().iterator(); iter.hasNext();) {
            Ressource ressource = (Ressource) iter.next();
            if(ressource.getPseudo().equals(r))
                res = true;
        }
        
        return res;
    }
    
    //OK : 30/11/08
    /**
     * Met à jour l'affectation des ressources sur les tâches mères
     * lorsqu'on affecte une ressource à une tâche fille
     * @param t
     * La tâche fille
     * @param r
     * La ressource à affecter
     */
    public static void mettreAJourAffectationDesc(Tache t, Ressource r) {
        if(t.getNiveau() != 1) {
            Tache mere = TacheManager.chargerTache(t.getPred());
            //Mises à jour            
            if(!RessourceManager.isRessourceAffectee(mere, r.getPseudo())) {
                mere.getRessources().add(r); //Affectation
                TacheManager.modifierTache(mere);
            }
            mettreAJourAffectationDesc(mere, r);
        }
    }
    
    /**
     * Obtenir l'instance d'une ressource d'une tâche
     * @param t
     * La tâche concernée
     * @param pseudo
     * Pseudo de la ressource à obtenir
     * @return
     * Ne retourne rien s'il ne trouve pas la ressource correspondante au pseudo
     */
    public static Ressource obtenirRessource(Tache t, String pseudo) {
        Ressource res = null;
        
        for(Iterator iter = t.getRessources().iterator(); iter.hasNext();) {
            Ressource r = (Ressource) iter.next();
            if(r.getPseudo().equals(pseudo))
                res = r;
        }
        
        return res;
    }
    
    /**
     * Desaffecte une ressource d'une tâche. La méhode ne fait rien
     * si la ressource n'est pas trouvée
     * Etat : OK
     * @param t
     * La tâche à desaffecter la ressource
     * @param r
     * La ressource à desaffecters
     */
    public static void desaffecterRessource(Tache t, int idRessource) {
        Ressource rech = null;
        
            for(Iterator iter = t.getRessources().iterator(); iter.hasNext();) {
                Ressource res = (Ressource) iter.next();
                if(idRessource == res.getIdRessource()) {
                    rech = res;
                    System.out.println("Desaffecté : " + res.getPseudo());
                    break;
                }
            }
            
            t.getRessources().remove(rech);
            TacheManager.modifierTache(t);
    }
    
    //is OK
    private static void obtenirArbre(Tache t, int idRessource) {
        if(t.getTachesFilles().size() != 0) {
            for(Iterator iter = t.getTachesFilles().iterator(); iter.hasNext();) {
                Tache fille = (Tache) iter.next();
                System.out.println("A supprimer : " + fille.getNomTache());
                buffer.add(fille);
                
                obtenirArbre(fille, idRessource);
            }
        }
    }
    
    /**
     * Desaffecter une ressource d'une tâche. Cela engendre également
     * le même effet pour les tâches filles. NB : Cette méthode n'a aucune
     * effet si la tâche n'a pas été initialisée avec la methode creerArborescence()
     * (This method has no effect if you don't initialize the Tache with creerArborescence()).
     * Si la ressource à desaffecter n'éxiste pas, la méthode ne fait rien
     * @param t
     */
    public static void desaffecterArborescence(Tache t, int idRessource) {
        clearBuffer();
        Tache racine = TacheManager.creerArborescence(t.getProjet().getIdProjet());
        Noeud rech = new Noeud();
        TacheManager.rechercher(racine, rech, t.getIdTache());
        obtenirArbre(rech.getTache(), idRessource);
        buffer.add(t); //Ajouter la mère, elle fait partie des tâches à desaffecter

        //Désaffecter les tâches filles
        for(Iterator iter = buffer.iterator(); iter.hasNext();) {
            Tache tSup = (Tache) iter.next();
            
            RessourceManager.desaffecterRessource(tSup, idRessource);
        }
        clearBuffer();
    }
}