package entitymanager;

import cfg.HibernateUtil;
import mapping.*;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.HibernateException;
import java.util.List;
import java.util.Iterator;

/**
 *
 * @author FireWolf
 */
public abstract class AgendaManager {
    /**
     * Affiche toutes les fichiers de la base
     */
    public static void afficher() {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        
        try {
            session.beginTransaction();
            
            List res = session.createQuery("from Agenda").list();
            
            for(Iterator iter = res.iterator(); iter.hasNext();) {
                Agenda a = (Agenda) iter.next();
                System.out.println(a.getDateAgenda() + " " + a.getDescription());
            }
            
            session.getTransaction().commit();
        }catch(HibernateException e) {
            e.printStackTrace();
        }
    }
}
