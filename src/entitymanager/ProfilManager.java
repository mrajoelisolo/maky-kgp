package entitymanager;

import cfg.HibernateUtil;
import mapping.*;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.HibernateException;
import java.util.List;
import java.util.Iterator;
import javax.swing.JTable;
import utile.MyRecordSet;
import utile.TableModel;

/**
 *
 * @author FireWolf
 */
public abstract class ProfilManager {
    
    /**
     * Crée un profile dans las base de données à partir de l'instance de Profil
     * @param p
     * Profil de l'utilisateur
     */
    public static void creerProfil(Profil p) {
        Transaction tx = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();

        try {
            tx = session.beginTransaction();
            session.save(p);
            tx.commit();
        } catch(HibernateException e) {
            e.printStackTrace();
            
            if(tx != null && tx.isActive())
                tx.rollback();
        }
    }
    
    /**
     * Modifier les propriétés d'un profil utilisateur
     * @param p
     * Instance du profil
     */
    public static void modifierProfil(Profil p) {
        Transaction tx = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();

        try {
            tx = session.beginTransaction();
            session.update(p);
            tx.commit();
        } catch(HibernateException e) {
            e.printStackTrace();
            
            if(tx != null && tx.isActive())
                tx.rollback();
        }
    }
    
    /**
     * Supprimer un profil de la base
     * @param p
     * Instance d'un profil
     */
    public static void supprimerProfil(Profil p) {
        Transaction tx = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        
        try {
            tx = session.beginTransaction();
            session.delete(p);
            tx.commit();
        } catch(HibernateException e) {
            e.printStackTrace();
            
            if(tx != null && tx.isActive()) {
                tx.rollback();
            }
        }
    }
    
    /**
     * Charger et crée une instance d'un profil utilisateur Profil
     * @param idProfil
     * identité du profil dans la base de données
     * @return
     * Instance de mapping.Profil
     */
    public static Profil chargerProfil(int idProfil) {
        Profil res = null;
        Transaction tx = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();

        try {
            tx = session.beginTransaction();
            
            List r = session.createQuery("from Profil as p where p.idProfil=" + idProfil).list();
            for(Iterator iter = r.iterator(); iter.hasNext();) {
                res = (Profil) iter.next();
            }
            
            //tx.commit();
        } catch(HibernateException e) {
            e.printStackTrace();
            
            if(tx != null && tx.isActive())
                tx.rollback();
        }
        return res;
    }
    
    /**
     * Charger et crée une instance d'un profil utilisateur Profil
     * @param idProfil
     * login du profil NB: un login est unique dans la base de données
     * @return
     * Instance de mapping.Profil
     */
    public static Profil chargerProfil(String login) {
        Profil res = null;
        Transaction tx = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        
        try {
            tx = session.beginTransaction();
            
            List r = session.createQuery("from Profil as p where p.login = '" + login + "'").list();
            for(Iterator iter = r.iterator(); iter.hasNext();) {
                res = (Profil) iter.next();
            }
            
            //tx.commit();
        } catch(HibernateException e) {
            e.printStackTrace();
            
            if(tx != null && tx.isActive())
                tx.rollback();
        }
        return res;
    }
    
    /**
     * Pour vérifier si le profil éxiste ou s'il est correct
     * @param login
     * login du profil
     * @param password
     * mot de passe
     * @return
     * true si le profil éxiste
     */
    public static boolean verifierProfil(String login, String password) {
        boolean res = false;
        Transaction tx = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();

        try {
            tx = session.beginTransaction();
            
            List r = session.createQuery("from Profil as p where p.login = '" + login + "' and p.password = '" + password + "'").list();
            if(r.size() > 0) res = true;
            
            tx.commit();
        } catch(HibernateException e) {
            e.printStackTrace();
            
            if(tx != null && tx.isActive())
                tx.rollback();
        }
        return res;
    }
    
    /**
     * Charger les profiles dans un JTable et dans un MyRecordSet
     * @param maTable
     * Instance d'un JTable
     * @param resultat
     * Instance d'un MyRecordSet
     * @return
     * Nombre d'enregistrements chargées
     */
    public static int chargerProfiles(JTable maTable, MyRecordSet resultat) {
        Transaction tx = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        int nbLignes;
        
        try {
            tx = session.beginTransaction();
            
            List res = session.createQuery("from Profil").list();
            Object[][] profils = new Object[res.size()][3];
            nbLignes = res.size();
            
            int i = 0;
            for(Iterator iter = res.iterator(); iter.hasNext();) {
                Profil p = (Profil) iter.next();
                
                profils[i][0] = p.getIdProfil();
                profils[i][1] = p.getLogin();
                profils[i][2] = p.getPrivilege();
                i++;
            }
            
            //Chargement des données dans la table
            //S'il n'y a aucun résultat : prévention contre les erreurs
            if(profils.length == 0)
                profils = new Object[1][3];
            
            String[] titres = {"identifiant", "login", "privilège"};
            TableModel modele = new TableModel(profils, titres);
            
            maTable.setModel(modele);
            maTable.setRowSelectionAllowed(true);
            maTable.setRowSelectionInterval(0, 0);
            maTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
            maTable.setPreferredSize(maTable.getMaximumSize());
            
            //Chargement des données dans le recordset
            Object[] o = res.toArray();
            resultat.setValue(o);
            
            tx.commit();
        } catch(HibernateException e) {
            e.printStackTrace();
            
            if(tx != null && tx.isActive())
                tx.rollback();
            
            nbLignes = 0;
        }
        return nbLignes;
    }
    
    public static int chargerProfiles(JTable maTable, MyRecordSet resultat,
            String critere) {
        Transaction tx = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        int nbLignes;
        
        try {
            tx = session.beginTransaction();
            
            String req = "from Profil as p where p.login like '" + critere + "'";
            List res = session.createQuery(req).list();
            Object[][] profils = new Object[res.size()][3];
            nbLignes = res.size();
            
            int i = 0;
            for(Iterator iter = res.iterator(); iter.hasNext();) {
                Profil p = (Profil) iter.next();
                
                profils[i][0] = p.getIdProfil();
                profils[i][1] = p.getLogin();
                profils[i][2] = p.getPrivilege();
                i++;
            }
            
            //Chargement des données dans la table
            //S'il n'y a aucun résultat : prévention contre les erreurs
            if(profils.length == 0)
                profils = new Object[1][3];
            
            String[] titres = {"identifiant", "login", "privilège"};
            
            TableModel modele = new TableModel(profils, titres);
            
            maTable.setModel(modele);
            maTable.setRowSelectionAllowed(true);
            maTable.setRowSelectionInterval(0, 0);
            maTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
            maTable.setPreferredSize(maTable.getMaximumSize());
            
            //Chargement des données dans le recordset
            Object[] o = res.toArray();
            resultat.setValue(o);
            
            tx.commit();
        } catch(HibernateException e) {
            e.printStackTrace();
            
            if(tx != null && tx.isActive())
                tx.rollback();
            
            nbLignes = 0;
        }
        return nbLignes;
    }
}
