package entitymanager;

import cfg.HibernateUtil;
import mapping.*;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.HibernateException;
import java.util.List;
import java.util.Iterator;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JTable;
import javax.swing.JComboBox;
import org.hibernate.ObjectNotFoundException;
import utile.MyRecordSet;
import utile.TableModel;

/**
 *
 * @author FireWolf
 */
public abstract class PosteManager {
    /**
     * Afficher toutes les postes de la base
     */
    public static void afficher() {
        Transaction tx = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        
        try{
            tx = session.beginTransaction();
            
            List res = session.createQuery("from Poste").list();
            for(Iterator iter = res.iterator(); iter.hasNext();) {
                Poste p = (Poste) iter.next();
                System.out.println(p.getNomPoste());
            }
            tx.commit();
        }catch(HibernateException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Charger une poste de la base
     * @param id
     * Identité du poste
     * @return
     * Instance de la poste
     */
    public static Poste chargerPoste(int id) {
        Transaction tx = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Poste res = null;
        
        try{
            tx = session.beginTransaction();
            
            res = (Poste) session.load(Poste.class, id);  

        }catch(HibernateException e) {
            e.printStackTrace();
        }
        
        return res;
    }
    
    /**
     * Créer une poste dans la base
     * @param p
     * Instance de la poste
     */
    public static void creerPoste(Poste p) {
        Transaction tx = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        
        try{
            tx = session.beginTransaction();
            
            session.save(p);
            
            tx.commit();
        }catch(HibernateException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Modifier une poste de la base
     * @param p
     * Instance de la base
     */
    public static void modifierPoste(Poste p) {
        Transaction tx = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();

        try {
            tx = session.beginTransaction();
            
            session.update(p);
            
            tx.commit();
        } catch(HibernateException e) {
            e.printStackTrace();
            
            if(tx != null && tx.isActive())
                tx.rollback();
        }
    }
    
    /**
     * Supprimer une poste de la base
     * @param p
     * Instance d'une poste
     */
    public static void supprimerPoste(Poste p) {
        Transaction tx = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();

        try {
            tx = session.beginTransaction();
            session.delete(p);
            tx.commit();
        } catch(HibernateException e) {
            e.printStackTrace();
            
            if(tx != null && tx.isActive())
                tx.rollback();
        }
    }
   
    /**
     * Charger tous les postes dans un RecordSet
     * @param maTable
     * Composant JTable pour afficher les résultats
     * @param resultat
     * Instance de la classe MyRecordSet pour contenir les résulats paralleles
     */
    public static int chargerPostes(JTable maTable, MyRecordSet resultat) {
        Transaction tx = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        int nbLignes = 0;
        
        try {
            tx = session.beginTransaction();
            
            List res = session.createQuery("from Poste").list();
            nbLignes = res.size();
            Object[][] postes = new Object[res.size()][3];
            
            int i = 0;
            for(Iterator iter = res.iterator(); iter.hasNext();) {
                Poste p = (Poste) iter.next();
                
                postes[i][0] = p.getIdPoste();
                postes[i][1] = p.getNomPoste();
                postes[i][2] = p.getCoutPoste();
                i++;
            }
            
            //Chargement des données dans la table
            //Prévention contre les erreurs
            if(postes.length == 0)
                postes = new Object[1][3];
            
            String[] titres = {"identifiant", "nom", "cout"};
            TableModel modele = new TableModel(postes, titres);
            
            maTable.setModel(modele);
            maTable.setRowSelectionAllowed(true);
            maTable.setRowSelectionInterval(0, 0);
            maTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
            maTable.setPreferredSize(maTable.getMaximumSize());
            
            //Chargement des données dans le recordset
            Object[] o = res.toArray();
            resultat.setValue(o);
            
            tx.commit();
        } catch(HibernateException e) {
            e.printStackTrace();
            
            if(tx != null && tx.isActive())
                tx.rollback();
        }
        return nbLignes;
    }
    
    /**
     * Charger les postes en fonction du critère spécifié
     * @param maTable
     * Composant JTable pour afficher les résultats
     * @param resultat
     * Instance de la classe MyRecordSet pour contenir les résulats paralleles
     * @param critere
     * La critère à spécifier, par exemple tous les postes dont le nom 
     * commence par dev, on spécifie "dev%"
     */
    public static int chargerPostes(JTable maTable, MyRecordSet resultat,
            String critere) {
        Transaction tx = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        int nbLignes = 0;
        
        try {
            tx = session.beginTransaction();
            
            String req = "from Poste as p where p.nomPoste like '" + critere + "'";
            List res = session.createQuery(req).list();
            nbLignes = res.size();
            Object[][] postes = new Object[res.size()][3];
            
            int i = 0;
            for(Iterator iter = res.iterator(); iter.hasNext();) {
                Poste p = (Poste) iter.next();
                
                postes[i][0] = p.getIdPoste();
                postes[i][1] = p.getNomPoste();
                postes[i][2] = p.getCoutPoste();
                i++;
            }
            
            //Chargement des données dans la table
            //Prévention contre les erreurs
            if(postes.length == 0)
                postes = new Object[1][3];
            
            String[] titres = {"identifiant", "nom", "cout"};
            TableModel modele = new TableModel(postes, titres);
            
            maTable.setModel(modele);
            maTable.setRowSelectionAllowed(true);
            maTable.setRowSelectionInterval(0, 0);
            maTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
            maTable.setPreferredSize(maTable.getMaximumSize());
            
            //Chargement des données dans le recordset
            Object[] o = res.toArray();
            resultat.setValue(o);
            
            tx.commit();
        } catch(HibernateException e) {
            e.printStackTrace();
            
            if(tx != null && tx.isActive())
                tx.rollback();
        }
        return nbLignes;
    }
    
    /**
     * Afficher les postes dans un comboBox
     * @param jCombo
     * Le JComboBox a remplir
     */
    public static void chargerPostes(JComboBox jCombo) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        DefaultComboBoxModel modele = new DefaultComboBoxModel();
        
        try{
            session.beginTransaction();
            
            List res = session.createQuery("from Poste").list();
            
            for(Iterator iter = res.iterator(); iter.hasNext();) {
                Poste p = (Poste) iter.next();
                modele.addElement(p.getNomPoste());
            }
            
            jCombo.setModel(modele);
            
            session.getTransaction().commit();
        }catch(HibernateException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Obtenir l'identifiant d'une poste dans la base de données
     * @param nomPoste
     * Nom de poste éxistant
     * @return
     * Identifiant
     */
    public static int getIdPoste(String nomPoste) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        int res = 0;
        
        try{
            session.beginTransaction();
            
            List p = session.createQuery("from Poste as p where p.nomPoste = '" + nomPoste + "'").list();
            
            if(!p.isEmpty()) {
                Poste poste = (Poste) p.get(0); 
                res = poste.getIdPoste();
            }
        }catch(HibernateException e) {
            e.printStackTrace();
        }
        
        return res;
    }
    
    public static Poste getPoste(String nomPoste) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Poste res = null;
        
        try{
            session.beginTransaction();
            
            List p = session.createQuery("from Poste as p where p.nomPoste = '" + nomPoste + "'").list();
            
            if(!p.isEmpty()) {
                res = (Poste) p.get(0);
            }
        }catch(HibernateException e) {
            e.printStackTrace();
        }
        
        return res;
    }
}
 