package entitymanager;

import org.hibernate.Transaction;
import cfg.HibernateUtil;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Vector;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTree;
import javax.swing.tree.TreePath;
import mapping.*;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import utile.MyRecordSet;
import utile.ConversionDate;
import lwcomponents.treetable.LWColumn;
import lwcomponents.treetable.DynamicTreeTableModel;
import lwcomponents.treetable.JTreeTable;
import lwcomponents.treetable.TreeTableModel;
import utile.TableModel;

/**
 *
 * @author FireWolf
 */
public abstract class TacheManager {
    //27/11/08
    //Pour stocker des données temporairement
    private static List buffer = new LinkedList();
    
    //05/11/08 AM
    
    /**
     * Efface la mémoire tampon de la classe
     */
    public static void clearBuffer() {
        buffer.clear();
    }
    
    /**
     * Retourne le contenu de la mémoire tampon sous forme de liste
     * @return
     */
    public List getBuffer() {
        return buffer;
    }
    
    /**
     * afficher les tâches d'un projet
     * @param idProjet
     * id du projet, valeur entière
     */
    public static void afficherTaches(int idProjet) {
        Transaction tx = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        
        try{
            tx = session.beginTransaction();
            List taches = session.createCriteria(Tache.class).add(Restrictions.eq("projet.idProjet", idProjet)).list();
            
            for(Iterator iter = taches.iterator(); iter.hasNext();) {
                Tache tache = (Tache) iter.next();
                System.out.println(tache.getIdTache() + " " + tache.getNomTache() + " " + tache.getPred() + " " + tache.getNiveau());
            }
            tx.commit();
            
        } catch(HibernateException e) {
            e.printStackTrace();
            
            if(tx != null && tx.isActive())
                tx.rollback();
        }
    }

    /**
     * Afficher les tâches à un niveau précis d'un projet
     * @param idProjet
     * id du projet, valeur entière
     * @param n
     * niveau des tâches (au niveau de l'arborescence)
     * @return
     * true si le niveau contient au moins une tâche
     */
    public static boolean afficherTachesParNiveau(int idProjet, int n) {
        Transaction tx = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        boolean res = true;
        
        try{
            tx = session.beginTransaction();
            List taches = session.createCriteria(Tache.class)
                    .add(Restrictions.and(Restrictions.eq("projet.idProjet", idProjet), Restrictions.eq("niveau", n))).list();
            
            System.out.println("Tâches de niveau " + n);
            if(taches.size() == 0) {
                res = false;
                System.out.print(" vide");
            }
            for(Iterator iter = taches.iterator(); iter.hasNext();) {
                Tache tache = (Tache) iter.next();
                System.out.println(tache.getIdTache() + " " + tache.getNomTache() + " " + tache.getPred());
            }
            
            tx.commit();
            
        } catch(HibernateException e) {
            e.printStackTrace();
            
            if(tx != null && tx.isActive())
                tx.rollback();
        }
        return res;
    }
    
    public static List chargerTachesParNiveau(int idProjet, int n) {
        Transaction tx = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        List res;
        
        try{
            tx = session.beginTransaction();
            res = session.createCriteria(Tache.class)
                    .add(Restrictions.and(Restrictions.eq("projet.idProjet", idProjet), Restrictions.eq("niveau", n))).list();
            
            //tx.commit();
            
            return res;
            
        } catch(HibernateException e) {
            e.printStackTrace();
            
            if(tx != null && tx.isActive())
                tx.rollback();
            
            return null;
        }
    }
    
    /**
     * Compte le nombre de niveaux de l'arborescence des tâches du projet
     * @param idProjet
     * id du projet
     * @return
     * nombre de niveaux de l'arborescence
     */
    public static int nombreDeNiveaux(int idProjet) {
        Transaction tx = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Integer nb = 0;
        
        try{
            tx = session.beginTransaction();
            List taches = session.createQuery("select max(niveau) from Tache as t where t.projet.idProjet = " + idProjet).list();
            
            for(Iterator iter = taches.iterator(); iter.hasNext();) {
                nb = (Integer) iter.next();
            }
                
            tx.commit();
            
            return nb;
            
        } catch(HibernateException e) {
            e.printStackTrace();
            
            if(tx != null && tx.isActive())
                tx.rollback();
            return nb;
        }
    }
    
    /**
     * Charge une instance d'une tâche à partir de son identifiant
     * Attention : ne pas appeler les méthodes qui appellent les collections
     * (getRessources(), etc ... Cela engendre une erreur car la session
     * est fermée dès que l'instanciation est effectuée (Pour économiser)
     * @param id
     * @return
     */
    public static Tache chargerTache(int id) {
        Transaction tx = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Tache res = null;
        
        try {
            tx = session.beginTransaction();
            
            List t = session.createQuery("from Tache as t where t.idTache=" + id).list();
            res = (Tache) t.get(0);
            
            //tx.commit();
        } catch(HibernateException e) {
            e.printStackTrace();
            
            if(tx != null && tx.isActive())
                tx.rollback();
        }
        return res;
    }
    
    public static List obtenirRessources(int idTache) {
        Transaction tx = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        List res = new LinkedList();
        
        try{
            tx = session.beginTransaction();
            
            Tache t = (Tache) session.load(Tache.class, idTache);
            for(Iterator iter = t.getRessources().iterator(); iter.hasNext();) {
                Ressource r = (Ressource) iter.next();
                res.add(r);
            }
            
            tx.commit();
        }catch(HibernateException e) {
            e.printStackTrace();
        }
        return res;
    }
    
     //05/11/08 PM
    
    /**
     * Permet de charger les tâches d'un projet dans une composant JTree
     * @param idProjet
     * id du projet
     * @return
     * Instance de la classe javax.swing.tree.DefaultMutableTreeNode;
     */
    public static Noeud chargerTaches(int idProjet) {
        Noeud[][] noeuds;
        
        int nbNiveaux = TacheManager.nombreDeNiveaux(idProjet);
        noeuds = new Noeud[nbNiveaux][];
        
        for(int niv = 1; niv <= nbNiveaux; niv++) {
            List sel = TacheManager.chargerTachesParNiveau(idProjet, niv);
            noeuds[niv - 1] = new Noeud[sel.size()];
            
            int i = 0;
            for(Iterator iter = sel.iterator(); iter.hasNext();) {
                Tache noeud = (Tache) iter.next();
                noeuds[niv - 1][i] = new Noeud(noeud.getNomTache());
                noeuds[niv - 1][i].setTache(noeud);
                i++;
            }   
        }
        
        for(int niv = 1; niv < nbNiveaux; niv++) {
            
            for(int i = 0; i < noeuds[niv - 1].length; i++) {
                for(int j = 0; j < noeuds[niv].length; j++) {
                    if(noeuds[niv - 1][i].getTache().getIdTache() == noeuds[niv][j].getTache().getPred())
                        noeuds[niv - 1][i].add(noeuds[niv][j]);
                }
            }
        }  
        
        return noeuds[0][0];
    }
    
 /*   public static List chargerTachesIntervalle(Date debut, Date fin) {
        Transaction tx = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        
        try{
            tx = session.beginTransaction();
            
            String req = "from Tache as t where ";
            
            tx.commit();
        } catch(HibernateException e) {
            e.printStackTrace();
            
            if(tx != null && tx.isActive())
                tx.rollback();
        }
    }*/
    
    //06-11-2008
    public static void ajouterTache(Tache t) {
        Transaction tx = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        
        try{
            tx = session.beginTransaction();
            session.save(t);
            tx.commit();
        } catch(HibernateException e) {
            e.printStackTrace();
            
            if(tx != null && tx.isActive())
                tx.rollback();
        }
    }
    
    public static void ajouterTache(Tache t, int idProjet) {
        Transaction tx = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        
        try{
            tx = session.beginTransaction();
            Tache tache = t;
            tache.getProjet().setIdProjet(idProjet);
            session.save(tache);
            tx.commit();
            
        } catch(HibernateException e) {
            e.printStackTrace();
            
            if(tx != null && tx.isActive())
                tx.rollback();
        }
    }
    
    public static void supprimerTache(Tache t) {
        Transaction tx = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        
        try{
            tx = session.beginTransaction();
            session.delete(t);
            tx.commit();
        } catch(HibernateException e) {
            e.printStackTrace();
            
            if(tx != null && tx.isActive())
                tx.rollback();
        }
    }
    
    public static void modifierTache(Tache t) {
        Transaction tx = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        
        try{
            tx = session.beginTransaction();
            
            session.merge(t);
            
            tx.commit();
            
        } catch(HibernateException e) {
            e.printStackTrace();
            
            if(tx != null && tx.isActive())
                tx.rollback();
        }
    }
    
    /**
     * Deprecated method
     * @param t
     */
    public static void suppimerTachesSuccesseurs(Tache t) {
        Transaction tx = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        
        try{
            tx = session.beginTransaction();
            
            String HQLDelete = "delete from Tache t where t.pred = :vPred";
            int etat = session.createQuery(HQLDelete).setString("vPred", "" + t.getIdTache()).executeUpdate();
            
            tx.commit();
            
        } catch(HibernateException e) {
            e.printStackTrace();
            
            if(tx != null && tx.isActive()) 
                tx.rollback();
        }
    }
    
    /**
     * Retourne la liste des tâches visibles sur l'arbre
     * @param jTree
     * Instance de la classe javax.swing.JTree;
     * @return
     * Liste des tâches visibles, les convertir en mapping.Tache
     */
    public static List parcourirArbre(JTree jTree) {
        int n = jTree.getRowCount();
        List res = null;
        if(n > 0) res = new LinkedList();
        
        for(int i = 0; i < n; i++) {
            TreePath tp = jTree.getPathForRow(i);
            Noeud noeud = (Noeud) tp.getLastPathComponent();
            Tache tache = noeud.getTache();
            res.add(tache);
        }
        
        return res;
    }
    
    /**
     * Crée une arborescence de tâches
     * @param idProjet
     * Identité du projet
     * @return
     * La tâche qui constitue la racine
     */
    public static Tache creerArborescence(int idProjet) {
        Tache res = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        
        try{
            session.beginTransaction();
            
            List[] noeuds;
            int nbNiveaux = TacheManager.nombreDeNiveaux(idProjet);
            noeuds = new LinkedList[nbNiveaux];
            
            for(int niv = 1; niv <= nbNiveaux; niv++) {
                List sel = TacheManager.chargerTachesParNiveau(idProjet, niv);
                noeuds[niv - 1] = new LinkedList();
                
                for(Iterator iter = sel.iterator(); iter.hasNext();) {
                    Tache noeud = (Tache) iter.next();
                    noeuds[niv - 1].add(noeud);
                }
            }
            
            for(int niv = 1; niv < nbNiveaux; niv++) {
                for(int i = 0; i < noeuds[niv - 1].size(); i++) {
                    for(int j = 0; j < noeuds[niv].size(); j++) {
                        Tache mere = (Tache) noeuds[niv - 1].get(i);
                        Tache fille = (Tache) noeuds[niv].get(j);
                        
                        if(mere.getIdTache() == fille.getPred())
                           mere.getTachesFilles().add(fille);
                    }
                }
            }
            
            res = (Tache) noeuds[0].get(0);
        
        }catch(HibernateException e) {
            e.printStackTrace();
        }
        
        
        return res;
    }
    
    /**
     * Afficher tout le contenu de l'arborescence des tâches
     * Si vous voulez effacer tout le contenu de la tâche, utilisez
     * la méthode supprimerArborescence()
     * @param t
     * Tâche racine
     */
    public static void afficher(Tache t) {
	if(t.getTachesFilles().size() != 0) {
            List res = t.getTachesFilles();
            for(Iterator iter = res.iterator(); iter.hasNext();) {
                Tache f = (Tache) iter.next();
                System.out.println(f.getNomTache());
                
                afficher(f);
            }
	}
    }
    
    /**
     * Obtenir l'arborescence d'une tâche sous forme de liste; cette liste est 
     * enregistrée dans la mémoire tampon de la classe.
     * Pour obtenir la liste, utiliser la méthode getBuffer()
     * Avant d'obtenir l'arborescence d'une tâche, invoquez d'abord
     * la méthode clearBuffer() pour réinitialiser la mémoire tampon
     * NB : Cette méthode doit être utilisée avec la méthode
     * creerArborescence()
     * @param t
     * Instance d'une tâche créée avec la méthode creerArborescence()
     * @param liste
     * Liste de l'arborescence
     */
    private static void obtenirArborescence(Tache t) {
	if(t.getTachesFilles().size() != 0) {
            List res = t.getTachesFilles();
            for(Iterator iter = res.iterator(); iter.hasNext();) {
                Tache f = (Tache) iter.next();
                buffer.add(f);
                
                obtenirArborescence(f);
            }
	}
    }
    
    /**
     * Supprimme la tâche ainsi que toutes les tâche filles
     * @param t
     * Instance d'une arborescence initialisée à partir de la méthode 
     * creerArborescence()
     */
    public static void supprimerArborescence(Tache t) {
        Transaction tx = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        
        clearBuffer();
        obtenirArborescence(t);
        
        try{
            tx = session.beginTransaction();
             
            for(Iterator iter = buffer.iterator(); iter.hasNext();) {
                Tache sel = (Tache) iter.next();
                session.delete(sel);
            }
            session.delete(t);
            
            tx.commit();
            clearBuffer();
        }catch(HibernateException e) {
            e.printStackTrace();
            
            if(tx != null && tx.isActive())
                tx.rollback();
        }
    }
 
    /**
     * Recherche la tâche dans l'arborescence et la retourne
     * Cette méthode n'a aucun effet si vous ne chargez pas
     * les tâches avec la méthode créerArborescence()
     * @param idTache
     * Id unique de la tâche
     * @return
     * Instance de la tâche recherchée
     */
    public static void rechercher(Tache racine, Noeud rech, int idTache) {        
        if(racine.getIdTache() == idTache)
            rech.setTache(racine);
        else if(racine.getTachesFilles().size() != 0) {
                List res = racine.getTachesFilles();
                Tache f;
                for(Iterator iter = res.iterator(); iter.hasNext();) {
                    f = (Tache) iter.next();

                    rechercher(f, rech, idTache);
                }
            }
    }
    
    public static int chargerTaches(int idProjet, JTable maTable, MyRecordSet resultat) {
        Transaction tx = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        int nbLignes;
        
        try {
            tx = session.beginTransaction();
            
            List res = session.createQuery("from Tache as t where t.idProjet = " + idProjet).list();
            Object[][] taches = new Object[res.size()][3];
            nbLignes = res.size();
            
            int i = 0;
            for(Iterator iter = res.iterator(); iter.hasNext();) {
                Tache t = (Tache) iter.next();
                taches[i][0] = ConversionDate.dateToString(t.getDebutTache());
                taches[i][1] = ConversionDate.dateToString(t.getFinTache());
                taches[i][2] = TacheManager.compterJoursSansDateFerie(t);
                i++;
            }
            
            //Chargement des données dans la table
            //S'il n'y a aucun résultat : prévention contre les erreurs
            if(taches.length == 0)
                taches = new Object[1][3];
            
            String[] titres = {"Début", "Fin", "Durée"};
            TableModel modele = new TableModel(taches, titres);
            
            maTable.setModel(modele);
            maTable.setRowSelectionAllowed(true);
            maTable.setRowSelectionInterval(0, 0);
            maTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
            maTable.setPreferredSize(maTable.getMaximumSize());
            
            //Chargement des données dans le recordset
            Object[] o = res.toArray();
            resultat.setValue(o);
            
            tx.commit();
        } catch(HibernateException e) {
            e.printStackTrace();
            
            if(tx != null && tx.isActive())
                tx.rollback();
            
            nbLignes = 0;
        }
        return nbLignes;
    }
    
    /**
     * Mettre à jour les dates des tâches à partir de la tâche FILLE à la tâche MERE;
     * Le chemin est descendant
     * @param t
     */
    public static void mettreAJourDateCheminDesc(Tache t) {
	if(t.getNiveau() != 1) {
            Tache mere = TacheManager.chargerTache(t.getPred());
            //Mises à jour
            if(t.getFinTache().after(mere.getFinTache())) {
                mere.setFinTache(t.getFinTache()); //Met à jour la date de la tâche mère;
                TacheManager.modifierTache(mere);
            }
            if(t.getDebutTache().before(mere.getDebutTache())) {
                mere.setDebutTache(t.getDebutTache()); //Met à jour la date de la tâche mère;
                TacheManager.modifierTache(mere);
            }
            mettreAJourDateCheminDesc(mere);
        }
    }
    
    /**
     * Mettre à jour les dates des tâches à partir de la tâche MERE jusqu'aux
     * tâches FILLES; Le chemin est ascendant
     * @param t
     */
    public static void mettreAJourDateCheminAsc(Tache t) {
        Tache racine = TacheManager.creerArborescence(t.getProjet().getIdProjet());
        Noeud mere = new Noeud();
        TacheManager.rechercher(racine, mere, t.getIdTache());
        clearBuffer();
        TacheManager.obtenirArborescence(mere.getTache());
        
        for(Iterator iter = buffer.iterator(); iter.hasNext();) {
            Tache fille = (Tache) iter.next();
            
            if(fille.getDebutTache().before(t.getDebutTache())) {
                fille.setDebutTache(t.getDebutTache());
                TacheManager.modifierTache(fille);
            }
            if(fille.getFinTache().after(t.getFinTache())) {
                fille.setFinTache(t.getFinTache());
                TacheManager.modifierTache(fille);
            }
        }
        clearBuffer();
    }
    
    /**
     * Afficher les noeuds visibles d'un arbre(JTree) dans une table(JTable)
     * @param jtree
     * @param table
     * @return
     * Nombre de noeuds visibles
     */
    public static int jTreeToJTable(JTree jtree, JTable table) {
        List res = TacheManager.parcourirArbre(jtree);
        
        Object[][] taches = new Object[res.size()][1];

        int i = 0;
        for(Iterator iter = res.iterator(); iter.hasNext();) {
            Tache t = (Tache) iter.next();
            String suffixe = "jours";
            if(TacheManager.compterJoursSansDateFerie(t) < 1)
                suffixe = "jour";
            
            taches[i][0] = TacheManager.compterJoursSansDateFerie(t) + " " + suffixe;
            i++;
        }

        //Chargement des données dans la table
        //S'il n'y a aucun résultat : prévention contre les erreurs
        if(taches.length == 0)
            taches = new Object[1][3];

        String[] titres = {"Durée"};
        TableModel modele = new TableModel(taches, titres);

        table.setModel(modele);
        table.setRowSelectionAllowed(true);
        table.setRowSelectionInterval(0, 0);
        table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
        table.setPreferredSize(table.getMaximumSize());
        
        return res.size();
    }
    
    /**
     * Calcule la durée d'une tâche en prenant compte des jours feriés dans la base
     * @param t
     * Tâche préalablement initialisé
     * @return
     * Nombre de jours
     */
    public static int compterJoursSansDateFerie(Tache t) {
        int duree = 0;
        Date date1 = t.getDebutTache();
        Date date2 = t.getFinTache();
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        
        duree = ConversionDate.nbJours(date1, date2);
        
        try{
            session.beginTransaction();
            
            int j1, j2, m1, m2;
            
            Calendar cld = Calendar.getInstance();
            cld.setTime(date1);
            j1 = cld.get(Calendar.DAY_OF_MONTH);
            m1 = cld.get(Calendar.MONTH) + 1;
            cld.setTime(date2);
            j2 = cld.get(Calendar.DAY_OF_MONTH);
            m2 = cld.get(Calendar.MONTH) + 1;
            
            //System.out.println("Jours " + j1 + " " + j2);
            //System.out.println("Mois " + m1 + " " + m2);
            
            String req = "from DateFerie where jourFerie between '" + j1 + "' and '" + j2 + "' and" +
                    " moisFerie between '" + m1 + "' and '" +  m2 + "'";
            
            List res = session.createQuery(req).list();
            duree -= res.size();
            
            //tx.commit();
        }catch(HibernateException e) {
            e.printStackTrace();
        }
        
        return duree;
    }
    
    /**
     * Obtenir les feuilles d'un projet, les feulles sont des tâches qui n'ont
     * pas de tâches filles
     * @param racine
     * Tâche racine représentant le projet
     * @return
     * Liste des feuilles
     * Création : 10/12/08
     * Validation : 10/12/08
     */
    public static List ObtenirFeuilles(int idProjet) {
        List res = new LinkedList();
        
        Tache taches = TacheManager.creerArborescence(idProjet);
        clearBuffer();
        TacheManager.obtenirArborescence(taches);
        for(Iterator iter = buffer.iterator(); iter.hasNext();) {
            Tache tache = (Tache) iter.next();
            if(tache.getTachesFilles().size() == 0)
                res.add(tache);
        }
        clearBuffer();   
        
        return res;
    }
    
    
    
    public static List chargerListeTaches(int idProjet) {
        List res = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction tx = null;
                
        try{
            tx = session.beginTransaction();
            
            res = session.createQuery("from Tache as t where t.projet.idProjet = " + idProjet).list();
            
            tx.commit();
        }catch(HibernateException e) {
            e.printStackTrace();
        }
        return res;
    }
    
    /**
     * @since 18/12/2008
     * Evalue le cout d'une tâche en prenant compte des ressources matérielles 
     * et ressources de travail
     * @param tache
     * Une tâche préalablement initialisée
     * @return
     * Le cout de la tâche
     */
    public static float getCoutTache(Tache tache) {
        float res = 0;
        int duree = 0;
        
        duree = TacheManager.compterJoursSansDateFerie(tache);
        
        List ressources = ProjetManager.obtenirRessourcesPropres(tache.getIdTache());
        for(Iterator iter = ressources.iterator(); iter.hasNext();) {
            Integer idRes = (Integer) iter.next();
            Ressource ressource = RessourceManager.chargerRessource(idRes);
            
            res += ressource.getPoste().getCoutPoste() * duree;
        }
        
        List aressources = ProjetManager.obtenirARessourcesPropres(tache.getIdTache());
        for(Iterator iter = aressources.iterator(); iter.hasNext();) {
            Integer idRes = (Integer) iter.next();
            ARessource aRessource = ARessourceManager.chargerARessource(idRes);
            
            res += aRessource.getCoutARessource() * duree;
        }
        
        return res;
    }
    
    
    /**
     * @since 20/12/2008
     * Evalue le cout d'une tâche en prenant compte des ressources matérielles 
     * et ressources de travail
     * @param tache
     * Une tâche préalablement initialisée
     * @return
     * Le cout de la tâche
     */
    public static float getCoutTacheParSelection(Tache tache) {
        float res = 0;
        int duree = 0;
        
        duree = TacheManager.compterJoursSansDateFerie(tache);
        
        List ressources = ProjetManager.obtenirRessources(tache.getIdTache());
        for(Iterator iter = ressources.iterator(); iter.hasNext();) {
            Integer idRes = (Integer) iter.next();
            Ressource ressource = RessourceManager.chargerRessource(idRes);
            
            res += ressource.getPoste().getCoutPoste() * duree;
        }
        
        List aressources = ProjetManager.obtenirARessources(tache.getIdTache());
        for(Iterator iter = aressources.iterator(); iter.hasNext();) {
            Integer idRes = (Integer) iter.next();
            ARessource aRessource = ARessourceManager.chargerARessource(idRes);
            
            res += aRessource.getCoutARessource() * duree;
        }
        
        return res;
    }
    
    /**
     * @since 29/12/08
     * Parcourir l'arbre d'une instance d'un composant JTreeTable
     * @param arbre
     * Instance du composant JTreeTable
     * @return 
     * Liste des tâches de l'arbre parcourue
     */
    public static List parcourirTreeTable(JTreeTable arbre) {
        int n = arbre.getRowCount();
        List res = null;
        
        if(n > 0) res = new LinkedList();
        
        for(int i = 0; i < n; i++) {
            TreePath tp = arbre.getTree().getPathForRow(i);
            Noeud noeud = (Noeud) tp.getLastPathComponent();
            Tache tache = noeud.getTache();
            res.add(tache);
        }
        
        return res;
    }
    
    /**
     * Déploie toutes les noeuds de l'arbre
     * @param arbre
     * Instance d'un composant JTree
     */
    public static void deployerJTree(JTree arbre) {
        int n = arbre.getRowCount();
        int i = 0;
        while(i < n) {
            TreePath tp = arbre.getPathForRow(i);
            arbre.expandPath(tp);
            n = arbre.getRowCount();
            i++;
        }
    }
}  