/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package mapping;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author FireWolf
 */
public class Projet {
    private int idProjet;
    private int idProfil;
    private String nomProjet;
    private Date debutProjet;
    private Date finProjet;
    private String description;
    private Set taches = new HashSet();
    private Set comparaisonTaches = new HashSet();
    
    public Projet() {}
    
    public int getIdProjet() {
        return idProjet;
    }

    public void setIdProjet(int idProjet) {
        this.idProjet = idProjet;
    }

    public String getNomProjet() {
        return nomProjet;
    }

    public void setNomProjet(String nomProjet) {
        this.nomProjet = nomProjet;
    }

    public Date getDebutProjet() {
        return debutProjet;
    }

    public void setDebutProjet(Date debutProjet) {
        this.debutProjet = debutProjet;
    }

    public Date getFinProjet() {
        return finProjet;
    }

    public void setFinProjet(Date finProjet) {
        this.finProjet = finProjet;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set getTaches() {
        return taches;
    }

    public void setTaches(Set taches) {
        this.taches = taches;
    }

    public Set getComparaisonTaches() {
        return comparaisonTaches;
    }

    public void setComparaisonTaches(Set comparaisonTaches) {
        this.comparaisonTaches = comparaisonTaches;
    }

    public int getIdProfil() {
        return idProfil;
    }

    public void setIdProfil(int idProfil) {
        this.idProfil = idProfil;
    }
}
