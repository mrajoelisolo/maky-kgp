/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package mapping;

import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author FireWolf
 */
public class Poste {
    private int idPoste;
    private String nomPoste;
    private float coutPoste;
    private String description;
    private Set ressources = new HashSet();
    
    public Poste() {}

    public int getIdPoste() {
        return idPoste;
    }

    public void setIdPoste(int idPoste) {
        this.idPoste = idPoste;
    }

    public String getNomPoste() {
        return nomPoste;
    }

    public void setNomPoste(String nomPoste) {
        this.nomPoste = nomPoste;
    }

    public float getCoutPoste() {
        return coutPoste;
    }

    public void setCoutPoste(float coutPoste) {
        this.coutPoste = coutPoste;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set getRessources() {
        return ressources;
    }

    public void setRessources(Set ressources) {
        this.ressources = ressources;
    }
}
