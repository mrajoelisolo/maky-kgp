/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package mapping;

/**
 *
 * @author FireWolf
 */
public class TComparable {
    private int idComparable;
    private int valeurId;
    private int idTache;
    private Projet projet;
    
    public TComparable() {}

    public int getIdTComparable() {
        return idComparable;
    }

    public void setIdTComparable(int idComparaisonTache) {
        this.idComparable = idComparaisonTache;
    }

    public int getValeurId() {
        return valeurId;
    }

    public void setValeurId(int valeurId) {
        this.valeurId = valeurId;
    }

    public int getIdTache() {
        return idTache;
    }
    
    public void setIdTache(int idTache) {
        this.idTache = idTache;
    }
    
    public Projet getProjet() {
        return projet;
    }

    public void setProjet(Projet projet) {
        this.projet = projet;
    }
}
