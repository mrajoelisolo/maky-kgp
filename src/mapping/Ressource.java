/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package mapping;

import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author FireWolf
 */
public class Ressource {
    private int idRessource;
    private String pseudo;
    private Set taches = new HashSet();
    private Poste poste;
    
    public Ressource() {}

    public int getIdRessource() {
        return idRessource;
    }

    public void setIdRessource(int idRessource) {
        this.idRessource = idRessource;
    }

    public String getPseudo() {
        return pseudo;
    }

    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    public Set getTaches() {
        return taches;
    }

    public void setTaches(Set taches) {
        this.taches = taches;
    }

    public Poste getPoste() {
        return poste;
    }

    public void setPoste(Poste poste) {
        this.poste = poste;
    }
}
