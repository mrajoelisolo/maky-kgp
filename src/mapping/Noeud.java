package mapping;

import java.util.Date;
import javax.swing.tree.DefaultMutableTreeNode;
import utile.ConversionDate;

/**
 *
 * @author FireWolf
 */
public class Noeud extends DefaultMutableTreeNode {
    
    private Tache tache;
    
    public Noeud() {}
    
    public Noeud(String nom) {
        super(nom);
    }
    
    public Tache getTache() {
        return tache;
    }
    
    public void setTache(Tache tache) {
        this.tache = tache;
        this.setUserObject(this.tache.getNomTache());
    }
    public void setValueAt(Object value, int col){
        switch(col){
            case 0:{
                tache.setNomTache(value.toString());
            }break;
            case 1:{
                Date date = ConversionDate.stringToDate(value.toString());
                tache.setDebutTache(date);
            }
            case 2:{
                Date date = ConversionDate.stringToDate(value.toString());
                tache.setFinTache(date);
            }
        }
    }
    
    public Object getValueAt(int col){
        switch(col){
            case 0:{
                return tache.getNomTache();
            }
            case 1:{
                return ConversionDate.dateToString(tache.getDebutTache());
            }
            case 2:{
                return ConversionDate.dateToString(tache.getFinTache());
            }
        }
        return null;
    }
    
    
}
