package mapping;

/**
 *
 * @author FireWolf
 */
public class ExceptionRessource {
    private long idExceptionRessource;
    private long idTache;
    private long idRessource;

    public long getIdExceptionRessource() {
        return idExceptionRessource;
    }

    public void setIdExceptionRessource(long idExceptionRessource) {
        this.idExceptionRessource = idExceptionRessource;
    }

    public long getIdTache() {
        return idTache;
    }

    public void setIdTache(long idTache) {
        this.idTache = idTache;
    }

    public long getIdRessource() {
        return idRessource;
    }

    public void setIdRessource(long idRessource) {
        this.idRessource = idRessource;
    }
}
