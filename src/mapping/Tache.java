/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mapping;


import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 *
 * @author FireWolf
 */
public class Tache {  
    private int idTache;
    private String nomTache;
    private Date debutTache;
    private Date finTache;
    private int pred;
    private int niveau;
    private boolean etat = false;
    private int idComparaison = 0;
    private List tachesFilles = new LinkedList();
    private Projet projet= new Projet();
    private Set ressources = new HashSet();
    private Set aressources = new HashSet();

    public Tache() {}

    public int getIdTache() {
        return idTache;
    }

    public void setIdTache(int idTache) {
        this.idTache = idTache;
    }

    public String getNomTache() {
        return nomTache;
    }

    public void setNomTache(String nomTache) {
        this.nomTache = nomTache;
    }

    public Date getDebutTache() {
        return debutTache;
    }

    public void setDebutTache(Date debutTache) {
        this.debutTache = debutTache;
    }

    public Date getFinTache() {
        return finTache;
    }

    public void setFinTache(Date finTache) {
        this.finTache = finTache;
    }

    public int getPred() {
        return pred;
    }

    public void setPred(int pred) {
        this.pred = pred;
    }

    public int getNiveau() {
        return niveau;
    }

    public void setNiveau(int niveau) {
        this.niveau = niveau;
    }

    public boolean getEtat() {
        return etat;
    }

    public void setEtat(boolean etat) {
        this.etat = etat;
    }

    public int getIdComparaison() {
        return idComparaison;
    }

    public void setIdComparaison(int idComparaison) {
        this.idComparaison = idComparaison;
    }

    public List getTachesFilles() {
        return tachesFilles;
    }

    public void setTachesFilles(List tachesFilles) {
        this.tachesFilles = tachesFilles;
    }

    public Projet getProjet() {
        return projet;
    }

    public void setProjet(Projet projet) {
        this.projet = projet;
    }

    public Set getRessources() {
        return ressources;
    }

    public void setRessources(Set ressources) {
        this.ressources = ressources;
    }

    public Set getAressources() {
        return aressources;
    }

    public void setAressources(Set aressources) {
        this.aressources = aressources;
    }
}
