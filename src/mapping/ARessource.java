/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package mapping;

import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author FireWolf
 */
public class ARessource {
    private int idARessource;
    private String nomARessource;
    private float coutARessource;
    private String description;
    private Set taches = new HashSet();
    
    public ARessource() {}

    public int getIdARessource() {
        return idARessource;
    }

    public void setIdARessource(int idARessource) {
        this.idARessource = idARessource;
    }

    public String getNomARessource() {
        return nomARessource;
    }

    public void setNomARessource(String nomARessource) {
        this.nomARessource = nomARessource;
    }

    public float getCoutARessource() {
        return coutARessource;
    }

    public void setCoutARessource(float coutARessource) {
        this.coutARessource = coutARessource;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set getTaches() {
        return taches;
    }

    public void setTaches(Set taches) {
        this.taches = taches;
    }
}
