package mapping;

import entitymanager.EntityManager;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import lwcomponents.canvas.LWShape;

/**
 *
 * @author FireWolf
 */
public class EtatARessource extends LWShape {
    public static int ROW_SPACING = 20;
    public static int COL_SPACING = 200;
    private Rectangle bounds = new Rectangle();
    
    private long idEtatARessource;
    private String nomARessource;
    private int duree;
    private float cout;

    public long getIdEtatARessource() {
        return idEtatARessource;
    }

    public void setIdEtatARessource(long idEtatARessource) {
        this.idEtatARessource = idEtatARessource;
    }

    public String getNomARessource() {
        return nomARessource;
    }

    public void setNomARessource(String nomARessource) {
        this.nomARessource = nomARessource;
    }

    public int getDuree() {
        return duree;
    }

    public void setDuree(int duree) {
        this.duree = duree;
    }

    public float getCout() {
        return cout;
    }

    public void setCout(float cout) {
        this.cout = cout;
    }
    
    @Override
    /**
     * Définit la façon dont l'élément de l'état doit être affiché 
     */
    public void draw(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        
        Font myFont = new Font(EntityManager.FONT_LIGNE, Font.BOLD, EntityManager.TAILLE_FONT_LIGNE);
        g2.setColor(new Color(0, 0, 0));
        g2.setFont(myFont);
        int sp = EntityManager.TAILLE_FONT_LIGNE + 5;
        g2.drawString(nomARessource, bounds.x + 10, bounds.y + sp);
        
        g2.drawString("      " + duree, bounds.x + COL_SPACING, bounds.y + sp);
        
        g2.drawString("  " + cout, bounds.x + 2*COL_SPACING, bounds.y + sp);
    }

    @Override
    public Rectangle getBounds() {
        return bounds;
    }
    
    public void setBounds(int x, int y, int wdt, int hgt) {
        this.bounds.setBounds(x, y, wdt, hgt);
    }
}
