/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package mapping;

import java.util.Date;

/**
 *
 * @author FireWolf
 */
public class Agenda {
    private int idAgenda;
    private Date dateAgenda;
    private String description;
    private Profil profil;

    public int getIdAgenda() {
        return idAgenda;
    }

    public void setIdAgenda(int idAgenda) {
        this.idAgenda = idAgenda;
    }

    public Date getDateAgenda() {
        return dateAgenda;
    }

    public void setDateAgenda(Date dateAgenda) {
        this.dateAgenda = dateAgenda;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Profil getProfil() {
        return profil;
    }

    public void setProfil(Profil profil) {
        this.profil = profil;
    }
}
