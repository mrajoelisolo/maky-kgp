package mapping;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.util.Date;
import lwcomponents.canvas.LWShape;
import utile.ConversionDate;
import entitymanager.EntityManager;

/**
 *
 * @author FireWolf
 */
public class EtatTache extends LWShape {
    public static int ROW_SPACING = 20;
    public static int COL_SPACING = 200;
    private Rectangle bounds = new Rectangle();
    
    private long idEtatTache;
    private String nomTache;
    private Date debutTache;
    private Date finTache;
    private float cout;

    public EtatTache() {}
    
    public long getIdEtatTache() {
        return idEtatTache;
    }

    public void setIdEtatTache(long idEtatTache) {
        this.idEtatTache = idEtatTache;
    }

    public String getNomTache() {
        return nomTache;
    }

    public void setNomTache(String nomTache) {
        this.nomTache = nomTache;
    }

    public Date getDebutTache() {
        return debutTache;
    }

    public void setDebutTache(Date debutTache) {
        this.debutTache = debutTache;
    }

    public Date getFinTache() {
        return finTache;
    }

    public void setFinTache(Date finTache) {
        this.finTache = finTache;
    }

    public float getCout() {
        return cout;
    }

    public void setCout(float cout) {
        this.cout = cout;
    }

    @Override
    /**
     * Définit la façon dont l'élément de l'état doit être affiché 
     */
    public void draw(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        
        Font myFont = new Font(EntityManager.FONT_LIGNE, Font.BOLD, EntityManager.TAILLE_FONT_LIGNE);
        g2.setColor(new Color(0, 0, 0));
        g2.setFont(myFont);
        g2.drawString(nomTache, bounds.x + 10, bounds.y + 20);
        
        int sp = EntityManager.TAILLE_FONT_LIGNE + 5;
        g2.drawString(ConversionDate.dateToString(debutTache), bounds.x + COL_SPACING, bounds.y + sp);
        
        g2.drawString(ConversionDate.dateToString(finTache), bounds.x + 2*COL_SPACING, bounds.y + sp);
        
        g2.drawString("" + cout, bounds.x + 3*COL_SPACING, bounds.y + 20);
    }

    @Override
    public Rectangle getBounds() {
        return bounds;
    }
    
    public void setBounds(int x, int y, int wdt, int hgt) {
        this.bounds.setBounds(x, y, wdt, hgt);
    }
}
