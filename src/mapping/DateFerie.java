/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package mapping;

import java.util.Date;

/**
 *
 * @author FireWolf
 */
public class DateFerie {
    private int idDateFerie;
    private String nomDateFerie;
    private int jourFerie;
    private int moisFerie;
    private String description;

    public int getIdDateFerie() {
        return idDateFerie;
    }

    public void setIdDateFerie(int idDateFerie) {
        this.idDateFerie = idDateFerie;
    }

    public String getNomDateFerie() {
        return nomDateFerie;
    }

    public void setNomDateFerie(String nomDateFerie) {
        this.nomDateFerie = nomDateFerie;
    }

    public int getJourFerie() {
        return jourFerie;
    }

    public void setJourFerie(int jourFerie) {
        this.jourFerie = jourFerie;
    }

    public int getMoisFerie() {
        return moisFerie;
    }

    public void setMoisFerie(int moisFerie) {
        this.moisFerie = moisFerie;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
