/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package mapping;

import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author FireWolf
 */
public class Profil {
    private int idProfil;
    private String login;
    private String password;
    private String privilege;
    private Set agendas = new HashSet();
    
    public Profil() {
        privilege = "STDUSER";
    }

    public int getIdProfil() {
        return idProfil;
    }

    public void setIdProfil(int idProfil) {
        this.idProfil = idProfil;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPrivilege() {
        return privilege;
    }

    public void setPrivilege(String privilege) {
        this.privilege = privilege;
    }

    public Set getAgendas() {
        return agendas;
    }

    public void setAgendas(Set agendas) {
        this.agendas = agendas;
    }
}
