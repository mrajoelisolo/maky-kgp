package application;

import application.user.UserAbout;
import application.user.UserBudget;
import application.user.UserBudgetMenu;
import application.user.UserGantt;
import application.user.UserGanttMenu;
import application.user.UserMenu;
import application.user.UserProjet;
import application.user.UserTache;
import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.Robot;
import java.awt.image.BufferedImage;
import java.util.Calendar;
import javax.swing.JOptionPane;
import lwcomponents.canvas.LWCanvas;
import lwcomponents.canvas.LWCustomCanvas;
import lwcomponents.canvas.LWShape;
import lwcomponents.util.LWButton2;
import lwcomponents.util.LWButtonSkin;
import lwcomponents.util.LWClock;
import lwcomponents.util.LWImgUtil;
import lwcomponents.util.LWMouseListener;
import lwcomponents.util.MakeNimbusLookNFeel;
import mapping.Profil;
import utile.PaneManager;

/**
 *
 * @author  FireWolf
 */
public class UserForm extends javax.swing.JFrame implements Runnable {
    private BufferedImage bcgImg;
    private BufferedImage titleImg;
    private BufferedImage showCase;
    private BufferedImage[] tabImg;
    private LWCanvas showCaseCanvas;
    private LWCanvas menuCanvas;
    private LWCustomCanvas titleCanvas;
    private LWMouseListener mouseListener;
    private LWButton2 b1;
    private LWButton2 b2;
    private LWButton2 b3;
    private LWButton2 b4;
    private LWButton2 b5;
    private LWButton2 b7;
    private LWClock myClock;
    private Calendar cld = Calendar.getInstance();
    private Thread thread;
    
    //Liste des menus du compte utilisateur
    private Profil utilisateur;
    private SessionForm sessionForm;
    private UserAbout userAbout;
    private UserMenu userMenu;
    private UserProjet userProjet;
    private UserTache userTache;
    private UserGanttMenu userGanttMenu;
    private UserGantt userGantt;
    private UserBudgetMenu userCoutMenu;
    private UserBudget userCout;
    
    /** Creates new form MenuForm */
    public UserForm() {
        initComponents();
        initialiserForm();
    }
    
    public UserForm(Profil p) {
        //Définition de l'utilisateur
        this.utilisateur = p;
        
        initComponents();
        initialiserForm();
        
        jLabel1.setText("Compte utilisateur, nom de votre login : " + utilisateur.getLogin());
    }

    private void initialiserForm() {
        bcgImg = LWImgUtil.loadImage("img\\DoricOrder.jpg");
        titleImg = LWImgUtil.loadImage("img\\titleBckg.jpg");
        tabImg = new BufferedImage[4];
        tabImg[0] = LWImgUtil.loadImage("img\\components\\showCase\\User\\showCase1.bmp");
        tabImg[1] = LWImgUtil.loadImage("img\\components\\showCase\\User\\showCase2.bmp");
        tabImg[2] = LWImgUtil.loadImage("img\\components\\showCase\\User\\showCase3.bmp");
        tabImg[3] = LWImgUtil.loadImage("img\\components\\showCase\\User\\showCase4.bmp");
        
        titleCanvas = new LWCustomCanvas() {
            @Override
            public void paint(Graphics g) {
                Graphics2D g2 = (Graphics2D) g;
                
                Rectangle b = jPanel4.getBounds();
                g2.drawImage(titleImg, 0, 0, b.width, b.height, this);
            }
        };
        jPanel4.add(titleCanvas);
        titleCanvas.setBounds(0, 0, jPanel4.getBounds().width, jPanel4.getBounds().height);
        
        menuCanvas = new LWCanvas() {
            Rectangle b = jPanel1.getBounds();
            
            @Override
            public void paintBackground(Graphics g) {
                Graphics2D g2 = (Graphics2D) g;
                g2.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
                
                g2.drawImage(bcgImg, 0, 0, b.width, b.height, null);
                
                g2.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.6F));
            }
        };
        menuCanvas.setAntialiasing(LWCanvas.ANTIALIASING_ON);
        menuCanvas.setRenderQuality(LWCanvas.RENDER_HIGH_QUALITY);
        mouseListener = new LWMouseListener(menuCanvas);
        
        b1 = new LWButton2(10, 10, "Projets");
        b1.setSize(170, 30);
        
        b2 = new LWButton2(10, 50, "Budget");
        b2.setSize(170, 30);
        
        b3 = new LWButton2(10, 90, "Ecarts");
        b3.setSize(170, 30);
        
        b4 = new LWButton2(10, 130, "Gantt");
        b4.setSize(170, 30);
        
        b5 = new LWButton2(10, 170, "Accueil");
        b5.setSize(170, 30);
        
        b7 = new LWButton2(10, 670, "Quitter");
        b7.setSize(170, 30);
        
        myClock = new LWClock();
        myClock.setBounds(5, 450, 180, 180);
        
        b1.setMouseListener(mouseListener);
        b2.setMouseListener(mouseListener);
        b3.setMouseListener(mouseListener);
        b4.setMouseListener(mouseListener);
        b5.setMouseListener(mouseListener);
        b7.setMouseListener(mouseListener);
        LWButtonSkin btnSkin = new LWButtonSkin("img\\components\\btnModel2", "btn0", "btn1", "btn2");
        b1.setSkin(btnSkin);
        b2.setSkin(btnSkin);
        b3.setSkin(btnSkin);
        b4.setSkin(btnSkin);
        b5.setSkin(btnSkin);
        b7.setSkin(btnSkin);
        
        menuCanvas.addShape(b1);
        menuCanvas.addShape(b2);
        menuCanvas.addShape(b3);
        menuCanvas.addShape(b4);
        menuCanvas.addShape(b5);
        menuCanvas.addShape(b7);
        menuCanvas.addShape(myClock);
        
        jPanel1.add(menuCanvas.getCanvas(jPanel1));
        menuCanvas.setBackground(new Color(51,153,255));
        //jPanel1.setOpaque(true);
        
        userMenu = new UserMenu();
        getUserMenu().setBounds(PaneManager.obtenirDecalageBounds(jPanel2, 3));
        getUserMenu().setVisible(true);
        jPanel2.add(getUserMenu());
      
        //-------------------------//
        userProjet = new UserProjet(utilisateur, this);
        getUserProjet().setBounds(PaneManager.obtenirDecalageBounds(jPanel2, 3));
        getUserProjet().setVisible(false);
        jPanel2.add(getUserProjet());
        
        setUserTache(new UserTache());
        getUserTache().setUserProjet(userProjet);
        getUserTache().setBounds(PaneManager.obtenirDecalageBounds(jPanel2, 3));
        getUserTache().setVisible(false);
        jPanel2.add(getUserTache());
        
        setUserCoutMenu(new UserBudgetMenu(utilisateur, this));
        getUserBudgetMenu().setBounds(PaneManager.obtenirDecalageBounds(jPanel2, 3));
        getUserBudgetMenu().setVisible(false);
        jPanel2.add(getUserBudgetMenu());
        
        setUserCout(new UserBudget());
        getUserBudget().setUserCoutMenu(userCoutMenu);
        getUserBudget().setBounds(PaneManager.obtenirDecalageBounds(jPanel2, 3));
        getUserBudget().setVisible(false);
        jPanel2.add(getUserBudget());
        
        //-------------------------//
        
        userGanttMenu = new UserGanttMenu(utilisateur, this);
        getUserGanttMenu().setBounds(PaneManager.obtenirDecalageBounds(jPanel2, 3));
        getUserGanttMenu().setVisible(false);
        jPanel2.add(getUserGanttMenu());
        
        setUserGantt(new UserGantt());
        getUserGantt().setUserGanttMenu(userGanttMenu);
        getUserGantt().setBounds(PaneManager.obtenirDecalageBounds(jPanel2, 3));
        getUserGantt().setVisible(false);
        jPanel2.add(getUserGantt());
        
        userAbout = new UserAbout();
        userAbout.setBounds(PaneManager.obtenirDecalageBounds(jPanel2, 3));
        userAbout.setVisible(false);
        jPanel2.add(userAbout);
        
        //Since 07/01/08
        showCaseCanvas = new LWCanvas() {
            Rectangle b = jPanel2.getBounds();
            @Override
            public void paintBackground(Graphics g) {
                Graphics2D g2 = (Graphics2D) g;
                
                if(showCase != null)
                    g2.drawImage(showCase, 0, 0, b.width, b.height, null);
            }
        };
        showCaseCanvas.setBounds(0, 0, jPanel2.getBounds().width, jPanel2.getBounds().height);
        jPanel2.add(showCaseCanvas);
        
        thread = new Thread(this);
        thread.start();
    }
    
    public void quitterApplication() {
        Object[] options = {"Oui", "Non"};
        int n = JOptionPane.showOptionDialog(this, "Etes vous sur de vouloir quitter ?", "Quitter l'application",
                JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[1]);
        
        if(n == 0) System.exit(0);
    }
    
    public void quitterSession() {
        Object[] options = {"Oui", "Non"};
        int n = JOptionPane.showOptionDialog(this, "Etes vous sur de vouloir fermer cette session ?", "Fermer la session",
                JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[1]);
        
        if(n == 0) {
            getSessionForm().setVisible(true);
            this.dispose();
        }
    }
    
    private void hideAllCanvas() {
        userMenu.setVisible(false);
        userProjet.setVisible(false);
        userTache.setVisible(false);
        userGanttMenu.setVisible(false);
        userGantt.setVisible(false);
        userCoutMenu.setVisible(false);
        userCout.setVisible(false);
        userAbout.setVisible(false);
    }
    
    private boolean menuVisible() {
        if(userMenu.isVisible() || userProjet.isVisible() || userTache.isVisible()
                || userGanttMenu.isVisible() || userGantt.isVisible() || userCoutMenu.isVisible()
                || userCout.isVisible() || userAbout.isVisible())
            return false;
        else
            return true;
    }
    
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        jMenuBarUser = new javax.swing.JMenuBar();
        jMenuFichier = new javax.swing.JMenu();
        jMenuItemSession = new javax.swing.JMenuItem();
        jMenuItemQuitter = new javax.swing.JMenuItem();
        jMenuEdition = new javax.swing.JMenu();
        jMenuItemCopierT = new javax.swing.JMenuItem();
        jMenuItemCouperT = new javax.swing.JMenuItem();
        jMenuItemCollerT = new javax.swing.JMenuItem();
        jMenuAide = new javax.swing.JMenu();
        jMenuItemApropos = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Kronos Gantt Project - Compte utilisateur");
        setMinimumSize(new java.awt.Dimension(1024, 768));
        setResizable(false);
        getContentPane().setLayout(null);

        jPanel1.setBackground(new java.awt.Color(204, 204, 255));
        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel1.setLayout(null);
        getContentPane().add(jPanel1);
        jPanel1.setBounds(0, 0, 190, 710);

        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel2.setLayout(null);
        getContentPane().add(jPanel2);
        jPanel2.setBounds(190, 60, 830, 620);

        jPanel3.setBackground(new java.awt.Color(102, 102, 255));
        jPanel3.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel3.setLayout(null);

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(0, 0, 102));
        jLabel1.setText("Compte d'utilisateur, nom de votre login : ");
        jPanel3.add(jLabel1);
        jLabel1.setBounds(10, 10, 460, 15);

        getContentPane().add(jPanel3);
        jPanel3.setBounds(190, 680, 830, 30);

        jPanel4.setBackground(new java.awt.Color(153, 153, 255));
        jPanel4.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel4.setLayout(null);

        jLabel2.setBackground(new java.awt.Color(0, 204, 204));
        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(0, 0, 102));
        jLabel2.setText("Bienvenue");
        jPanel4.add(jLabel2);
        jLabel2.setBounds(10, 10, 660, 22);

        getContentPane().add(jPanel4);
        jPanel4.setBounds(190, 0, 830, 50);

        jPanel5.setBackground(new java.awt.Color(51, 51, 255));
        jPanel5.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        getContentPane().add(jPanel5);
        jPanel5.setBounds(190, 50, 830, 10);

        jMenuFichier.setText("Fichier");

        jMenuItemSession.setText("Fermer la session");
        jMenuItemSession.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemSessionActionPerformed(evt);
            }
        });
        jMenuFichier.add(jMenuItemSession);

        jMenuItemQuitter.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_Q, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItemQuitter.setText("Quitter l'application");
        jMenuItemQuitter.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemQuitterActionPerformed(evt);
            }
        });
        jMenuFichier.add(jMenuItemQuitter);

        jMenuBarUser.add(jMenuFichier);

        jMenuEdition.setText("Edition");

        jMenuItemCopierT.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_C, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItemCopierT.setText("Copier une tâche");
        jMenuEdition.add(jMenuItemCopierT);

        jMenuItemCouperT.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_X, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItemCouperT.setText("Couper une tâche");
        jMenuEdition.add(jMenuItemCouperT);

        jMenuItemCollerT.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_V, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItemCollerT.setText("Coller une tâche");
        jMenuEdition.add(jMenuItemCollerT);

        jMenuBarUser.add(jMenuEdition);

        jMenuAide.setText("Aide");

        jMenuItemApropos.setText("A propos");
        jMenuItemApropos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemAproposActionPerformed(evt);
            }
        });
        jMenuAide.add(jMenuItemApropos);

        jMenuBarUser.add(jMenuAide);

        setJMenuBar(jMenuBarUser);

        pack();
    }// </editor-fold>//GEN-END:initComponents

private void jMenuItemQuitterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemQuitterActionPerformed
// TODO add your handling code here:
    quitterApplication();
}//GEN-LAST:event_jMenuItemQuitterActionPerformed

private void jMenuItemSessionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemSessionActionPerformed
// TODO add your handling code here:
    quitterSession();
}//GEN-LAST:event_jMenuItemSessionActionPerformed

private void jMenuItemAproposActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemAproposActionPerformed
// TODO add your handling code here
    userMenu.setVisible(false);
    userProjet.setVisible(false);
    userTache.setVisible(false);
    userGanttMenu.setVisible(false);
    userGantt.setVisible(false);
    userCoutMenu.setVisible(false);
    userCout.setVisible(false);
    userAbout.setVisible(true);
    jLabel2.setText("A propos de Kronos Gantt Project");
}//GEN-LAST:event_jMenuItemAproposActionPerformed

    /**
    * @param args the command line arguments
    */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                MakeNimbusLookNFeel.makeIt();
                new UserForm().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JMenu jMenuAide;
    private javax.swing.JMenuBar jMenuBarUser;
    private javax.swing.JMenu jMenuEdition;
    private javax.swing.JMenu jMenuFichier;
    private javax.swing.JMenuItem jMenuItemApropos;
    private javax.swing.JMenuItem jMenuItemCollerT;
    private javax.swing.JMenuItem jMenuItemCopierT;
    private javax.swing.JMenuItem jMenuItemCouperT;
    private javax.swing.JMenuItem jMenuItemQuitter;
    private javax.swing.JMenuItem jMenuItemSession;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    // End of variables declaration//GEN-END:variables

    public void run() {
        try{
            while(true) {
                    thread.sleep(10);
                    if(b1.getFocusState()) {
                        /*New here : Animation included*/
                        if(userMenu.isVisible() || menuVisible()) {
                            hideAllCanvas();
                            showCase = tabImg[0];
                            jPanel2.repaint();
                        }
                        /**/
                    //Projets
                    LWShape sel = mouseListener.getSelection();
                    if(sel != null)
                        if(sel.equals(b1)) {
                            getUserMenu().setVisible(false);
                            getUserBudgetMenu().setVisible(false);
                            getUserProjet().setVisible(true);
                            getUserGanttMenu().setVisible(false);
                            /*Sous fenêtres*/
                            getUserTache().setVisible(false);
                            getUserGantt().setVisible(false);
                            getUserBudget().setVisible(false);
                            userAbout.setVisible(false);

                            jLabel2.setText("Gestion de projets : vous pouvez ajouter, " +
                                    "modifier ou supprimer les projets");

                            mouseListener.clearSelection();
                        }
                } 

                if(b2.getFocusState()) {
                    
                        if(userMenu.isVisible() || menuVisible()) {
                            hideAllCanvas();
                            showCase = tabImg[1];
                            jPanel2.repaint();
                        }
                    
                    //Budgets
                        LWShape sel = mouseListener.getSelection();
                        if(sel != null)
                            if(sel.equals(b2)) {
                                getUserMenu().setVisible(false);
                                getUserBudgetMenu().setVisible(true);
                                getUserProjet().setVisible(false);
                                getUserGanttMenu().setVisible(false);
                                /*Sous fenêtres*/
                                getUserTache().setVisible(false);
                                getUserGantt().setVisible(false);
                                getUserBudget().setVisible(false);
                                userAbout.setVisible(false);

                                jLabel2.setText("Budgets : génerer des états de budgets " +
                                        "pour les projets");

                                mouseListener.clearSelection();
                            }
                }  

                if(b3.getFocusState()) {
                    
                        if(userMenu.isVisible() || menuVisible()) {
                            hideAllCanvas();
                            showCase = tabImg[2];
                            jPanel2.repaint();
                        }
                    
                      //Ecarts
                        LWShape sel = mouseListener.getSelection();
                        if(sel != null)
                            if(sel.equals(b3)) {
                                getUserMenu().setVisible(false);
                                getUserBudgetMenu().setVisible(false);
                                getUserProjet().setVisible(false);
                                getUserGanttMenu().setVisible(false);
                                /*Sous fenêtres*/
                                getUserTache().setVisible(false);
                                getUserGantt().setVisible(false);
                                getUserBudget().setVisible(false); 
                                userAbout.setVisible(false);

                                jLabel2.setText("Ecarts : génerer les états d'écarts " +
                                        "entre les projets initial et le projet finales");

                                mouseListener.clearSelection();
                            }
                }

                if(b4.getFocusState()) {
                    
                        if(userMenu.isVisible() || menuVisible()) {
                            hideAllCanvas();
                            showCase = tabImg[3];
                            jPanel2.repaint();
                        }
                    
                    //Gantt
                        LWShape sel = mouseListener.getSelection();
                        if(sel != null)
                            if(sel.equals(b4)) {
                                getUserMenu().setVisible(false);
                                getUserBudgetMenu().setVisible(false);
                                getUserProjet().setVisible(false);
                                getUserGanttMenu().setVisible(true);
                                /*Sous fenêtres*/
                                getUserTache().setVisible(false);
                                getUserGantt().setVisible(false);
                                getUserBudget().setVisible(false);
                                userAbout.setVisible(false);

                                jLabel2.setText("Diagramme de Gantt : visualiser le" +
                                        "planning initial des projets");

                                mouseListener.clearSelection();
                            }
                }
                    
                if(b5.getFocusState()) {  
                    //Gantt
                        LWShape sel = mouseListener.getSelection();
                        if(sel != null)
                            if(sel.equals(b5)) {
                                getUserMenu().setVisible(true);
                                getUserBudgetMenu().setVisible(false);
                                getUserProjet().setVisible(false);
                                getUserGanttMenu().setVisible(false);
                                /*Sous fenêtres*/
                                getUserTache().setVisible(false);
                                getUserGantt().setVisible(false);
                                getUserBudget().setVisible(false);
                                userAbout.setVisible(false);

                                jLabel2.setText("Bienvenue");

                                mouseListener.clearSelection();
                            }
                }

                if(b7.getFocusState()) {
                    //Quitter
                        LWShape sel = mouseListener.getSelection();
                        if(sel != null)
                            if(sel.equals(b7)) {

                                quitterApplication();

                                mouseListener.clearSelection();
                            }
                }

                //Affiche l'horloge
                cld = Calendar.getInstance();
                int heure = cld.get(Calendar.HOUR);
                int minute = cld.get(Calendar.MINUTE);
                int seconde = cld.get(Calendar.SECOND);

                myClock.setHour(heure, minute, seconde);
                menuCanvas.repaint();
            }
            
        }catch(InterruptedException e) {
            return;
        }
    }
    
    public UserMenu getUserMenu() {
        return userMenu;
    }

    public UserProjet getUserProjet() {
        return userProjet;
    }

    public UserTache getUserTache() {
        return userTache;
    }

    /**
     * Définir l'instance de la fenêtre de session correspondante
     * @param sessionForm
     */
    public void setSessionForm(SessionForm sessionForm) {
        this.sessionForm = sessionForm;
    }

    /**
     * Obtenir l'instance de la fenêtre de session correspondante
     * @param sessionForm
     */
    public SessionForm getSessionForm() {
        return sessionForm;
    }

    public void setUserTache(UserTache userTache) {
        this.userTache = userTache;
    }

    public UserGanttMenu getUserGanttMenu() {
        return userGanttMenu;
    }

    public void setUserGantt(UserGantt userGantt) {
        this.userGantt = userGantt;
    }

    public UserGantt getUserGantt() {
        return userGantt;
    }

    public UserBudgetMenu getUserBudgetMenu() {
        return userCoutMenu;
    }

    public UserBudget getUserBudget() {
        return userCout;
    }

    public void setUserCout(UserBudget userCout) {
        this.userCout = userCout;
    }

    public void setUserCoutMenu(UserBudgetMenu userCoutMenu) {
        this.userCoutMenu = userCoutMenu;
    }
}
