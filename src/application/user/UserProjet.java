package application.user;

import application.UserForm;
import entitymanager.EntityManager;
import entitymanager.ProjetManager;
import javax.swing.JButton;
import javax.swing.JRadioButton;
import mapping.Profil;
import mapping.Projet;
import utile.ConversionDate;
import utile.MyRecordSet;
/**
 *
 * @author  FireWolf
 */
public class UserProjet extends javax.swing.JPanel {
    private UserForm userForm;
    private MyRecordSet resultat;
    private Profil utilisateur;
    private String mode;
    private Projet selection;
    private Projet projet;
    
    /** Creates new form AdminProfil */
    public UserProjet() {
        initComponents();
        initialiserForm();
    }
    
    public UserProjet(Profil p, UserForm userForm) {
        utilisateur = p;
        
        initComponents();
        initialiserForm();
        
        this.userForm = userForm;
    }

    private void initialiserForm() {
        resultat = new MyRecordSet();
        
        setEtatSaisie(false);
        setFiltrage(false);
        
        int nbRes = ProjetManager.chargerProjets(jTableProjets, resultat, utilisateur);
        
        System.out.println("Nombre de résultats : " + nbRes);
    }
    
    /**
     * Active ou désactive la zone de saisie
     * @param etat
     * si true alors la saisie est permise
     */
    private void setEtatSaisie(boolean etat) {
        jTextFieldNom.setEditable(etat);
        jTextFieldDebut.setEditable(etat);
        jTextFieldFin.setEditable(etat);
        jButton4.setEnabled(etat);
        jButton5.setEnabled(etat);
    }
    
    /**
     * Active ou désactive l'option de filtrage
     */
    private void setFiltrage(boolean etat) {
        jButtonFiltrer.setEnabled(etat);
        jRadioButtonFiltre1.setEnabled(etat);
        jRadioButtonFiltre3.setEnabled(etat);
        jRadioButtonFiltre2.setEnabled(etat);
        jTextFieldCritere.setEditable(etat);
        selectionnerRadioBouton(jRadioButtonFiltre1);
    }
    
    /**
     * Afficher les propriétés du profil dans la zone de saisie
     * @param p
     * Instance d'un profil
     */
    private void afficherProprietesProjet(Projet p) {
        jTextFieldNom.setText(p.getNomProjet());
        jTextFieldDebut.setText(ConversionDate.dateToString(p.getDebutProjet()));
        jTextFieldFin.setText(ConversionDate.dateToString(p.getFinProjet()));   
    }
    
    /**
     * Obtenir les propriétés du profil dans le but d'une modification
     * @return
     * Instance d'un profil
     */
    private Projet getProprietesProjet() {
        Projet p = selection;
        p.setNomProjet(jTextFieldNom.getText());
        p.setDebutProjet(ConversionDate.stringToDate(jTextFieldDebut.getText()));
        p.setFinProjet(ConversionDate.stringToDate(jTextFieldFin.getText()));
        return p;
    }
    
    /**
     * Obtenir le projet saisi dans le but d'un ajout
     * @return
     * Instance du projet saisi
     */
    private Projet getSaisie() {
        Projet res = new Projet();
        res.setIdProfil(utilisateur.getIdProfil());
        res.setNomProjet(jTextFieldNom.getText());
        res.setDebutProjet(ConversionDate.stringToDate(jTextFieldDebut.getText()));
        res.setFinProjet(ConversionDate.stringToDate(jTextFieldFin.getText()));
        return res;
    }
    
    /**
     * Vider la zone de saisie pour effectuer un ajout de nouveau profil
     */
    private void initSaisie() {
        jTextFieldNom.setText("");
        jTextFieldDebut.setText("");
        jTextFieldFin.setText("");
    }
    
    /**
     * Mettre à jour l'affichage des profils de la base de données
     */
    public void updateResultat() {
        int nbRes = ProjetManager.chargerProjets(jTableProjets, resultat, utilisateur);
        
        if(nbRes == 0)
            System.out.println("Aucun résulat");
        else
            System.out.println("Nombre de résultats : " + nbRes);
    }
    
    /**
     * Afficher les résultats filtrés
     * @param critère
     * Critère de sélection
     */
    private void filtrerResultat(String critere) {
        int nbRes = ProjetManager.chargerProjets(jTableProjets, resultat, utilisateur, critere);
        
        if(nbRes == 0)
            System.out.println("Aucun résulat");
        else
            System.out.println("Nombre de résultats : " + nbRes);
    }
    
    /**
     * Activer tous les boutons de modifications (ajout, suppression, modification)
     */
    public void activerBoutonsModif() {
        jButtonAjouterProjet.setEnabled(true);
        jButtonSupprimerProjet.setEnabled(true);
        jButtonModifierProjet.setEnabled(true);
    }
    
    /**
     * Activer tous les boutons de modifications sauf un seul
     * @param b
     * Le seul bouton qui ne doit pas être activé
     */
    public void activerBoutonsModifSauf(JButton b) {
        activerBoutonsModif();
        b.setEnabled(false);
    }
    
    private void selectionnerRadioBouton(JRadioButton radioBouton) {
        jRadioButtonFiltre1.setSelected(false);
        jRadioButtonFiltre3.setSelected(false);
        jRadioButtonFiltre2.setSelected(false);
        
        radioBouton.setSelected(true);
    }
    
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanelSaisie = new javax.swing.JPanel();
        jLabelNom = new javax.swing.JLabel();
        jLabelDebut = new javax.swing.JLabel();
        jLabelFin = new javax.swing.JLabel();
        jTextFieldNom = new javax.swing.JTextField();
        jTextFieldDebut = new javax.swing.JTextField();
        jButton4 = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        jTextFieldFin = new javax.swing.JTextField();
        jPanelProjets = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTableProjets = new javax.swing.JTable();
        jButtonAjouterProjet = new javax.swing.JButton();
        jButtonSupprimerProjet = new javax.swing.JButton();
        jButtonModifierProjet = new javax.swing.JButton();
        jButtonOuvrirProjet = new javax.swing.JButton();
        jPanelFiltrage = new javax.swing.JPanel();
        jComboBoxFiltrage = new javax.swing.JComboBox();
        jLabel7 = new javax.swing.JLabel();
        jRadioButtonFiltre1 = new javax.swing.JRadioButton();
        jRadioButtonFiltre3 = new javax.swing.JRadioButton();
        jRadioButtonFiltre2 = new javax.swing.JRadioButton();
        jTextFieldCritere = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        jButtonFiltrer = new javax.swing.JButton();

        setMinimumSize(new java.awt.Dimension(600, 550));
        setPreferredSize(new java.awt.Dimension(830, 660));
        addComponentListener(new java.awt.event.ComponentAdapter() {
            public void componentShown(java.awt.event.ComponentEvent evt) {
                formComponentShown(evt);
            }
        });
        setLayout(null);

        jPanelSaisie.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(), "Propriétés du projet", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N
        jPanelSaisie.setLayout(null);

        jLabelNom.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabelNom.setText("Nom");
        jPanelSaisie.add(jLabelNom);
        jLabelNom.setBounds(10, 40, 60, 30);

        jLabelDebut.setFont(new java.awt.Font("Tahoma", 1, 11));
        jLabelDebut.setText("Début");
        jPanelSaisie.add(jLabelDebut);
        jLabelDebut.setBounds(10, 80, 80, 30);

        jLabelFin.setFont(new java.awt.Font("Tahoma", 1, 11));
        jLabelFin.setText("Fin");
        jPanelSaisie.add(jLabelFin);
        jLabelFin.setBounds(10, 120, 70, 30);
        jPanelSaisie.add(jTextFieldNom);
        jTextFieldNom.setBounds(70, 40, 200, 30);
        jPanelSaisie.add(jTextFieldDebut);
        jTextFieldDebut.setBounds(150, 80, 120, 30);

        jButton4.setLabel("Valider");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });
        jPanelSaisie.add(jButton4);
        jButton4.setBounds(10, 180, 120, 23);

        jButton5.setLabel("Annuler");
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });
        jPanelSaisie.add(jButton5);
        jButton5.setBounds(150, 180, 120, 23);
        jPanelSaisie.add(jTextFieldFin);
        jTextFieldFin.setBounds(150, 120, 120, 30);

        add(jPanelSaisie);
        jPanelSaisie.setBounds(10, 400, 280, 210);

        jPanelProjets.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(), "Liste des projets", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N
        jPanelProjets.setLayout(null);

        jTableProjets.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jTableProjets.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTableProjetsMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(jTableProjets);

        jPanelProjets.add(jScrollPane1);
        jScrollPane1.setBounds(10, 30, 790, 320);

        jButtonAjouterProjet.setText("Ajout");
        jButtonAjouterProjet.setActionCommand("jButton1");
        jButtonAjouterProjet.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAjouterProjetActionPerformed(evt);
            }
        });
        jPanelProjets.add(jButtonAjouterProjet);
        jButtonAjouterProjet.setBounds(140, 360, 110, 23);

        jButtonSupprimerProjet.setText("Suppression");
        jButtonSupprimerProjet.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSupprimerProjetActionPerformed(evt);
            }
        });
        jPanelProjets.add(jButtonSupprimerProjet);
        jButtonSupprimerProjet.setBounds(400, 360, 110, 23);

        jButtonModifierProjet.setText("Modification");
        jButtonModifierProjet.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonModifierProjetActionPerformed(evt);
            }
        });
        jPanelProjets.add(jButtonModifierProjet);
        jButtonModifierProjet.setBounds(270, 360, 110, 23);

        jButtonOuvrirProjet.setFont(new java.awt.Font("Tahoma", 1, 11));
        jButtonOuvrirProjet.setForeground(new java.awt.Color(0, 102, 204));
        jButtonOuvrirProjet.setText("Ouvrir");
        jButtonOuvrirProjet.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonOuvrirProjetActionPerformed(evt);
            }
        });
        jPanelProjets.add(jButtonOuvrirProjet);
        jButtonOuvrirProjet.setBounds(10, 360, 110, 23);

        add(jPanelProjets);
        jPanelProjets.setBounds(10, 10, 810, 390);

        jPanelFiltrage.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(), "Options de filtrage", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N
        jPanelFiltrage.setLayout(null);

        jComboBoxFiltrage.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Tous les projets", "Par filtrage" }));
        jComboBoxFiltrage.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jComboBoxFiltrageItemStateChanged(evt);
            }
        });
        jPanelFiltrage.add(jComboBoxFiltrage);
        jComboBoxFiltrage.setBounds(80, 40, 190, 20);

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 11));
        jLabel7.setText("Afficher");
        jPanelFiltrage.add(jLabel7);
        jLabel7.setBounds(10, 40, 70, 20);

        jRadioButtonFiltre1.setText("Le nom commence par");
        jRadioButtonFiltre1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButtonFiltre1ActionPerformed(evt);
            }
        });
        jPanelFiltrage.add(jRadioButtonFiltre1);
        jRadioButtonFiltre1.setBounds(10, 180, 180, 23);

        jRadioButtonFiltre3.setText("Le nom se termine par");
        jRadioButtonFiltre3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButtonFiltre3ActionPerformed(evt);
            }
        });
        jPanelFiltrage.add(jRadioButtonFiltre3);
        jRadioButtonFiltre3.setBounds(350, 180, 150, 23);

        jRadioButtonFiltre2.setText("Le nom contient");
        jRadioButtonFiltre2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButtonFiltre2ActionPerformed(evt);
            }
        });
        jPanelFiltrage.add(jRadioButtonFiltre2);
        jRadioButtonFiltre2.setBounds(190, 180, 160, 23);
        jPanelFiltrage.add(jTextFieldCritere);
        jTextFieldCritere.setBounds(80, 80, 190, 30);

        jLabel8.setFont(new java.awt.Font("Tahoma", 1, 11));
        jLabel8.setText("Critère");
        jPanelFiltrage.add(jLabel8);
        jLabel8.setBounds(10, 80, 60, 30);

        jButtonFiltrer.setText("Filtrer");
        jButtonFiltrer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonFiltrerActionPerformed(evt);
            }
        });
        jPanelFiltrage.add(jButtonFiltrer);
        jButtonFiltrer.setBounds(10, 120, 140, 23);

        add(jPanelFiltrage);
        jPanelFiltrage.setBounds(300, 400, 520, 210);
    }// </editor-fold>//GEN-END:initComponents

private void jButtonAjouterProjetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAjouterProjetActionPerformed
// TODO add your handling code here:
    mode = EntityManager.AJOUT;
    setEtatSaisie(true);
    activerBoutonsModifSauf(jButtonAjouterProjet);
    jButton4.setText("Ajouter");
    initSaisie();
}//GEN-LAST:event_jButtonAjouterProjetActionPerformed

private void jButtonSupprimerProjetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSupprimerProjetActionPerformed
// TODO add your handling code here:
    mode = EntityManager.SUPPRESSION;
    if(selection != null) {
        ProjetManager.supprimerProjet(selection);
        updateResultat();
    }
}//GEN-LAST:event_jButtonSupprimerProjetActionPerformed

private void jButtonModifierProjetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonModifierProjetActionPerformed
// TODO add your handling code here:
    mode = EntityManager.MODIFICATION;
    setEtatSaisie(true);
    activerBoutonsModifSauf(jButtonModifierProjet);
    jButton4.setText("Modifier");
}//GEN-LAST:event_jButtonModifierProjetActionPerformed

private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
// TODO add your handling code here:
    mode = EntityManager.AUCUNE;
    setEtatSaisie(false);
    if(selection != null)
        afficherProprietesProjet(selection);
    
    activerBoutonsModif();
    jButton4.setText("Valider");
}//GEN-LAST:event_jButton5ActionPerformed

private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
// TODO add your handling code here:
    setEtatSaisie(false);
    
    Projet p;
    
    if(mode.equals(EntityManager.AJOUT)) {
        p = getSaisie();
        ProjetManager.creerProjet(p);
    }
    else if(mode.equals(EntityManager.MODIFICATION)) {
        p = getProprietesProjet();
        ProjetManager.modifierProjet(p);
    }
    
    updateResultat();
    activerBoutonsModif();
    jButton4.setText("Valider");
}//GEN-LAST:event_jButton4ActionPerformed

private void jTableProjetsMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTableProjetsMouseClicked
// TODO add your handling code here:
    int i = jTableProjets.getSelectedRow();
    Projet p = (Projet) resultat.getValueAt(i);
    selection = p;
    afficherProprietesProjet(p);
}//GEN-LAST:event_jTableProjetsMouseClicked

private void formComponentShown(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_formComponentShown
// TODO add your handling code here:
    updateResultat();
}//GEN-LAST:event_formComponentShown

private void jRadioButtonFiltre2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButtonFiltre2ActionPerformed
// TODO add your handling code here:
    selectionnerRadioBouton(jRadioButtonFiltre2);
}//GEN-LAST:event_jRadioButtonFiltre2ActionPerformed

private void jComboBoxFiltrageItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jComboBoxFiltrageItemStateChanged
// TODO add your handling code here:
    int i = jComboBoxFiltrage.getSelectedIndex();
    if(i == 0) {
        setFiltrage(false);
        updateResultat();
        jTextFieldCritere.setText("");
    }
    else if(i == 1)
        setFiltrage(true);
}//GEN-LAST:event_jComboBoxFiltrageItemStateChanged

private void jButtonFiltrerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonFiltrerActionPerformed
// TODO add your handling code here:
    String critere = jTextFieldCritere.getText();
    
    if(jRadioButtonFiltre1.isSelected()) {
        filtrerResultat(critere + "%");
    }else if(jRadioButtonFiltre3.isSelected()) {
        filtrerResultat("%" + critere);
    }else if(jRadioButtonFiltre2.isSelected()) {
        filtrerResultat("%" + critere + "%");
    }
}//GEN-LAST:event_jButtonFiltrerActionPerformed

private void jRadioButtonFiltre1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButtonFiltre1ActionPerformed
// TODO add your handling code here:
    selectionnerRadioBouton(jRadioButtonFiltre1);
}//GEN-LAST:event_jRadioButtonFiltre1ActionPerformed

private void jRadioButtonFiltre3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButtonFiltre3ActionPerformed
// TODO add your handling code here:
    selectionnerRadioBouton(jRadioButtonFiltre3);
}//GEN-LAST:event_jRadioButtonFiltre3ActionPerformed

private void jButtonOuvrirProjetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonOuvrirProjetActionPerformed
// TODO add your handling code here:
    if(selection != null) {
        projet = selection;
        userForm.getUserProjet().setVisible(false);
        userForm.getUserTache().setVisible(true);
        userForm.getUserTache().setProjet(projet);
    }
}//GEN-LAST:event_jButtonOuvrirProjetActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JButton jButtonAjouterProjet;
    private javax.swing.JButton jButtonFiltrer;
    private javax.swing.JButton jButtonModifierProjet;
    private javax.swing.JButton jButtonOuvrirProjet;
    private javax.swing.JButton jButtonSupprimerProjet;
    private javax.swing.JComboBox jComboBoxFiltrage;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabelDebut;
    private javax.swing.JLabel jLabelFin;
    private javax.swing.JLabel jLabelNom;
    private javax.swing.JPanel jPanelFiltrage;
    private javax.swing.JPanel jPanelProjets;
    private javax.swing.JPanel jPanelSaisie;
    private javax.swing.JRadioButton jRadioButtonFiltre1;
    private javax.swing.JRadioButton jRadioButtonFiltre2;
    private javax.swing.JRadioButton jRadioButtonFiltre3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTableProjets;
    private javax.swing.JTextField jTextFieldCritere;
    private javax.swing.JTextField jTextFieldDebut;
    private javax.swing.JTextField jTextFieldFin;
    private javax.swing.JTextField jTextFieldNom;
    // End of variables declaration//GEN-END:variables

    public Projet getProjet() {
        return projet;
    }

    public void setProjet(Projet projet) {
        this.projet = projet;
    }

    public UserForm getUserForm() {
        return userForm;
    }

    public void setUserForm(UserForm userForm) {
        this.userForm = userForm;
    }
}
