package application.user;

import entitymanager.ProjetManager;
import mapping.Projet;
import etat.EtatTaches;
import etat.EtatRessources;
import etat.EtatARessources;
import lwcomponents.canvas.LWCanvas;
import etat.EtatManager;
import utile.PaneManager;
import entitymanager.EntityManager;
import etat.LWLabel;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import lwcomponents.util.LWImgUtil;
import lwcomponents.util.PrintManager;

/**
 *
 * @author  FireWolf
 */
public class UserBudget extends javax.swing.JPanel {
    private UserBudgetMenu userCoutMenu;
    private Projet projet; //Contient l'identité du projet
    private EtatTaches etatTaches;
    private EtatRessources etatRessources;
    private EtatARessources etatARessources;
    private LWCanvas canvasEtat;
    private BufferedImage logo = LWImgUtil.loadImage("img\\components\\LWReport\\logo.bmp");
    private LWCanvas canvasEtatTaches;
    private LWCanvas canvasEtatRessources;
    private LWCanvas canvasEtatARessources;
    private int xPosEtatTache = 0;
    private int yPosEtatTache = 0;
    private int xPosEtatRessource = 0;
    private int yPosEtatRessource = 0;
    private int xPosEtatARessource = 0;
    private int yPosEtatARessource = 0;
    private String modeEtat = EntityManager.ETAT_TACHE;   
    
    /** Creates new form UserTache */
    public UserBudget() {
        initComponents(); 
        initialiserForm();
    }

    private void initialiserForm() {
        
        
        canvasEtat = new LWCanvas() {
            @Override
            public void paintBackground(Graphics g) {
                Graphics2D g2 = (Graphics2D) g;
                
                Rectangle b = canvasEtat.getBounds();
                
                g2.drawImage(logo, 5, 5, b.width - 10, b.height - 10, null);
            }
        };
        canvasEtat.setBounds(PaneManager.obtenirDecalageBounds(jPanelEtat, 2));
        
        etatTaches = new EtatTaches();
        etatRessources = new EtatRessources();
        etatARessources = new EtatARessources();
        canvasEtatTaches = new LWCanvas();
        canvasEtatRessources = new LWCanvas();
        canvasEtatARessources = new LWCanvas();
  
        jPanelEtat.add(canvasEtat);
        jPanelEtat.add(canvasEtatTaches);
        jPanelEtat.add(canvasEtatRessources);
        jPanelEtat.add(canvasEtatARessources);
        
        canvasEtatTaches.setBounds(PaneManager.obtenirDecalageBounds(jPanelEtat, 2));
        canvasEtatRessources.setBounds(PaneManager.obtenirDecalageBounds(jPanelEtat, 2));
        canvasEtatARessources.setBounds(PaneManager.obtenirDecalageBounds(jPanelEtat, 2));
        canvasEtat.setVisible(true);
        canvasEtatTaches.setVisible(false);
        canvasEtatRessources.setVisible(false);
        canvasEtatARessources.setVisible(false);
    }    
    
    public void selectionnerCanevas(LWCanvas canvas) {
        canvasEtatTaches.setVisible(false);
        canvasEtatRessources.setVisible(false);
        canvasEtatARessources.setVisible(false);
        
        canvas.setVisible(true);
        jPanelEtat.repaint();
    }
    
    private void initEtat() {
        //EtatManager.initEtatTaches(etatTaches, canvasEtatTaches); NO
        jPanelEtat.repaint();
        canvasEtatTaches.repaint();
    }

    private void initEtats() {
        etatTaches.viderListe();
        etatRessources.viderListe();
        etatARessources.viderListe();
    } 
    
    private void setPosEtat(int x, int y) {
        if(modeEtat.equals(EntityManager.ETAT_TACHE))
            canvasEtatTaches.setOrigin(x, y);
        else if(modeEtat.equals(EntityManager.ETAT_RESSOURCE))
            canvasEtatRessources.setOrigin(x, y);
        else if(modeEtat.equals(EntityManager.ETAT_ARESSOURCE))
            canvasEtatARessources.setOrigin(x, y);
    }
    
    private void setHScrollEtat(int val) {
        if(modeEtat.equals(EntityManager.ETAT_TACHE)) {
            xPosEtatTache += val;
            setPosEtat(xPosEtatTache, yPosEtatTache);
        }else if(modeEtat.equals(EntityManager.ETAT_RESSOURCE)) {
            xPosEtatRessource += val;
            setPosEtat(xPosEtatRessource, yPosEtatRessource);
        }else if(modeEtat.equals(EntityManager.ETAT_ARESSOURCE)) {
            xPosEtatARessource += val;
            setPosEtat(xPosEtatARessource, yPosEtatARessource);
        }
        initEtat();
    }
    
    private void setVScrollEtat(int val) {
        if(modeEtat.equals(EntityManager.ETAT_TACHE)) {
            yPosEtatTache += val;
            setPosEtat(xPosEtatTache, yPosEtatTache);
        }else if(modeEtat.equals(EntityManager.ETAT_RESSOURCE)) {
            yPosEtatRessource += val;
            setPosEtat(xPosEtatRessource, yPosEtatRessource);
        }else if(modeEtat.equals(EntityManager.ETAT_ARESSOURCE)) {
            yPosEtatARessource += val;
            setPosEtat(xPosEtatARessource, yPosEtatARessource);
        }
        initEtat();
    }
    
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanelTaches = new javax.swing.JPanel();
        jPanelEtat = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jButtonRight = new javax.swing.JButton();
        jButtonPageDown = new javax.swing.JButton();
        jButtonGauche = new javax.swing.JButton();
        jButtonPageUp = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jButtonRetourProjets = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jButtonEtatRessources = new javax.swing.JButton();
        jButtonEtatTaches = new javax.swing.JButton();
        jButtonEtatARessources = new javax.swing.JButton();

        setPreferredSize(new java.awt.Dimension(830, 660));
        addComponentListener(new java.awt.event.ComponentAdapter() {
            public void componentHidden(java.awt.event.ComponentEvent evt) {
                formComponentHidden(evt);
            }
            public void componentShown(java.awt.event.ComponentEvent evt) {
                formComponentShown(evt);
            }
        });
        setLayout(null);

        jPanelTaches.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanelTaches.setLayout(null);

        jPanelEtat.setBackground(new java.awt.Color(255, 255, 255));
        jPanelEtat.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanelEtat.setLayout(null);
        jPanelTaches.add(jPanelEtat);
        jPanelEtat.setBounds(10, 10, 790, 400);

        jPanel3.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel3.setLayout(null);

        jButtonRight.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonRightActionPerformed(evt);
            }
        });
        jPanel3.add(jButtonRight);
        jButtonRight.setBounds(70, 30, 33, 20);

        jButtonPageDown.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonPageDownActionPerformed(evt);
            }
        });
        jPanel3.add(jButtonPageDown);
        jButtonPageDown.setBounds(40, 50, 30, 20);

        jButtonGauche.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonGaucheActionPerformed(evt);
            }
        });
        jPanel3.add(jButtonGauche);
        jButtonGauche.setBounds(10, 30, 30, 20);

        jButtonPageUp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonPageUpActionPerformed(evt);
            }
        });
        jPanel3.add(jButtonPageUp);
        jButtonPageUp.setBounds(40, 10, 30, 20);

        jButton1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jButton1.setForeground(new java.awt.Color(0, 0, 204));
        jButton1.setText("Imprimer les etats");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jPanel3.add(jButton1);
        jButton1.setBounds(150, 50, 240, 23);

        jPanelTaches.add(jPanel3);
        jPanel3.setBounds(10, 420, 790, 80);

        add(jPanelTaches);
        jPanelTaches.setBounds(10, 50, 810, 510);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder()));
        jPanel1.setLayout(null);

        jButtonRetourProjets.setFont(new java.awt.Font("Tahoma", 1, 11));
        jButtonRetourProjets.setForeground(new java.awt.Color(0, 51, 204));
        jButtonRetourProjets.setText("Retourner à la liste de projets");
        jButtonRetourProjets.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonRetourProjetsActionPerformed(evt);
            }
        });
        jPanel1.add(jButtonRetourProjets);
        jButtonRetourProjets.setBounds(10, 10, 240, 23);

        add(jPanel1);
        jPanel1.setBounds(10, 570, 810, 40);

        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel2.setLayout(null);

        jButtonEtatRessources.setFont(new java.awt.Font("Tahoma", 1, 11));
        jButtonEtatRessources.setForeground(new java.awt.Color(0, 0, 204));
        jButtonEtatRessources.setText("Etat des ressources");
        jButtonEtatRessources.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonEtatRessourcesActionPerformed(evt);
            }
        });
        jPanel2.add(jButtonEtatRessources);
        jButtonEtatRessources.setBounds(280, 10, 250, 23);

        jButtonEtatTaches.setFont(new java.awt.Font("Tahoma", 1, 11));
        jButtonEtatTaches.setForeground(new java.awt.Color(0, 0, 204));
        jButtonEtatTaches.setText("Etat des tâches");
        jButtonEtatTaches.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonEtatTachesActionPerformed(evt);
            }
        });
        jPanel2.add(jButtonEtatTaches);
        jButtonEtatTaches.setBounds(10, 10, 250, 23);

        jButtonEtatARessources.setFont(new java.awt.Font("Tahoma", 1, 11));
        jButtonEtatARessources.setForeground(new java.awt.Color(0, 0, 204));
        jButtonEtatARessources.setText("Etat des autres ressources");
        jButtonEtatARessources.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonEtatARessourcesActionPerformed(evt);
            }
        });
        jPanel2.add(jButtonEtatARessources);
        jButtonEtatARessources.setBounds(550, 10, 250, 23);

        add(jPanel2);
        jPanel2.setBounds(10, 10, 810, 40);
    }// </editor-fold>//GEN-END:initComponents

private void formComponentShown(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_formComponentShown
// TODO add your handling code here: 
    canvasEtatTaches.clearShapes();
    canvasEtatRessources.clearShapes();
    canvasEtatARessources.clearShapes();
    BufferedImage tmp = LWImgUtil.loadImage("img\\components\\LWReport\\Status1.bmp");
    
    //Etape1 : Chargement de l'etat des taches
    Graphics2D g2 = (Graphics2D) jPanelEtat.getGraphics();
    g2.drawImage(tmp, 10, 320, 450, 60, null);
    ProjetManager.EvaluerEtatTaches(getProjet().getIdProjet(), etatTaches);
    EtatManager.initEtatTaches(projet, etatTaches, canvasEtatTaches);
    
    

    //Etape2 : Chargement des etats des ressources
    tmp = LWImgUtil.loadImage("img\\components\\LWReport\\Status2.bmp");
    g2.drawImage(tmp, 10, 320, 450, 60, null);
    ProjetManager.EvaluerEtatRessources(getProjet().getIdProjet(), etatRessources);
    EtatManager.initEtatRessources(projet, etatRessources, canvasEtatRessources);
    g2.setColor(Color.BLUE);
    

    //Etape3 : Chargement des etats des autres ressources
    tmp = LWImgUtil.loadImage("img\\components\\LWReport\\Status3.bmp");
    g2.drawImage(tmp, 10, 320, 450, 60, null);
    ProjetManager.EvaluerEtatARessources(getProjet().getIdProjet(), etatARessources);
    EtatManager.initEtatARessources(projet, etatARessources, canvasEtatARessources);

    canvasEtat.setVisible(false); //Une fois chargé , on bascule
    canvasEtatTaches.setVisible(true);
    g2.setColor(Color.GREEN);
    tmp = LWImgUtil.loadImage("img\\components\\LWReport\\Status4.bmp");
    g2.drawImage(tmp, 10, 320, 450, 60, null);

    initEtats(); //new since 23/12/2008

    jPanelEtat.repaint();
}//GEN-LAST:event_formComponentShown

private void jButtonRetourProjetsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonRetourProjetsActionPerformed
// TODO add your handling code here:
    this.setVisible(false);
    canvasEtat.setVisible(true);
    canvasEtatTaches.setVisible(false);
    canvasEtatRessources.setVisible(false);
    canvasEtatARessources.setVisible(false);
    
    getUserCoutMenu().setVisible(true);
}//GEN-LAST:event_jButtonRetourProjetsActionPerformed

private void jButtonEtatTachesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonEtatTachesActionPerformed
// TODO add your handling code here:
    selectionnerCanevas(canvasEtatTaches);
    modeEtat = EntityManager.ETAT_TACHE;
}//GEN-LAST:event_jButtonEtatTachesActionPerformed

private void jButtonEtatRessourcesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonEtatRessourcesActionPerformed
// TODO add your handling code here:
    selectionnerCanevas(canvasEtatRessources);
    modeEtat = EntityManager.ETAT_RESSOURCE;
}//GEN-LAST:event_jButtonEtatRessourcesActionPerformed

private void jButtonEtatARessourcesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonEtatARessourcesActionPerformed
// TODO add your handling code here:
    selectionnerCanevas(canvasEtatARessources);
    modeEtat = EntityManager.ETAT_ARESSOURCE;
}//GEN-LAST:event_jButtonEtatARessourcesActionPerformed

private void jButtonPageUpActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonPageUpActionPerformed
// TODO add your handling code here:
    setVScrollEtat(10);
}//GEN-LAST:event_jButtonPageUpActionPerformed

private void jButtonPageDownActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonPageDownActionPerformed
// TODO add your handling code here:
    setVScrollEtat(-10);
}//GEN-LAST:event_jButtonPageDownActionPerformed

private void jButtonGaucheActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonGaucheActionPerformed
// TODO add your handling code here:
    setHScrollEtat(-10);
}//GEN-LAST:event_jButtonGaucheActionPerformed

private void jButtonRightActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonRightActionPerformed
// TODO add your handling code here:
    setHScrollEtat(10);
}//GEN-LAST:event_jButtonRightActionPerformed

private void formComponentHidden(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_formComponentHidden
// TODO add your handling code here:
    canvasEtat.setVisible(true);
    canvasEtatTaches.setVisible(false);
    canvasEtatRessources.setVisible(false);
    canvasEtatARessources.setVisible(false);
}//GEN-LAST:event_formComponentHidden

private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
// TODO add your handling code here:
    PrintManager pm = new PrintManager(jPanelEtat.getComponents());
    pm.print();
}//GEN-LAST:event_jButton1ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButtonEtatARessources;
    private javax.swing.JButton jButtonEtatRessources;
    private javax.swing.JButton jButtonEtatTaches;
    private javax.swing.JButton jButtonGauche;
    private javax.swing.JButton jButtonPageDown;
    private javax.swing.JButton jButtonPageUp;
    private javax.swing.JButton jButtonRetourProjets;
    private javax.swing.JButton jButtonRight;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanelEtat;
    private javax.swing.JPanel jPanelTaches;
    // End of variables declaration//GEN-END:variables

    /**
     * Obtenir le projet correspondant
     * @return
     * Instance du projet
     */
    public Projet getProjet() {
        return projet;
    }
    
    /**
     * Spécifier le projet à ouvrir
     * @param projet
     * Instance du projet
     */
    public void setProjet(Projet projet) {
        this.projet = projet;
    }

    public UserBudgetMenu getUserCoutMenu() {
        return userCoutMenu;
    }

    public void setUserGanttMenu(UserBudgetMenu userCoutMenu) {
        this.setUserCoutMenu(userCoutMenu);
    }

    public void setUserCoutMenu(UserBudgetMenu userCoutMenu) {
        this.userCoutMenu = userCoutMenu;
    }
}
