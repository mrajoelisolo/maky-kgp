package application.user;

import entitymanager.ARessourceManager;
import entitymanager.RessourceManager;
import entitymanager.EntityManager;
import entitymanager.ExceptionRessourceManager;
import mapping.Projet;
import entitymanager.TacheManager;
import java.util.Calendar;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultTreeModel;
import mapping.ARessource;
import mapping.Noeud;
import mapping.Ressource;
import mapping.Tache;
import utile.ConversionDate;
import utile.FormCalendrier;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import javax.swing.JCheckBox;
import mapping.ExceptionRessource;

/**
 *
 * @author  FireWolf
 */
public class UserTache extends javax.swing.JPanel {
    private UserProjet userProjet;
    private Projet projet; //Contient l'identité du projet
    private Noeud selection; //La tâche séléctionné en cours
    private Noeud racine; //La racine représentant le projet
    private String mode; //Mode d'édition, ajout, modification ou suppressions
    private DefaultTreeModel modele;
    private DefaultListModel lmRessource;
    private DefaultListModel lmARessource;
    private Set memTamponRes = new HashSet(); //Mémoire tampon pour les ressources
    private Set memTamponARes = new HashSet(); //Mémoire tampon pour les ressources matérielles
    private List bufferRes = new LinkedList(); //Sert de mémoire tampon
    
    private JDialog dlg = new JDialog();
    private FormCalendrier cld = new FormCalendrier(dlg, 50, 50, false, "Date");
    
    /** Creates new form UserTache */
    public UserTache() {
        initComponents(); 
        initialiserForm();
    }

    private void initialiserForm() {
        //Les tâches sont initialisées lorsque le composant est affiché, donc rien ici
        setEtatSaisie(false);
        
        //Evenement qui écoute la sélection des tâches
        jTree1.addTreeSelectionListener(new TreeSelectionListener() {
            public void valueChanged(TreeSelectionEvent e) {
                Noeud noeud = (Noeud) jTree1.getLastSelectedPathComponent();
                
                if(noeud != null) { 
                    selection = noeud;
                    
                    afficherProprietesTache(noeud.getTache());
                }
            }
        });
        
        lmRessource = new DefaultListModel();
        lmARessource = new DefaultListModel();
        jListRessources.setModel(lmRessource);
        jListARessources.setModel(lmARessource);
        //jComboBoxComparaison.setEnabled(false);
    }
     
    /**
     * @since 30/12/2008
     * Initialiser les tâches a afficher
     * @param p
     * Le projet qui contient les tâches
     */
    private void initTaches(Projet p) {
        racine = TacheManager.chargerTaches(p.getIdProjet());
        modele = new DefaultTreeModel(racine);
        jTree1.setModel(modele);
        TacheManager.deployerJTree(jTree1); //New since 30/12/2008
    }
    
    private void recharger(Projet p) {
        initTaches(p);
        modele.reload(racine);
        jTree1.repaint();
    }
    
    /**
     * Active ou désactive la zone de saisie
     * @param etat
     * si true alors la saisie est permise
     */
    private void setEtatSaisie(boolean etat) {
        jTextFieldNom.setEditable(etat); //Nom de la tâches
        jTextFieldDebut.setEditable(etat); // Date de début
        jTextFieldFin.setEditable(etat); // Date de fin
        jButtonDebut.setEnabled(etat); //Date de début
        jButtonFin.setEnabled(etat); //Date de fin
        jButtonValiderSaisie.setEnabled(etat); //Valider
        jButtonAnnulerSaisie.setEnabled(etat); //Annuler
        jListRessources.setEnabled(etat); //Liste des ressources
        jButtonAjouterRessource.setEnabled(etat); //Ajouter des ressources
        jButtonEnleverRessource.setEnabled(etat); //Enlever des ressources
        jButtonAjouterARessource.setEnabled(etat);
        jButtonEnleverARessource.setEnabled(etat);
        jListRessources.setEnabled(etat);
        jListARessources.setEnabled(etat);
        jTree1.setEnabled(!etat);
        //jCheckBoxComparaison.setEnabled(etat);
    }
    
    /**
     * Afficher les propriétés du profil dans la zone de saisie
     * @param p
     * Instance d'un profil
     */
    private void afficherProprietesTache(Tache t) {
        jTextFieldNom.setText(t.getNomTache());
        jTextFieldDebut.setText(ConversionDate.dateToString(t.getDebutTache()));
        jTextFieldFin.setText(ConversionDate.dateToString(t.getFinTache()));
        //jCheckBoxComparaison.setSelected(t.getEtat());
        
        lmRessource.clear();
        for(Iterator iter = t.getRessources().iterator(); iter.hasNext();) {
            Ressource r = (Ressource) iter.next();
            lmRessource.addElement(r.getPseudo());
        }
        lmARessource.clear();
        for(Iterator iter = t.getAressources().iterator(); iter.hasNext();)  {
            ARessource ar = (ARessource) iter.next();
            lmARessource.addElement(ar.getNomARessource());
        }
    }
    
    /**
     * Obtenir les propriétés du profil dans le but d'une modification
     * @return
     * Instance d'un profil
     */
    private Noeud getProprietesTache() {
        Noeud noeud = selection;
        Tache t = noeud.getTache();
        t.setNomTache(jTextFieldNom.getText());
        t.setDebutTache(ConversionDate.stringToDate(jTextFieldDebut.getText()));
        t.setFinTache(ConversionDate.stringToDate(jTextFieldFin.getText()));
        //t.setEtat(jCheckBoxComparaison.isSelected());
        return noeud;
    }
    
    /**
     * Obtenir la tache saisie dans le but d'un ajout
     * @return
     * Instance de la tâche
     */
    private Tache getSaisie() {
        Tache t = new Tache();
        t.setNomTache(jTextFieldNom.getText());
        t.setDebutTache(ConversionDate.stringToDate(jTextFieldDebut.getText()));
        t.setFinTache(ConversionDate.stringToDate(jTextFieldFin.getText()));
        return t;
    }
    
    /**
     * Initialiser la zone de saisie pour effectuer un ajout de nouvelle tâche
     */
    private void initSaisie() {
        Date date = new Date();
        Calendar cald = Calendar.getInstance();
        cald.setTime(date);
        cald.add(Calendar.DAY_OF_MONTH, 1);
        
        jTextFieldNom.setText("");
        jTextFieldDebut.setText(ConversionDate.dateToString(date));
        date = cld.getDate();
        jTextFieldFin.setText(ConversionDate.dateToString(date));
        lmRessource.clear();
        lmARessource.clear();
        //jCheckBoxComparaison.setSelected(false);
        memTamponRes.clear();
    }
    
    /**
     * Activer tous les boutons de modifications (ajout, suppression, modification)
     */
    public void activerBoutonsModif() {
        jButtonAjouterTache.setEnabled(true);
        jButtonModifierTache.setEnabled(true);
        jButtonSupprimerTache.setEnabled(true);
    }
    
    /**
     * Définir l'état des boutons de modifications
     */
    public void setBoutonsModif(boolean etat) {
        jButtonAjouterTache.setEnabled(etat);
        jButtonModifierTache.setEnabled(etat);
        jButtonSupprimerTache.setEnabled(etat);
    }
    
    /**
     * Activer tous les boutons de modifications sauf un seul
     * @param b
     * Le seul bouton qui ne doit pas être activé
     */
    public void activerBoutonsModifSauf(JButton b) {
        activerBoutonsModif();
        b.setEnabled(false);
    }
    
    public String ajouterRessource() {
        Object[] composant = new Object[3];
        composant[0] = "Sélectionner la ressource à ajouter";
        
        JComboBox cb = new JComboBox();
        RessourceManager.chargerRessources(cb);
        composant[1] = cb;
        
        JCheckBox checkB = new JCheckBox();
        checkB.setText("Selectionnable");
        composant[2] = checkB;
        
        String option[] = {"Ajouter", "Annuler"};
        
        int res = JOptionPane.showOptionDialog(this, composant, "Ajout d'une ressource", 
                JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE, null, option, option[0]);
        
        String resultat = "";
        if(res == 0) {
            //Si l'affectation est séléctionnable, alors on mémorise vers la mémoire tampon
            if(checkB.isSelected()) {
                ExceptionRessource er = new ExceptionRessource();
                Ressource r = RessourceManager.chargerRessource((String) cb.getSelectedItem());
                er.setIdRessource(r.getIdRessource());
                er.setIdTache(selection.getTache().getIdTache());
                bufferRes.add(er);
            }
            
            resultat = (String) cb.getSelectedItem();
        }
        
        return resultat;
    }
    
    public String ajouterARessource() {
        Object[] composant = new Object[3];
        composant[0] = "Sélectionner la ressource matérielle à ajouter";
        
        JComboBox cb = new JComboBox();
        ARessourceManager.chargerARessources(cb);
        composant[1] = cb;
        
        String option[] = {"Ajouter", "Annuler"};
        
        int res = JOptionPane.showOptionDialog(this, composant, "Ajout d'une ressource matérielle", 
                JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE, null, option, option[0]);
        
        String resultat = "";
        if(res == 0)
            resultat = (String) cb.getSelectedItem();
        
        return resultat;
    }
    
    private boolean memTamponResContient(String elt) {
        boolean resultat = false;
        
        for(Iterator iter = memTamponRes.iterator(); iter.hasNext();) {
            Ressource r = (Ressource) iter.next();
            if(r.getPseudo().equals(elt)) {
                resultat = true;
                break;
            }    
        }
        
        return resultat;
    }
    
    private boolean memTamposAResContient(String elt) {
       boolean resultat = false;
        
        for(Iterator iter = memTamponARes.iterator(); iter.hasNext();) {
            ARessource r = (ARessource) iter.next();
            if(r.getNomARessource().equals(elt)) {
                resultat = true;
                break;
            }    
        }
        
        return resultat; 
    }
    
    private void validerExceptionRessource() {
        //05-01-09
        //Validation des ressources dites 'exceptions'
        for(Iterator iter = bufferRes.iterator(); iter.hasNext();) {
            ExceptionRessource er = (ExceptionRessource) iter.next();
            ExceptionRessourceManager.ajouter(er);
        }
    }
    
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanelTaches = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTree1 = new javax.swing.JTree();
        jButtonAjouterTache = new javax.swing.JButton();
        jButtonModifierTache = new javax.swing.JButton();
        jButtonSupprimerTache = new javax.swing.JButton();
        jButtonRetourProjets = new javax.swing.JButton();
        jPanelSaisie = new javax.swing.JPanel();
        jButtonValiderSaisie = new javax.swing.JButton();
        jButtonAnnulerSaisie = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jListRessources = new javax.swing.JList();
        jButtonAjouterRessource = new javax.swing.JButton();
        jButtonEnleverRessource = new javax.swing.JButton();
        jPanel5 = new javax.swing.JPanel();
        jTextFieldFin = new javax.swing.JTextField();
        jButtonFin = new javax.swing.JButton();
        jButtonDebut = new javax.swing.JButton();
        jTextFieldDebut = new javax.swing.JTextField();
        jLabelFin = new javax.swing.JLabel();
        jLabelDebut = new javax.swing.JLabel();
        jLabelNom = new javax.swing.JLabel();
        jTextFieldNom = new javax.swing.JTextField();
        jPanel6 = new javax.swing.JPanel();
        jButtonAjouterARessource = new javax.swing.JButton();
        jButtonEnleverARessource = new javax.swing.JButton();
        jScrollPane3 = new javax.swing.JScrollPane();
        jListARessources = new javax.swing.JList();

        setPreferredSize(new java.awt.Dimension(830, 660));
        addComponentListener(new java.awt.event.ComponentAdapter() {
            public void componentShown(java.awt.event.ComponentEvent evt) {
                formComponentShown(evt);
            }
        });
        setLayout(null);

        jPanelTaches.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder()), "Arborescence des tâches", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Tahoma", 1, 12))); // NOI18N
        jPanelTaches.setLayout(null);

        jTree1.setModel(null);
        jScrollPane1.setViewportView(jTree1);

        jPanelTaches.add(jScrollPane1);
        jScrollPane1.setBounds(10, 30, 420, 460);

        jButtonAjouterTache.setText("Ajouter");
        jButtonAjouterTache.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAjouterTacheActionPerformed(evt);
            }
        });
        jPanelTaches.add(jButtonAjouterTache);
        jButtonAjouterTache.setBounds(10, 500, 100, 23);

        jButtonModifierTache.setText("Modifier");
        jButtonModifierTache.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonModifierTacheActionPerformed(evt);
            }
        });
        jPanelTaches.add(jButtonModifierTache);
        jButtonModifierTache.setBounds(130, 500, 100, 23);

        jButtonSupprimerTache.setText("Supprimer");
        jButtonSupprimerTache.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSupprimerTacheActionPerformed(evt);
            }
        });
        jPanelTaches.add(jButtonSupprimerTache);
        jButtonSupprimerTache.setBounds(250, 500, 100, 23);

        jButtonRetourProjets.setFont(new java.awt.Font("Tahoma", 1, 11));
        jButtonRetourProjets.setForeground(new java.awt.Color(0, 51, 204));
        jButtonRetourProjets.setText("Retourner à la liste de projets");
        jButtonRetourProjets.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonRetourProjetsActionPerformed(evt);
            }
        });
        jPanelTaches.add(jButtonRetourProjets);
        jButtonRetourProjets.setBounds(10, 570, 240, 23);

        add(jPanelTaches);
        jPanelTaches.setBounds(10, 10, 440, 600);

        jPanelSaisie.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(), "Propriétés de la tâche", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Tahoma", 1, 12))); // NOI18N
        jPanelSaisie.setLayout(null);

        jButtonValiderSaisie.setLabel("Valider");
        jButtonValiderSaisie.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonValiderSaisieActionPerformed(evt);
            }
        });
        jPanelSaisie.add(jButtonValiderSaisie);
        jButtonValiderSaisie.setBounds(10, 570, 120, 23);

        jButtonAnnulerSaisie.setLabel("Annuler");
        jButtonAnnulerSaisie.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAnnulerSaisieActionPerformed(evt);
            }
        });
        jPanelSaisie.add(jButtonAnnulerSaisie);
        jButtonAnnulerSaisie.setBounds(230, 570, 120, 23);

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(), "Affectation de ressources humaines", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Tahoma", 1, 12))); // NOI18N
        jPanel3.setForeground(new java.awt.Color(0, 0, 204));
        jPanel3.setLayout(null);

        jScrollPane2.setViewportView(jListRessources);

        jPanel3.add(jScrollPane2);
        jScrollPane2.setBounds(110, 30, 220, 110);

        jButtonAjouterRessource.setText("Ajouter");
        jButtonAjouterRessource.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAjouterRessourceActionPerformed(evt);
            }
        });
        jPanel3.add(jButtonAjouterRessource);
        jButtonAjouterRessource.setBounds(10, 30, 90, 23);

        jButtonEnleverRessource.setText("Enlever");
        jButtonEnleverRessource.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonEnleverRessourceActionPerformed(evt);
            }
        });
        jPanel3.add(jButtonEnleverRessource);
        jButtonEnleverRessource.setBounds(10, 120, 90, 23);

        jPanelSaisie.add(jPanel3);
        jPanel3.setBounds(10, 230, 340, 160);

        jPanel5.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel5.setLayout(null);

        jTextFieldFin.setBackground(new java.awt.Color(223, 224, 224));
        jTextFieldFin.setEditable(false);
        jTextFieldFin.setText("11-11-2008");
        jPanel5.add(jTextFieldFin);
        jTextFieldFin.setBounds(150, 130, 130, 30);

        jButtonFin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonFinActionPerformed(evt);
            }
        });
        jPanel5.add(jButtonFin);
        jButtonFin.setBounds(290, 140, 30, 20);

        jButtonDebut.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonDebutActionPerformed(evt);
            }
        });
        jPanel5.add(jButtonDebut);
        jButtonDebut.setBounds(290, 80, 30, 20);

        jTextFieldDebut.setBackground(new java.awt.Color(223, 224, 224));
        jTextFieldDebut.setEditable(false);
        jTextFieldDebut.setText("11-11-2008");
        jPanel5.add(jTextFieldDebut);
        jTextFieldDebut.setBounds(150, 70, 130, 30);

        jLabelFin.setFont(new java.awt.Font("Tahoma", 1, 11));
        jLabelFin.setText("Fin");
        jPanel5.add(jLabelFin);
        jLabelFin.setBounds(10, 130, 60, 30);

        jLabelDebut.setFont(new java.awt.Font("Tahoma", 1, 11));
        jLabelDebut.setText("Début");
        jPanel5.add(jLabelDebut);
        jLabelDebut.setBounds(10, 70, 70, 30);

        jLabelNom.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabelNom.setText("Nom");
        jPanel5.add(jLabelNom);
        jLabelNom.setBounds(10, 10, 50, 30);
        jPanel5.add(jTextFieldNom);
        jTextFieldNom.setBounds(70, 10, 260, 30);

        jPanelSaisie.add(jPanel5);
        jPanel5.setBounds(10, 30, 340, 190);

        jPanel6.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(), "Autres ressources", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Tahoma", 1, 11))); // NOI18N
        jPanel6.setLayout(null);

        jButtonAjouterARessource.setText("Ajouter");
        jButtonAjouterARessource.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAjouterARessourceActionPerformed(evt);
            }
        });
        jPanel6.add(jButtonAjouterARessource);
        jButtonAjouterARessource.setBounds(10, 30, 90, 23);

        jButtonEnleverARessource.setText("Enlever");
        jButtonEnleverARessource.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonEnleverARessourceActionPerformed(evt);
            }
        });
        jPanel6.add(jButtonEnleverARessource);
        jButtonEnleverARessource.setBounds(10, 130, 90, 23);

        jScrollPane3.setViewportView(jListARessources);

        jPanel6.add(jScrollPane3);
        jScrollPane3.setBounds(110, 20, 220, 130);

        jPanelSaisie.add(jPanel6);
        jPanel6.setBounds(10, 390, 340, 170);

        add(jPanelSaisie);
        jPanelSaisie.setBounds(460, 10, 360, 600);
    }// </editor-fold>//GEN-END:initComponents

private void jButtonModifierTacheActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonModifierTacheActionPerformed
// TODO add your handling code here:
    mode = EntityManager.MODIFICATION;
    setEtatSaisie(true);
    activerBoutonsModifSauf(jButtonModifierTache);
    bufferRes.clear(); //05-01-09
    jButtonValiderSaisie.setText("Modifier");
}//GEN-LAST:event_jButtonModifierTacheActionPerformed

private void jButtonValiderSaisieActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonValiderSaisieActionPerformed
// TODO add your handling code here:
    setEtatSaisie(false);
    
    if(mode.equals(EntityManager.AJOUT)) {
        Tache t = getSaisie();

        if(selection == null) { 
            //Si aucune selection n'est créée: Créer une tâche principale
            //On ajoute une tâche de niveau 2 (on considère que la racine est de niveau 1)
            Noeud rac = (Noeud) jTree1.getModel().getRoot();

            t.setNiveau(2);
            t.setPred(rac.getTache().getIdTache());
        } 
        else {  
            //Si aucune selection n'est créée: Création de sous-tâche
            //On ajoute une tâche de niveau supérieure à 2 (3 et plus)
            t.setNiveau(selection.getTache().getNiveau() + 1);
            t.setPred(selection.getTache().getIdTache());
        }
        t.getProjet().setIdProjet(projet.getIdProjet());
        t.setRessources(memTamponRes);
        TacheManager.ajouterTache(t);
        
        //Vérification de l'arborescence sur l'affectation de ressources
        for(Iterator iter = memTamponRes.iterator(); iter.hasNext();) {
            Ressource r = (Ressource) iter.next();
            RessourceManager.mettreAJourAffectationDesc(t, r);
        }
        
        for(Iterator iter = memTamponARes.iterator(); iter.hasNext();) {
            ARessource r = (ARessource) iter.next();
            ARessourceManager.mettreAJourAffectationDesc(t, r);
        }
        
        validerExceptionRessource();
        
        recharger(projet);
    }
    else if(mode.equals(EntityManager.MODIFICATION)) {
        Tache t = getProprietesTache().getTache();
        Noeud noeud = getProprietesTache();
        
        //Si le niveau de la tâche dépasse 1, alors on doit vérifier
        //L'état des dates des tâches mères
        if(t.getNiveau() > 1)
            TacheManager.mettreAJourDateCheminDesc(t);
        //Si la tâche possede des filles, alors il faut vérifier l'état
        //de ces tâches filles
        if(!noeud.isLeaf())
            TacheManager.mettreAJourDateCheminAsc(t);
        
        validerExceptionRessource();
        
        TacheManager.modifierTache(t);
        
        recharger(projet);
    }
    
    activerBoutonsModif();
    modele.reload();
    TacheManager.deployerJTree(jTree1); //New since 30/12/2008
    jTree1.repaint();
}//GEN-LAST:event_jButtonValiderSaisieActionPerformed

private void jButtonAnnulerSaisieActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAnnulerSaisieActionPerformed
// TODO add your handling code here: 
    mode = EntityManager.AUCUNE;
    setEtatSaisie(false);
    if(selection != null)
        afficherProprietesTache(selection.getTache());
    
    activerBoutonsModif();
    jButtonValiderSaisie.setText("Valider");
}//GEN-LAST:event_jButtonAnnulerSaisieActionPerformed

private void formComponentShown(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_formComponentShown
// TODO add your handling code here:
    initTaches(userProjet.getProjet()); //Initialise les tâches du projet séléctionné
}//GEN-LAST:event_formComponentShown

private void jButtonAjouterTacheActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAjouterTacheActionPerformed
// TODO add your handling code here:
    mode = EntityManager.AJOUT;
    setEtatSaisie(true);
    activerBoutonsModifSauf(jButtonAjouterTache);
    jButtonValiderSaisie.setText("Ajouter");
    bufferRes.clear(); //05-01-09
    initSaisie();
}//GEN-LAST:event_jButtonAjouterTacheActionPerformed

private void jButtonSupprimerTacheActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSupprimerTacheActionPerformed
// TODO add your handling code here:
    mode = EntityManager.SUPPRESSION;
    if(selection != null) {
        //Si la tâche séléctionnée n'a pas d'enfants, alors il suffit de ne supprimer qu'une
        //seule tâche
        if(selection.isLeaf()) {
            Object[] options = {"Oui", "Non"};
            int n = JOptionPane.showOptionDialog(this, "Etes vous sur de supprimer cette tâche ?", "Supprimer",
            JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[1]);
            
            if(n == 0) {
                TacheManager.supprimerTache(selection.getTache());
                //modele.removeNodeFromParent(selection);
                recharger(projet);
                JOptionPane.showMessageDialog(null, "La tâche a été supprimée");
            }
        }
        //Sinon on procède a un algorithme qui utilise un processus récursif
        //Ce procédé enlève la tâche mère ainsi que toutes les tâches filles
        //Jusqu'à la dernière génération
        else
        {
            Object[] options = {"Oui", "Non"};
            int n = JOptionPane.showOptionDialog(this, "Cette tâche contient d'autres tâches, procéder ?", "Supprimer",
            JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[1]);
        
            if(n == 0) {
                Tache tSup = TacheManager.creerArborescence(projet.getIdProjet()); //Charge l'intégralité des tâches
                Noeud rech = new Noeud();
                TacheManager.rechercher(tSup, rech,selection.getTache().getIdTache()); //Recherche la tâche cible à supprimer          
                TacheManager.supprimerArborescence(rech.getTache());
                recharger(projet);
                JOptionPane.showMessageDialog(null, "Les tâches ont été supprimées");
            }
        }        
        TacheManager.deployerJTree(jTree1); //New since 30/12/2008
    } else
        JOptionPane.showMessageDialog(null, "Aucune tâche n'a été sélectionnée");
}//GEN-LAST:event_jButtonSupprimerTacheActionPerformed

private void jButtonDebutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonDebutActionPerformed
// TODO add your handling code here:
    cld.setVisible(true);
    
    if(cld.isOk()) {
        jTextFieldDebut.setText(ConversionDate.dateToString(cld.getDate()));
    }
}//GEN-LAST:event_jButtonDebutActionPerformed

private void jButtonFinActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonFinActionPerformed
// TODO add your handling code here:
    cld.setVisible(true);
    
    if(cld.isOk()) {
        jTextFieldFin.setText(ConversionDate.dateToString(cld.getDate()));
    }
}//GEN-LAST:event_jButtonFinActionPerformed

private void jButtonRetourProjetsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonRetourProjetsActionPerformed
// TODO add your handling code here:
    this.setVisible(false);
    getUserProjet().setVisible(true);
}//GEN-LAST:event_jButtonRetourProjetsActionPerformed

private void jButtonAjouterRessourceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAjouterRessourceActionPerformed
// TODO add your handling code here:
    String res = ajouterRessource();
    if(!res.equals("")) {
        Ressource r = RessourceManager.chargerRessource(res);
        
        if(mode.equals(EntityManager.MODIFICATION)) {
            if(!RessourceManager.isRessourceAffectee(selection.getTache(), res)) {
                Tache t = selection.getTache();
                lmRessource.addElement(res);

                t.getRessources().add(r); //Ajoute la ressource
                //TacheManager.modifierTache(t); //Prkoi on n'a pa besoin de ça???
                RessourceManager.mettreAJourAffectationDesc(t, r);
            }else
                JOptionPane.showMessageDialog(this, "Vous avez déja affecté cette ressource");
        }else if(mode.equals(EntityManager.AJOUT)) {
            //Affectation de ressources pour une nouvelle tâche
            if(!memTamponResContient(r.getPseudo())) {
                memTamponRes.add(r);
                lmRessource.addElement(r.getPseudo());
            }
            else
                JOptionPane.showMessageDialog(this, "Vous avez déja affecté cette ressource");
        }
    }
}//GEN-LAST:event_jButtonAjouterRessourceActionPerformed

private void jButtonEnleverRessourceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonEnleverRessourceActionPerformed
// TODO add your handling code here:
    Object[] options = {"Oui", "Non"};
        int n = JOptionPane.showOptionDialog(this, "Etes vous sur de vouloir désaffecter cette ressource?", "Désaffectation de ressource",
        JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[1]);
        
    if(n == 0) {
        Tache t = selection.getTache();
        String nomRes = (String) jListRessources.getSelectedValue();
        int i = jListRessources.getSelectedIndex();
        lmRessource.remove(i);
        Ressource r = RessourceManager.chargerRessource(nomRes);

        int idRessource = r.getIdRessource();
        RessourceManager.desaffecterArborescence(t, idRessource);
        
        //05/01/08
        //ExceptionRessourceManager.enleverException(selection.getTache().getIdTache(), idRessource);
    }
}//GEN-LAST:event_jButtonEnleverRessourceActionPerformed

private void jButtonAjouterARessourceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAjouterARessourceActionPerformed
// TODO add your handling code here:
    String res = ajouterARessource();
    if(!res.equals("")) {
        ARessource r = ARessourceManager.chargerARessource(res);
        
        if(mode.equals(EntityManager.MODIFICATION)) {
            if(!ARessourceManager.isARessourceAffectee(selection.getTache(), res)) {
                Tache t = selection.getTache();
                lmARessource.addElement(res);

                t.getAressources().add(r); //Ajoute la ressource
                //TacheManager.modifierTache(t); //Prkoi on n'a pa besoin de ça???
                ARessourceManager.mettreAJourAffectationDesc(t, r);
            }else
                JOptionPane.showMessageDialog(this, "Vous avez déja affecté cette ressource matérielle");
        }else if(mode.equals(EntityManager.AJOUT)) {
            //Affectation de ressources pour une nouvelle tâche
            if(!memTamposAResContient(r.getNomARessource())) {
                memTamponARes.add(r);
                lmARessource.addElement(r.getNomARessource());
            }
            else
                JOptionPane.showMessageDialog(this, "Vous avez déja affecté cette ressource matérielle");
        }
    }
}//GEN-LAST:event_jButtonAjouterARessourceActionPerformed

private void jButtonEnleverARessourceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonEnleverARessourceActionPerformed
// TODO add your handling code here:
    Object[] options = {"Oui", "Non"};
        int n = JOptionPane.showOptionDialog(this, "Etes vous sur de vouloir désaffecter cette ressource matérielle?", "Désaffectation de ressource",
        JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[1]);
        
    if(n == 0) {
        Tache t = selection.getTache();
        String nomRes = (String) jListARessources.getSelectedValue();
        int i = jListARessources.getSelectedIndex();
        lmARessource.remove(i);
        ARessource r = ARessourceManager.chargerARessource(nomRes);

        int idARessource = r.getIdARessource();
        ARessourceManager.desaffecterArborescence(t, idARessource);
    }
}//GEN-LAST:event_jButtonEnleverARessourceActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonAjouterARessource;
    private javax.swing.JButton jButtonAjouterRessource;
    private javax.swing.JButton jButtonAjouterTache;
    private javax.swing.JButton jButtonAnnulerSaisie;
    private javax.swing.JButton jButtonDebut;
    private javax.swing.JButton jButtonEnleverARessource;
    private javax.swing.JButton jButtonEnleverRessource;
    private javax.swing.JButton jButtonFin;
    private javax.swing.JButton jButtonModifierTache;
    private javax.swing.JButton jButtonRetourProjets;
    private javax.swing.JButton jButtonSupprimerTache;
    private javax.swing.JButton jButtonValiderSaisie;
    private javax.swing.JLabel jLabelDebut;
    private javax.swing.JLabel jLabelFin;
    private javax.swing.JLabel jLabelNom;
    private javax.swing.JList jListARessources;
    private javax.swing.JList jListRessources;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanelSaisie;
    private javax.swing.JPanel jPanelTaches;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTextField jTextFieldDebut;
    private javax.swing.JTextField jTextFieldFin;
    private javax.swing.JTextField jTextFieldNom;
    private javax.swing.JTree jTree1;
    // End of variables declaration//GEN-END:variables

    /**
     * Obtenir le projet correspondant
     * @return
     * Instance du projet
     */
    public Projet getProjet() {
        return projet;
    }
    
    /**
     * Spécifier le projet à ouvrir
     * @param projet
     * Instance du projet
     */
    public void setProjet(Projet projet) {
        this.projet = projet;
    }

    public UserProjet getUserProjet() {
        return userProjet;
    }

    public void setUserProjet(UserProjet userProjet) {
        this.userProjet = userProjet;
    }
}
