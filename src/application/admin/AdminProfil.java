package application.admin;

import entitymanager.EntityManager;
import entitymanager.ProfilManager;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import mapping.Profil;
import utile.MyRecordSet;
/**
 *
 * @author  FireWolf
 */
public class AdminProfil extends javax.swing.JPanel {
    private MyRecordSet resultat;
    private Profil utilisateur;
    private String mode;
    private Profil selection;
    
    /** Creates new form AdminProfil */
    public AdminProfil() {
        initComponents();
        initialiserForm();
    }
    
    public AdminProfil(Profil p) {
        initComponents();
        initialiserForm();
        utilisateur = p;
    }

    private void initialiserForm() {
        resultat = new MyRecordSet();
        
        setEtatSaisie(false);
        setFiltrage(false);
        
        int nbRes = ProfilManager.chargerProfiles(jTable1, resultat);
        
        if(nbRes == 0)
            jLabel9.setText("Aucun résulat");
        else
            jLabel9.setText("Nombre de résultats : " + nbRes);
    }
    
    /**
     * Active ou désactive la zone de saisie
     * @param etat
     * si true alors la saisie est permise
     */
    private void setEtatSaisie(boolean etat) {
        jTextField1.setEditable(etat);
        jTextField2.setEditable(etat);
        jComboBox1.setEnabled(etat);
        jButton4.setEnabled(etat);
        jButton5.setEnabled(etat);
    }
    
    /**
     * Active ou désactive l'option de filtrage
     */
    private void setFiltrage(boolean etat) {
        jButton6.setEnabled(etat);
        jRadioButton1.setEnabled(etat);
        jRadioButton2.setEnabled(etat);
        jRadioButton3.setEnabled(etat);
        jTextField3.setEditable(etat);
        selectionnerRadioBouton(jRadioButton1);
    }
    
    /**
     * Afficher les propriétés du profil dans la zone de saisie
     * @param p
     * Instance d'un profil
     */
    private void afficherProprietesProfil(Profil p) {
        jTextField1.setText(p.getLogin());
        jTextField2.setText(p.getPassword());
        
        int sel = 0;
        if(p.getPrivilege().equals("STDUSER"))
            sel = 0;
        else if(p.getPrivilege().equals("SPECUSER"))
            sel = 1;
        else if(p.getPrivilege().equals("SYSADMIN"))
            sel = 2;
        
        jComboBox1.setSelectedIndex(sel);
    }
    
    /**
     * Obtenir les propriétés du profil dans le but d'une modification
     * @return
     * Instance d'un profil
     */
    private Profil getProprietesProfil() {
        Profil p = selection;
        p.setLogin(jTextField1.getText());
        p.setPassword(jTextField2.getText());
        p.setPrivilege((String) jComboBox1.getSelectedItem());
        return p;
    }
    
    private Profil getSaisie() {
        Profil res = new Profil();
        res.setLogin(jTextField1.getText());
        res.setPassword(jTextField2.getText());
        res.setPrivilege((String) jComboBox1.getSelectedItem());
        
        return res;
    }
    
    /**
     * Vider la zone de saisie pour effectuer un ajout de nouveau profil
     */
    private void initSaisie() {
        jTextField1.setText("");
        jTextField2.setText("");
        jComboBox1.setSelectedIndex(0);
    }
    
    /**
     * Mettre à jour l'affichage des profils de la base de données
     */
    public void updateResultat() {
        int nbRes = ProfilManager.chargerProfiles(jTable1, resultat);
        
        if(nbRes == 0)
            jLabel9.setText("Aucun résulat");
        else
            jLabel9.setText("Nombre de résultats : " + nbRes);
    }
    
    /**
     * Afficher les résultats filtrés
     * @param critère
     * Critère de sélection
     */
    private void filtrerResultat(String critere) {
        int nbRes = ProfilManager.chargerProfiles(jTable1, resultat, critere);
        
        if(nbRes == 0)
            jLabel9.setText("Aucun résulat");
        else
            jLabel9.setText("Nombre de résultats : " + nbRes);
    }
    
    /**
     * Activer tous les boutons de modifications (ajout, suppression, modification)
     */
    public void activerBoutonsModif() {
        jButton1.setEnabled(true);
        jButton2.setEnabled(true);
        jButton3.setEnabled(true);
    }
    
    /**
     * Activer tous les boutons de modifications sauf un seul
     * @param b
     * Le seul bouton qui ne doit pas être activé
     */
    public void activerBoutonsModifSauf(JButton b) {
        activerBoutonsModif();
        b.setEnabled(false);
    }
    
    private void selectionnerRadioBouton(JRadioButton radioBouton) {
        jRadioButton1.setSelected(false);
        jRadioButton2.setSelected(false);
        jRadioButton3.setSelected(false);
        
        radioBouton.setSelected(true);
    }
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jComboBox1 = new javax.swing.JComboBox();
        jLabel4 = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        jTextField2 = new javax.swing.JTextField();
        jButton4 = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jLabel9 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jComboBox2 = new javax.swing.JComboBox();
        jLabel7 = new javax.swing.JLabel();
        jRadioButton1 = new javax.swing.JRadioButton();
        jRadioButton2 = new javax.swing.JRadioButton();
        jRadioButton3 = new javax.swing.JRadioButton();
        jTextField3 = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        jButton6 = new javax.swing.JButton();

        setMinimumSize(new java.awt.Dimension(600, 550));
        setPreferredSize(new java.awt.Dimension(830, 660));
        addComponentListener(new java.awt.event.ComponentAdapter() {
            public void componentShown(java.awt.event.ComponentEvent evt) {
                formComponentShown(evt);
            }
        });
        setLayout(null);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(), "Propriétés du profil", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Tahoma", 1, 12))); // NOI18N
        jPanel1.setLayout(null);

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel2.setText("Login");
        jPanel1.add(jLabel2);
        jLabel2.setBounds(10, 40, 60, 30);

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 11));
        jLabel3.setText("Mot de passe");
        jPanel1.add(jLabel3);
        jLabel3.setBounds(10, 80, 80, 30);

        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "STDUSER", "SPECUSER", "SYSADMIN" }));
        jPanel1.add(jComboBox1);
        jComboBox1.setBounds(170, 120, 100, 20);

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 11));
        jLabel4.setText("Privilège");
        jPanel1.add(jLabel4);
        jLabel4.setBounds(10, 120, 70, 20);
        jPanel1.add(jTextField1);
        jTextField1.setBounds(150, 40, 120, 30);
        jPanel1.add(jTextField2);
        jTextField2.setBounds(150, 80, 120, 30);

        jButton4.setLabel("Valider");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton4);
        jButton4.setBounds(10, 180, 120, 23);

        jButton5.setLabel("Annuler");
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton5);
        jButton5.setBounds(150, 180, 120, 23);

        add(jPanel1);
        jPanel1.setBounds(10, 400, 280, 210);

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(), "Liste des profils", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Tahoma", 1, 12))); // NOI18N
        jPanel2.setLayout(null);

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jTable1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable1MouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1);

        jPanel2.add(jScrollPane1);
        jScrollPane1.setBounds(10, 20, 790, 330);

        jButton1.setText("Ajout");
        jButton1.setActionCommand("jButton1");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jPanel2.add(jButton1);
        jButton1.setBounds(10, 360, 110, 23);

        jButton2.setText("Suppression");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        jPanel2.add(jButton2);
        jButton2.setBounds(270, 360, 110, 23);

        jButton3.setText("Modification");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        jPanel2.add(jButton3);
        jButton3.setBounds(140, 360, 110, 23);

        jLabel9.setFont(new java.awt.Font("Tahoma", 1, 11));
        jLabel9.setForeground(new java.awt.Color(0, 0, 102));
        jLabel9.setText("Nombre de résultats");
        jPanel2.add(jLabel9);
        jLabel9.setBounds(600, 210, 200, 14);

        add(jPanel2);
        jPanel2.setBounds(10, 10, 810, 390);

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(), "Options de filtrage", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Tahoma", 1, 12))); // NOI18N
        jPanel3.setLayout(null);

        jComboBox2.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Tous les profils", "Par filtrage" }));
        jComboBox2.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jComboBox2ItemStateChanged(evt);
            }
        });
        jPanel3.add(jComboBox2);
        jComboBox2.setBounds(80, 40, 190, 20);

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 11));
        jLabel7.setText("Afficher");
        jPanel3.add(jLabel7);
        jLabel7.setBounds(10, 40, 70, 20);

        jRadioButton1.setText("Le login commence par");
        jRadioButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButton1ActionPerformed(evt);
            }
        });
        jPanel3.add(jRadioButton1);
        jRadioButton1.setBounds(10, 180, 180, 23);

        jRadioButton2.setText("Le login se termine par");
        jRadioButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButton2ActionPerformed(evt);
            }
        });
        jPanel3.add(jRadioButton2);
        jRadioButton2.setBounds(350, 180, 150, 23);

        jRadioButton3.setText("Le login contient");
        jRadioButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButton3ActionPerformed(evt);
            }
        });
        jPanel3.add(jRadioButton3);
        jRadioButton3.setBounds(190, 180, 160, 23);
        jPanel3.add(jTextField3);
        jTextField3.setBounds(80, 80, 190, 30);

        jLabel8.setFont(new java.awt.Font("Tahoma", 1, 11));
        jLabel8.setText("Critère");
        jPanel3.add(jLabel8);
        jLabel8.setBounds(10, 80, 60, 30);

        jButton6.setText("Filtrer");
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });
        jPanel3.add(jButton6);
        jButton6.setBounds(10, 120, 140, 23);

        add(jPanel3);
        jPanel3.setBounds(300, 400, 520, 210);
    }// </editor-fold>//GEN-END:initComponents

private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
// TODO add your handling code here:
    mode = EntityManager.AJOUT;
    setEtatSaisie(true);
    activerBoutonsModifSauf(jButton1);
    jButton4.setText("Ajouter");
    initSaisie();
}//GEN-LAST:event_jButton1ActionPerformed

private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
// TODO add your handling code here:
    mode = EntityManager.SUPPRESSION;
    if(selection != null) {
        Object[] options = {"Oui", "Non"};
        int n = JOptionPane.showOptionDialog(this, "Etes vous sur de supprimer ce profil ?", "Supprimer",
        JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[1]);

        if(n == 0) {
            ProfilManager.supprimerProfil(selection);
            updateResultat();
        }
    }
}//GEN-LAST:event_jButton2ActionPerformed

private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
// TODO add your handling code here:
    mode = EntityManager.MODIFICATION;
    setEtatSaisie(true);
    activerBoutonsModifSauf(jButton3);
    jButton4.setText("Modifier");
}//GEN-LAST:event_jButton3ActionPerformed

private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
// TODO add your handling code here:
    mode = EntityManager.AUCUNE;
    setEtatSaisie(false);
    if(selection != null)
        afficherProprietesProfil(selection);
    
    activerBoutonsModif();
    jButton4.setText("Valider");
}//GEN-LAST:event_jButton5ActionPerformed

private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
// TODO add your handling code here:
    setEtatSaisie(false);
    
    Profil p;
    
    if(mode.equals(EntityManager.AJOUT)) {
        p = getSaisie();
        ProfilManager.creerProfil(p);
    }
    else if(mode.equals(EntityManager.MODIFICATION)) {
        p = getProprietesProfil();
        ProfilManager.modifierProfil(p);
    }
    
    updateResultat();
    activerBoutonsModif();
    jButton4.setText("Valider");
}//GEN-LAST:event_jButton4ActionPerformed

private void jTable1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable1MouseClicked
// TODO add your handling code here:
    int i = jTable1.getSelectedRow();
    Profil p = (Profil) resultat.getValueAt(i);
    selection = p;
    afficherProprietesProfil(p);
}//GEN-LAST:event_jTable1MouseClicked

private void formComponentShown(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_formComponentShown
// TODO add your handling code here:
    updateResultat();
}//GEN-LAST:event_formComponentShown

private void jRadioButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButton3ActionPerformed
// TODO add your handling code here:
    selectionnerRadioBouton(jRadioButton3);
}//GEN-LAST:event_jRadioButton3ActionPerformed

private void jComboBox2ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jComboBox2ItemStateChanged
// TODO add your handling code here:
    int i = jComboBox2.getSelectedIndex();
    if(i == 0) {
        setFiltrage(false);
        updateResultat();
        jTextField3.setText("");
    }
    else if(i == 1)
        setFiltrage(true);
}//GEN-LAST:event_jComboBox2ItemStateChanged

private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6ActionPerformed
// TODO add your handling code here:
    String critere = jTextField3.getText();
    
    if(jRadioButton1.isSelected()) {
        filtrerResultat(critere + "%");
    }else if(jRadioButton2.isSelected()) {
        filtrerResultat("%" + critere);
    }else if(jRadioButton3.isSelected()) {
        filtrerResultat("%" + critere + "%");
    }
}//GEN-LAST:event_jButton6ActionPerformed

private void jRadioButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButton1ActionPerformed
// TODO add your handling code here:
    selectionnerRadioBouton(jRadioButton1);
}//GEN-LAST:event_jRadioButton1ActionPerformed

private void jRadioButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButton2ActionPerformed
// TODO add your handling code here:
    selectionnerRadioBouton(jRadioButton2);
}//GEN-LAST:event_jRadioButton2ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JButton jButton6;
    private javax.swing.JComboBox jComboBox1;
    private javax.swing.JComboBox jComboBox2;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JRadioButton jRadioButton1;
    private javax.swing.JRadioButton jRadioButton2;
    private javax.swing.JRadioButton jRadioButton3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTextField jTextField2;
    private javax.swing.JTextField jTextField3;
    // End of variables declaration//GEN-END:variables

}
