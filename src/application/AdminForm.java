package application;

import application.SessionForm;
import application.admin.*;
import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import javax.swing.JOptionPane;
import lwcomponents.canvas.LWCanvas;
import lwcomponents.canvas.LWCustomCanvas;
import lwcomponents.canvas.LWShape;
import lwcomponents.util.LWButton2;
import lwcomponents.util.LWButtonSkin;
import lwcomponents.util.LWImgUtil;
import lwcomponents.util.LWMouseListener;
import lwcomponents.util.MakeNimbusLookNFeel;
import mapping.Profil;
import utile.PaneManager;

/**
 *
 * @author  FireWolf
 */
public class AdminForm extends javax.swing.JFrame implements Runnable {
    private BufferedImage bcgImg;
    private BufferedImage titleImg;
    private BufferedImage showCase;
    private BufferedImage[] tabImg;
    private LWCanvas showCaseCanvas;
    private LWCanvas menuCanvas;
    private LWCustomCanvas titleCanvas;
    private LWMouseListener mouseListener;
    private LWButton2 b1; //Profils
    private LWButton2 b2; //Ressources
    private LWButton2 b3; //Autres ressources
    private LWButton2 b4; //Postes
    private LWButton2 b5; //Jours fériés
    private LWButton2 b6;
    private LWButton2 b7; //Quitter
    
    private Profil utilisateur; //Le profil de l'utilisateur
    private AdminMenu adminMenu; //L'interface du menu d'accueil
    private AdminRessource adminRessource; //L'interface de gestion de ressources
    private AdminARessource adminARessource; //L'interface de gestion de ressources matérielles
    private AdminProfil adminProfil; //L'interface de gestion de profils
    private AdminPoste adminPoste; //L'interface de gestion de postes
    private AdminJourFerie adminJourFerie; //Interface de gestion de jours feriés
    private AdminAbout adminAbout;
    private SessionForm sessionForm; //L'interface de session correspondante
    private Thread thread;
    
    
    /** Creates new form MenuForm */
    public AdminForm() {
        initComponents();
        initialiserForm();
    }
    
    /**Initialise la forme avec le profil**/
    public AdminForm(Profil p) {
        initComponents();
        
        initialiserForm();
        
        this.utilisateur = p;
        jLabel1.setText("Compte d'administration, nom de votre login : " + p.getLogin());
    }

    private void initialiserForm() {    
        bcgImg = LWImgUtil.loadImage("img\\DoricOrder.jpg");
        titleImg = LWImgUtil.loadImage("img\\titleBckg.jpg");
        tabImg = new BufferedImage[5];
        tabImg[0] = LWImgUtil.loadImage("img\\components\\showCase\\Admin\\showCase1.bmp");
        tabImg[1] = LWImgUtil.loadImage("img\\components\\showCase\\Admin\\showCase2.bmp");
        tabImg[2] = LWImgUtil.loadImage("img\\components\\showCase\\Admin\\showCase3.bmp");
        tabImg[3] = LWImgUtil.loadImage("img\\components\\showCase\\Admin\\showCase4.bmp");
        tabImg[4] = LWImgUtil.loadImage("img\\components\\showCase\\Admin\\showCase5.bmp");
        
        titleCanvas = new LWCustomCanvas() {
            @Override
            public void paint(Graphics g) {
                Graphics2D g2 = (Graphics2D) g;
                
                Rectangle b = jPanel4.getBounds();
                g2.drawImage(titleImg, 0, 0, b.width, b.height, this);
            }
        };
        jPanel4.add(titleCanvas);
        titleCanvas.setBounds(0, 0, jPanel4.getBounds().width, jPanel4.getBounds().height);
        
        menuCanvas = new LWCanvas() {
            Rectangle b = jPanel1.getBounds();
            
            @Override
            public void paintBackground(Graphics g) {
                Graphics2D g2 = (Graphics2D) g;
                
                g2.drawImage(bcgImg, 0, 0, b.width, b.height, null);
                
                g2.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.6F));
            }
        };
        
        mouseListener = new LWMouseListener(menuCanvas);
        
        b1 = new LWButton2(10, 10, "Profils");
        b1.setSize(170, 30);
        
        b2 = new LWButton2(10, 50, "Ressources");
        b2.setSize(170, 30);
        
        b3 = new LWButton2(10, 90, "Autres Ressources");
        b3.setSize(170, 30);
        
        b4 = new LWButton2(10, 130, "Postes");
        b4.setSize(170, 30);
        
        b5 = new LWButton2(10, 170, "Jours feriés");
        b5.setSize(170, 30);
        
        b6 = new LWButton2(10, 210, "Accueil");
        b6.setSize(170, 30);
        
        b7 = new LWButton2(10, 670, "Quitter");
        b7.setSize(170, 30);
        
        b1.setMouseListener(mouseListener);
        b2.setMouseListener(mouseListener);
        b3.setMouseListener(mouseListener);
        b4.setMouseListener(mouseListener);
        b5.setMouseListener(mouseListener);
        b6.setMouseListener(mouseListener);
        b7.setMouseListener(mouseListener);
        LWButtonSkin btnSkin = new LWButtonSkin("img\\components\\btnModel2", "btn0", "btn1", "btn2");
        b1.setSkin(btnSkin);
        b2.setSkin(btnSkin);
        b3.setSkin(btnSkin);
        b4.setSkin(btnSkin);
        b5.setSkin(btnSkin);
        b6.setSkin(btnSkin);
        b7.setSkin(btnSkin);
        
        menuCanvas.addShape(b1);
        menuCanvas.addShape(b2);
        menuCanvas.addShape(b3);
        menuCanvas.addShape(b4);
        menuCanvas.addShape(b5);
        menuCanvas.addShape(b6);
        menuCanvas.addShape(b7);
        
        menuCanvas.setBackground(new Color(0,153,255));
        jPanel1.add(menuCanvas.getCanvas(jPanel1));
        
        adminMenu = new AdminMenu();
        adminMenu.setBounds(PaneManager.obtenirDecalageBounds(jPanel2, 3));
        adminMenu.setVisible(true);
        jPanel2.add(adminMenu);
        
        adminRessource = new AdminRessource();
        adminRessource.setBounds(PaneManager.obtenirDecalageBounds(jPanel2, 3));
        adminRessource.setVisible(false);
        jPanel2.add(adminRessource);
        
        adminARessource = new AdminARessource();
        adminARessource.setBounds(PaneManager.obtenirDecalageBounds(jPanel2, 3));
        adminARessource.setVisible(false);
        jPanel2.add(adminARessource);
        
        adminProfil = new AdminProfil();
        adminProfil.setBounds(PaneManager.obtenirDecalageBounds(jPanel2, 3));
        adminProfil.setVisible(false);
        jPanel2.add(adminProfil);
        
        adminPoste = new AdminPoste();
        adminPoste.setBounds(PaneManager.obtenirDecalageBounds(jPanel2, 3));
        adminPoste.setVisible(false);
        jPanel2.add(adminPoste);
        
        adminJourFerie = new AdminJourFerie();
        adminJourFerie.setBounds(PaneManager.obtenirDecalageBounds(jPanel2, 3));
        adminJourFerie.setVisible(false);
        jPanel2.add(adminJourFerie);
        
        adminAbout = new AdminAbout();
        adminAbout.setBounds(PaneManager.obtenirDecalageBounds(jPanel2, 3));
        adminAbout.setVisible(false);
        jPanel2.add(adminAbout);
        
        //Since 07/01/08
        showCaseCanvas = new LWCanvas() {
            Rectangle b = jPanel2.getBounds();
            @Override
            public void paintBackground(Graphics g) {
                Graphics2D g2 = (Graphics2D) g;
                
                if(showCase != null)
                    g2.drawImage(showCase, 0, 0, b.width, b.height, null);
            }
        };
        showCaseCanvas.setBounds(0, 0, jPanel2.getBounds().width, jPanel2.getBounds().height);
        jPanel2.add(showCaseCanvas);
        
        thread = new Thread(this);
        thread.start();
    }
    
    /**
     * Demande de confirmation si vous voulez quitter l'application
     */
    public void quitterApplication() {
        Object[] options = {"Oui", "Non"};
        int n = JOptionPane.showOptionDialog(this, "Etes vous sur de vouloir quitter ?", "Quitter l'application",
                JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[1]);
        
        if(n == 0) System.exit(0);
    }
    
    /**
     * Demande de confirmation de changement de compte
     */
    public void quitterSession() {
        Object[] options = {"Oui", "Non"};
        int n = JOptionPane.showOptionDialog(this, "Etes vous sur de vouloir fermer cette session ?", "Fermer la session",
                JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[1]);
      
        if(n == 0) {            
            getSessionForm().setVisible(true);            
            this.dispose();
        }
    }
    
    private void hideAllCanvas() {
        adminMenu.setVisible(false);
        adminProfil.setVisible(false);
        adminRessource.setVisible(false);
        adminARessource.setVisible(false);
        adminPoste.setVisible(false);
        adminJourFerie.setVisible(false);
        adminAbout.setVisible(false);
    }
    
    private boolean menuVisible() {
        if(adminMenu.isVisible() || adminProfil.isVisible() || adminRessource.isVisible()
                || adminARessource.isVisible() || adminPoste.isVisible() || adminJourFerie.isVisible()
                || adminAbout.isVisible())
            return false;
        else
            return true;
    }
    
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        jMenuBarAdmin = new javax.swing.JMenuBar();
        jMenuFichier = new javax.swing.JMenu();
        jMenuItemSession = new javax.swing.JMenuItem();
        jMenuItemQuitter = new javax.swing.JMenuItem();
        jMenuEdition = new javax.swing.JMenu();
        jMenuAide = new javax.swing.JMenu();
        jMenuApropos = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Kronos Gantt Project - Compte administrateur");
        setMinimumSize(new java.awt.Dimension(1024, 768));
        setResizable(false);
        getContentPane().setLayout(null);

        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel2.setLayout(null);
        getContentPane().add(jPanel2);
        jPanel2.setBounds(190, 60, 830, 620);

        jPanel3.setBackground(new java.awt.Color(102, 102, 255));
        jPanel3.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel3.setLayout(null);

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 12));
        jLabel1.setForeground(new java.awt.Color(0, 0, 102));
        jLabel1.setText("Compte d'administration, nom de votre login : ");
        jPanel3.add(jLabel1);
        jLabel1.setBounds(10, 10, 460, 15);

        getContentPane().add(jPanel3);
        jPanel3.setBounds(190, 680, 830, 30);

        jPanel1.setBackground(new java.awt.Color(204, 204, 255));
        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel1.setLayout(null);
        getContentPane().add(jPanel1);
        jPanel1.setBounds(0, 0, 190, 710);

        jPanel4.setBackground(new java.awt.Color(153, 153, 255));
        jPanel4.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel4.setLayout(null);

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 18));
        jLabel2.setForeground(new java.awt.Color(0, 0, 102));
        jLabel2.setText("Bienvenue");
        jPanel4.add(jLabel2);
        jLabel2.setBounds(10, 10, 660, 22);

        getContentPane().add(jPanel4);
        jPanel4.setBounds(190, 0, 830, 50);

        jPanel5.setBackground(new java.awt.Color(51, 51, 255));
        jPanel5.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        getContentPane().add(jPanel5);
        jPanel5.setBounds(190, 50, 830, 10);

        jMenuFichier.setText("Fichier");

        jMenuItemSession.setText("Fermer la session");
        jMenuItemSession.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemSessionActionPerformed(evt);
            }
        });
        jMenuFichier.add(jMenuItemSession);

        jMenuItemQuitter.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_Q, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItemQuitter.setText("Quitter");
        jMenuItemQuitter.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemQuitterActionPerformed(evt);
            }
        });
        jMenuFichier.add(jMenuItemQuitter);

        jMenuBarAdmin.add(jMenuFichier);

        jMenuEdition.setText("Edition");
        jMenuBarAdmin.add(jMenuEdition);

        jMenuAide.setText("Aide");
        jMenuAide.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuAideActionPerformed(evt);
            }
        });

        jMenuApropos.setText("A Propos");
        jMenuApropos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuAproposActionPerformed(evt);
            }
        });
        jMenuAide.add(jMenuApropos);

        jMenuBarAdmin.add(jMenuAide);

        setJMenuBar(jMenuBarAdmin);

        pack();
    }// </editor-fold>//GEN-END:initComponents

private void jMenuItemQuitterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemQuitterActionPerformed
// TODO add your handling code here:
    quitterApplication();
}//GEN-LAST:event_jMenuItemQuitterActionPerformed

private void jMenuItemSessionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemSessionActionPerformed
// TODO add your handling code here:
    quitterSession();
}//GEN-LAST:event_jMenuItemSessionActionPerformed

private void jMenuAproposActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuAproposActionPerformed
// TODO add your handling code here:
//    final BufferedImage img = LWImgUtil.loadImage("img\\About\\About.bmp");
//    LWDialog dlg = new LWDialog(this, "A propos de Kronos Gantt Project") {
//        @Override
//        public void drawBackground(Graphics g) {
//            Graphics2D g2 = (Graphics2D) g;
//            
//            g2.drawImage(img, 0, 0, 640, 480, null);
//        }
//    };
//    dlg.setVisible(true);
    adminMenu.setVisible(false);
    adminProfil.setVisible(false);
    adminRessource.setVisible(false);
    adminARessource.setVisible(false);
    adminPoste.setVisible(false);
    adminJourFerie.setVisible(false);
    adminAbout.setVisible(true);
    jLabel2.setText("A propos de Kronos Gantt Project");
}//GEN-LAST:event_jMenuAproposActionPerformed

private void jMenuAideActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuAideActionPerformed
// TODO add your handling code here:
}//GEN-LAST:event_jMenuAideActionPerformed

    /**
    * @param args the command line arguments
    */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                MakeNimbusLookNFeel.makeIt();
                new AdminForm().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JMenu jMenuAide;
    private javax.swing.JMenuItem jMenuApropos;
    private javax.swing.JMenuBar jMenuBarAdmin;
    private javax.swing.JMenu jMenuEdition;
    private javax.swing.JMenu jMenuFichier;
    private javax.swing.JMenuItem jMenuItemQuitter;
    private javax.swing.JMenuItem jMenuItemSession;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    // End of variables declaration//GEN-END:variables

    public void run() {
        try{
            while(true) {
                thread.sleep(10);
                if(b1.getFocusState()) {
                    
                    if(adminMenu.isVisible() || menuVisible()) {
                        hideAllCanvas();
                        showCase = tabImg[0];
                        jPanel2.repaint();
                    }
                    
                        LWShape sel = mouseListener.getSelection();
                        if(sel != null)
                            if(sel.equals(b1)) {
                                adminMenu.setVisible(false);
                                adminProfil.setVisible(true);
                                adminRessource.setVisible(false);
                                adminARessource.setVisible(false);
                                adminPoste.setVisible(false);
                                adminJourFerie.setVisible(false);
                                adminAbout.setVisible(false);
                                jLabel2.setText("Gestion de profils : vous pouvez ajouter, " +
                                        "modifier ou supprimer les profils");

                                mouseListener.clearSelection();
                            }
                    }
                    
                if(b2.getFocusState()) {

                    if(adminMenu.isVisible() || menuVisible()) {
                        hideAllCanvas();
                        showCase = tabImg[1];
                        jPanel2.repaint();
                    }
                    
                        LWShape sel = mouseListener.getSelection();
                        if(sel != null)
                            if(sel.equals(b2)) {
                                adminMenu.setVisible(false);
                                adminProfil.setVisible(false);
                                adminRessource.setVisible(true);
                                adminARessource.setVisible(false);
                                adminPoste.setVisible(false);
                                adminJourFerie.setVisible(false);
                                adminAbout.setVisible(false);
                                jLabel2.setText("Gestion de ressources : vous pouvez ajouter, " +
                                        "modifier ou supprimer les ressources humaines");

                                mouseListener.clearSelection();
                            }
                }  

                if(b3.getFocusState()) {
                    
                    if(adminMenu.isVisible() || menuVisible()) {
                        hideAllCanvas();
                        showCase = tabImg[2];
                        jPanel2.repaint();
                    }
                    
                        LWShape sel = mouseListener.getSelection();
                        if(sel != null)
                            if(sel.equals(b3)) {
                                adminMenu.setVisible(false);
                                adminProfil.setVisible(false);
                                adminRessource.setVisible(false);
                                adminARessource.setVisible(true);
                                adminPoste.setVisible(false);
                                adminJourFerie.setVisible(false);
                                adminAbout.setVisible(false);
                                jLabel2.setText("Gestion des autres ressources : vous pouvez ajouter, " +
                                        "modifier ou supprimer les ressources matérielles");

                                mouseListener.clearSelection();
                            }
                }

                if(b4.getFocusState()) {
                    
                    if(adminMenu.isVisible() || menuVisible()) {
                        hideAllCanvas();
                        showCase = tabImg[3];
                        jPanel2.repaint();
                    }
                    
                        LWShape sel = mouseListener.getSelection();
                        if(sel != null)
                            if(sel.equals(b4)) {
                                adminMenu.setVisible(false);
                                adminProfil.setVisible(false);
                                adminRessource.setVisible(false);
                                adminARessource.setVisible(false);
                                adminPoste.setVisible(true);
                                adminJourFerie.setVisible(false);
                                adminAbout.setVisible(false);
                                jLabel2.setText("Gestion de postes : vous pouvez ajouter, " +
                                        "modifier ou supprimer des postes");

                                mouseListener.clearSelection();
                            }
                }

                if(b5.getFocusState()) {
                    
                    if(adminMenu.isVisible() || menuVisible()) {
                        hideAllCanvas();
                        showCase = tabImg[4];
                        jPanel2.repaint();
                    }
                    
                        LWShape sel = mouseListener.getSelection();
                        if(sel != null)
                            if(sel.equals(b5)) {
                                adminMenu.setVisible(false);
                                adminProfil.setVisible(false);
                                adminRessource.setVisible(false);
                                adminARessource.setVisible(false);
                                adminPoste.setVisible(false);
                                adminJourFerie.setVisible(true);
                                adminAbout.setVisible(false);
                                jLabel2.setText("Gestion de jours fériés : vous pouvez ajouter, " +
                                        "modifier ou supprimer Des jours fériés");

                                mouseListener.clearSelection();
                            }
                }

                if(b6.getFocusState()) {
                    
                    if(adminMenu.isVisible() || menuVisible()) {
                        hideAllCanvas();
                        showCase = tabImg[4];
                        jPanel2.repaint();
                    }
                    
                        LWShape sel = mouseListener.getSelection();
                        if(sel != null)
                            if(sel.equals(b6)) {
                                adminMenu.setVisible(true);
                                adminProfil.setVisible(false);
                                adminRessource.setVisible(false);
                                adminARessource.setVisible(false);
                                adminPoste.setVisible(false);
                                adminJourFerie.setVisible(false);
                                adminAbout.setVisible(false);
                                jLabel2.setText("Bienvenue");

                                mouseListener.clearSelection();
                            }
                }
                
                if(b7.getFocusState()) {

                        LWShape sel = mouseListener.getSelection();
                        if(sel != null)
                            if(sel.equals(b7)) {

                                quitterApplication();

                                mouseListener.clearSelection();
                            }
                } 
            }
        }catch(InterruptedException e) {
            return;
        }
    }

    /**
     * Obtenir l'instance de la fenêtre de session correspondante
     * @param sessionForm
     */
    public void setSessionForm(SessionForm sessionForm) {
        this.sessionForm = sessionForm;
    }

    public SessionForm getSessionForm() {
        return sessionForm;
    }
}
