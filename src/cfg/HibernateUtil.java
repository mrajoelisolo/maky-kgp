package cfg;

import org.hibernate.*;
import org.hibernate.cfg.*;

public class HibernateUtil {
    private static final SessionFactory sessionFactory;
    private static String CONFIG_FILE_LOCATION = "cfg/hibernate.cfg.xml";
    
    static {
        try {
            sessionFactory = new Configuration().configure(CONFIG_FILE_LOCATION).buildSessionFactory();
        } catch (Throwable ex) {
            
            System.err.println("Initial SessionFactory creation failed." + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }
    
      public static SessionFactory getSessionFactory() {
           return sessionFactory;
    }
}

