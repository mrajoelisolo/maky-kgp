package etat;

import entitymanager.EntityManager;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import lwcomponents.canvas.LWShape;

/**
 *
 * @author FireWolf
 */
public class LWLibelle extends LWShape {
    private Rectangle bounds = new Rectangle();
    private String libelle;
    
    public LWLibelle(String libelle) {
        this.libelle = libelle;
    }
    
    public String getLibelle() {
        return libelle;
    }
    
    @Override
    public void draw(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        Font myFont = new Font(EntityManager.FONT_LIBELLE, Font.BOLD, EntityManager.TAILLE_FONT_LIBELLE);
        g2.setColor(new Color(0, 0, 0));
        g2.setFont(myFont);
        
        int sp = EntityManager.TAILLE_FONT_LIBELLE + 5;
        
        g2.drawString(libelle, bounds.x, bounds.y + sp);
    }

    @Override
    public Rectangle getBounds() {
        return bounds;
    }
    
    public void setBounds(int x, int y, int wdt, int hgt) {
        bounds.setBounds(x, y, wdt, hgt);
    }
}
