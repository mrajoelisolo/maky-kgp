/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package etat;

import entitymanager.EntityManager;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import lwcomponents.canvas.LWShape;

/**
 *
 * @author FireWolf
 */
public class LWLabel extends LWShape {
    private Rectangle bounds = new Rectangle();
    private String text;
    private int fontSize = 16;
    
    public LWLabel(String text) {
        this.text = text;
    }
    
    @Override
    public void draw(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        
        Font myFont = new Font(EntityManager.FONT_TITRE, Font.BOLD, fontSize);
        g2.setColor(new Color(0, 0, 0));
        g2.setFont(myFont);
        
        int sp = fontSize + 5;
        
        g2.drawString(text, bounds.x, bounds.y + sp);
    }

    @Override
    public Rectangle getBounds() {
        return bounds;
    }

    public void setBounds(int x, int y, int wdt, int hgt) {
        bounds.setBounds(x, y, wdt, hgt);
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
