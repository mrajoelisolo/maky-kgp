package etat;

import entitymanager.EntityManager;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import lwcomponents.canvas.LWShape;
import lwcomponents.util.LWImgUtil;

/**
 *
 * @author FireWolf
 */
public class LWTitle extends LWShape {
    private Rectangle bounds = new Rectangle();
    private String[] titles;
    public static int COL_SPACING = 200;
    private BufferedImage img = LWImgUtil.loadImage("img\\components\\LWReport\\HorizontalBar.bmp");
    
    public LWTitle(String ... titles) {
        this.titles = new String[titles.length];
        
        int i = 0;
        for(String o : titles) {
            this.titles[i] = new String("" + o);
            i++;
        } 
    }
    
    @Override
    public void draw(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        
        g2.drawImage(img, bounds.x, bounds.y, bounds.width, bounds.height, null);
        
        Font myFont = new Font(EntityManager.FONT_TITRE, Font.BOLD, EntityManager.TAILLE_FONT_TITRE);
        g2.setColor(new Color(0, 0, 0));
        g2.setFont(myFont);
        
        int sp = EntityManager.TAILLE_FONT_TITRE + 5;
        
        int xPos = bounds.x;
        for(int i = 0; i < titles.length; i++) {
            g2.drawString(titles[i], xPos + 15, bounds.y + sp);
            xPos += COL_SPACING;
        }
    }

    @Override
    public Rectangle getBounds() {
        return bounds;
    }

    public void setBounds(int x, int y, int wdt, int hgt) {
        bounds.setBounds(x, y, wdt, hgt);
    }
    
    public String[] getTitles() {
        return titles;
    }
    
    public void setTitles(String ... titles) {
        this.titles = titles;
    }
}
