package etat;

import entitymanager.EntityManager;
import java.util.Iterator;
import lwcomponents.canvas.LWCanvas;
import mapping.EtatARessource;
import mapping.EtatRessource;
import mapping.EtatTache;
import mapping.Projet;
import utile.ConversionDate;

/**
 *
 * @author FireWolf
 * Cette classe contient les fonctionnalités pour génerer les etats. Il existe
 * 3 types d'etats, a savoir, les tâches, les ressources de travail et enfin les
 * ressources matérielles
 */
public abstract class EtatManager {
    /**
     * Génère un état formulaire dans le canevas
     * @param etat
     * Instance de la classe EtatTaches
     * @param canvas
     * Instance de la classe LWCanvas
     */
    public static void initEtatTaches(Projet projet, EtatTaches etat, LWCanvas canvas) {
        int yPos = EtatTaches.Y_POS;
        canvas.clearShapes();
        
        LWLibelle libelle = new LWLibelle("Etat du budget des taches");
        libelle.setBounds(10, yPos - EtatTaches.Y_POS, 100, 50);
        canvas.addShape(libelle);
        
        LWLabel titreProjet = new LWLabel("Projet : " + projet.getNomProjet());
        titreProjet.setBounds(15, 20 + yPos - EtatTaches.Y_POS + 20, 100, 50);
        canvas.addShape(titreProjet);
        
        LWLabel debutProjet = new LWLabel("Debut : " + ConversionDate.dateToString(projet.getDebutProjet()));
        debutProjet.setBounds(15, 40 + yPos - EtatTaches.Y_POS + 20, 100, 50);
        canvas.addShape(debutProjet);
        
        LWLabel finProjet = new LWLabel("Fin : " + ConversionDate.dateToString(projet.getFinProjet()));
        finProjet.setBounds(15, 60 + yPos - EtatTaches.Y_POS + 20, 100, 50);
        canvas.addShape(finProjet);
        
        LWTitle titre = new LWTitle("Tâche", "Debut", "Fin", "Coût(Ar)");
        titre.setBounds(10, yPos - 30, canvas.getBounds().width - 20, 30);
        canvas.addShape(titre);
        for(Iterator iter = etat.getList().iterator(); iter.hasNext();) {
            EtatTache etatTache = (EtatTache) iter.next();
            
            etatTache.setBounds(20, yPos, 100, 20);
            canvas.addShape(etatTache);
            
            yPos += EtatTache.ROW_SPACING;
        }
        LWEtatTotalTaches total = new LWEtatTotalTaches(etat);
        total.setBounds(20, yPos + 15, canvas.getBounds().width - 20, 20);
        canvas.addShape(total);
    }
    
    /**
     * Génère un état formulaire dans le canevas
     * @param etat
     * Instance de la classe EtatTaches
     * @param canvas
     * Instance de la classe LWCanvas
     */
    public static void initEtatRessources(Projet projet, EtatRessources etat , LWCanvas canvas) {
        int yPos = EtatRessources.Y_POS;
        canvas.clearShapes();
        
        LWLibelle libelle = new LWLibelle("Etat du budget des ressources de travail");
        libelle.setBounds(10, yPos - EtatRessources.Y_POS, 100, 50);
        canvas.addShape(libelle);
        
        LWLabel titreProjet = new LWLabel("Projet : " + projet.getNomProjet());
        titreProjet.setBounds(15, 20 + yPos - EtatTaches.Y_POS + 20, 100, 50);
        canvas.addShape(titreProjet);
        
        LWLabel debutProjet = new LWLabel("Debut : " + ConversionDate.dateToString(projet.getDebutProjet()));
        debutProjet.setBounds(15, 40 + yPos - EtatTaches.Y_POS + 20, 100, 50);
        canvas.addShape(debutProjet);
        
        LWLabel finProjet = new LWLabel("Fin : " + ConversionDate.dateToString(projet.getFinProjet()));
        finProjet.setBounds(15, 60 + yPos - EtatTaches.Y_POS + 20, 100, 50);
        canvas.addShape(finProjet);
        
        LWTitle titre = new LWTitle("Ressource", "Durée(j/h)", "Coût(" + EntityManager.UT_MONETAIRE + ")");
        titre.setBounds(10, yPos - 30, canvas.getBounds().width - 20, 30);
        canvas.addShape(titre);
        for(Iterator iter = etat.getList().iterator(); iter.hasNext();) {
            EtatRessource etatRessource = (EtatRessource) iter.next();
            
            etatRessource.setBounds(20, yPos, 100, 20);
            canvas.addShape(etatRessource);
            
            yPos += EtatRessource.ROW_SPACING;
        }
        LWEtatTotalRessources total = new LWEtatTotalRessources(etat);
        total.setBounds(20, yPos + 15, canvas.getBounds().width - 20, 20);
        canvas.addShape(total);
    }
    
    public static void initEtatARessources(Projet projet, EtatARessources etat, LWCanvas canvas) {
        int yPos = EtatARessources.Y_POS;
        canvas.clearShapes();
        
        LWLibelle libelle = new LWLibelle("Etat du budget des autres ressources");
        libelle.setBounds(10, yPos - EtatARessources.Y_POS, 100, 50);
        canvas.addShape(libelle);
        
        LWLabel titreProjet = new LWLabel("Projet : " + projet.getNomProjet());
        titreProjet.setBounds(15, 20 + yPos - EtatTaches.Y_POS + 20, 100, 50);
        canvas.addShape(titreProjet);
        
        LWLabel debutProjet = new LWLabel("Debut : " + ConversionDate.dateToString(projet.getDebutProjet()));
        debutProjet.setBounds(15, 40 + yPos - EtatTaches.Y_POS + 20, 100, 50);
        canvas.addShape(debutProjet);
        
        LWLabel finProjet = new LWLabel("Fin : " + ConversionDate.dateToString(projet.getFinProjet()));
        finProjet.setBounds(15, 60 + yPos - EtatTaches.Y_POS + 20, 100, 50);
        canvas.addShape(finProjet);
        
        LWTitle titre = new LWTitle("Ressource", "Durée(j/h)", "Coût(" + EntityManager.UT_MONETAIRE + ")");
        titre.setBounds(10, yPos - 30, canvas.getBounds().width - 20, 30);
        canvas.addShape(titre);
        
        for(Iterator iter = etat.getList().iterator(); iter.hasNext();) {
            EtatARessource etatARessource = (EtatARessource) iter.next();
            
            etatARessource.setBounds(20, yPos, 100, 20);
            canvas.addShape(etatARessource);
            
            yPos += EtatARessource.ROW_SPACING;
        }
        LWEtatTotalARessources total = new LWEtatTotalARessources(etat);
        total.setBounds(20, yPos + 15, canvas.getBounds().width - 20, 20);
        canvas.addShape(total);
    }
}
