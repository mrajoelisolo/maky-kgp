package etat;

import entitymanager.EntityManager;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import lwcomponents.canvas.LWShape;
import mapping.EtatTache;
import lwcomponents.util.LWImgUtil;

/**
 *
 * @author FireWolf
 */
public class LWEtatTotalTaches extends LWShape {
    private int colPos = 3;
    private int barSpacing = 5;
    private Rectangle bounds = new Rectangle();
    private EtatTaches etatTaches;
    private BufferedImage img = LWImgUtil.loadImage("img\\components\\LWReport\\HorBar2.bmp");
    private float cout;
    
    public LWEtatTotalTaches(EtatTaches etatTaches) {
        this.etatTaches = etatTaches;
        cout = etatTaches.getCout();
    }
    
    @Override
    public void draw(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        
        Font myFont = new Font(EntityManager.FONT_TITRE, Font.BOLD, EntityManager.TAILLE_FONT_TITRE);
        g2.setColor(new Color(0, 0, 0));
        g2.setFont(myFont);
        g2.setColor(Color.BLUE);
        int sp = EntityManager.TAILLE_FONT_TITRE + 5;
        
        g2.drawImage(img, bounds.x - 10, bounds.y - barSpacing, bounds.width, 5, null);
        g2.drawString("Coût total", bounds.x, bounds.y + sp);
        g2.setColor(new Color(0, 0, 0));
        g2.drawString("" + cout + " " + EntityManager.UT_MONETAIRE, bounds.x + colPos * EtatTache.COL_SPACING, bounds.y + 20);
    }

    @Override
    public Rectangle getBounds() {
        return bounds;
    }
    
    public void setBounds(int x, int y, int wdt, int hgt) {
        bounds.setBounds(x, y, wdt, hgt);
    }

    public EtatTaches getEtatTaches() {
        return etatTaches;
    }

    public void setEtatTaches(EtatTaches etatTaches) {
        this.etatTaches = etatTaches;
        cout = etatTaches.getCout();
    }
    
    /**
     * Obtenir la position du colonne 
     * @return
     * entier, le premier colonne correspond à 0 et non à 1
     */
    public int getColPos() {
        return colPos;
    }

    /**
     * Définir l'emplacement de la colonne auquel on veut afficher la chiffre
     * @param colPos
     * Entiert
     */
    public void setColPos(int colPos) {
        this.colPos = colPos;
    }

    public int getBarSpacing() {
        return barSpacing;
    }

    public void setBarSpacing(int barSpacing) {
        this.barSpacing = barSpacing;
    }
}
