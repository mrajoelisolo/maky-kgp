package etat;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import mapping.EtatTache;

/**
 *
 * @author FireWolf
 * @since 18/12/2008
 * Cette classe représente la structure d'un état formulaire
 * des tâches, des dates, des coûts ainsi que le total des coûts
 */
public class EtatTaches {
    private float cout = 0;
    private List data = new LinkedList();
    public static int Y_POS = 150;
    
    public EtatTaches() {
    }
    
    public EtatTache getValueAt(int i) {
        return (EtatTache) data.get(i);
    }
    
    public void setValueAt(int i, EtatTache value) {
        if(i < data.size())
            data.set(i, value);
    }
    
    public void addValue(EtatTache value) {
        data.add(value);
    }
    
    public int getSize() {
        return data.size();
    }
    
    public List getList() {
        return data;
    }
    
    public float getCout() {
        evalCout();
        
        return cout;
    }
    
    /**
     * Calcule la somme des coûts des tâches, on obtient alors
     * le coût du projet
     */
    private void evalCout() {
        int somme = 0;
        for(Iterator iter = data.iterator(); iter.hasNext();) {
            EtatTache etat = (EtatTache) iter.next();
            
            somme += etat.getCout();
        }
        cout = somme;
    }
    
    /**
     * Vider la liste des données
     */
    public void viderListe() {
        System.out.println("DEBUG : vidage de la liste");
        data.clear();
    }
}
