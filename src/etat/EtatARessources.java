package etat;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import mapping.EtatARessource;

/**
 *
 * @author FireWolf
 * @since 19/12/2008
 * Cette classe représente la structure d'un état formulaire
 * des ressources, des dates, des coûts ainsi que le total des coûts
 */
public class EtatARessources {
    private float cout = 0;
    private List data = new LinkedList();    
    public static int Y_POS = 150;
    
    public EtatARessources() {
    }
    
    public EtatARessource getValueAt(int i) {
        return (EtatARessource) data.get(i);
    }
    
    public void setValueAt(int i, EtatARessource value) {
        if(i < data.size())
            data.set(i, value);
    }
    
    public void addValue(EtatARessource value) {
        data.add(value);
    }
    
    public int getSize() {
        return data.size();
    }
    
    public List getList() {
        return data;
    }
    
    public float getCout() {
        evalCout();
        
        return cout;
    }
    
    /**
     * Calcule la somme des coûts des ressources, on obtient alors
     * le coût du projet
     */
    private void evalCout() {
        int somme = 0;
        for(Iterator iter = data.iterator(); iter.hasNext();) {
            EtatARessource etat = (EtatARessource) iter.next();
            
            somme += etat.getCout();
        }
        cout = somme;
    }
    
    /**
     * Vider la liste des données
     */
    public void viderListe() {
        data.clear();
    }
}
