/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package utile;

import java.util.Date;
import java.util.Calendar;

import java.text.SimpleDateFormat;
import java.text.ParseException;
import java.util.GregorianCalendar;

/**
 *
 * @author FireWolf
 */
public abstract class ConversionDate {
    /**
     * Définir une date avec le jour, le mois et l'année
     * @param j
     * Jour de l'année
     * @param m
     * Mois de l'année
     * @param a
     * Année
     * @return
     * Instance de la classe java.util.Date
     */
    public static Date creerDate(int j, int m, int a) {
        Calendar cld = Calendar.getInstance();
        cld.set(a, m-1, j, 0, 0, 0);
        Date laDate = cld.getTime();
        return laDate;
    }
    
    /**
     * Créer une date avec le jour, le mois l'année et l'heure
     * @param j
     * Jour de l'année
     * @param m
     * Mois de l'année
     * @param a
     * Année
     * @param h
     * Heure, de 0 à 23
     * @param mn
     * Minute
     * @param s
     * Seconde
     * @return
     */
    public static Date creerDate(int j, int m, int a, int h, int mn, int s) {
        Calendar cld = Calendar.getInstance();
        cld.set(a, m-1, j, h, mn, s);
        Date laDate = cld.getTime();
        return laDate;
    }
    
    /**
     * Vérifie si la date spécifiée est valide
     * @param j
     * Jour de l'année
     * @param m
     * Mois de l'année
     * @param a
     * Année
     * @return
     * true si la date est valide, false dans le cas echéant
     */
    public static boolean dateValide(int j, int m, int a) {
        Calendar cld = Calendar.getInstance();
        cld.setLenient(false);
        cld.set(a, m, j);
        
        try {
            cld.getTime();
            
        } catch(IllegalArgumentException e) {
            return false;
        }
        
        return true;
    }
    
    /**
     * Convertir une chaîne en date
     * @param date
     * Chaîne au format 'jj-mm-yyyy'
     * @return
     * Instance de la classe java.util.Date
     */
    public static Date stringToDate(String date) {
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date maDate = dateFormat.parse(date);
            return maDate;
            
        } catch(ParseException e) {
            e.printStackTrace();
            return null;
        }
    }
    
    /**
     * Convertir une date en chaîne
     * @param date
     * Instance de la classe java.util.Date
     * @return
     * Châine au format 'jj-mm-yyyy'
     */
    public static String dateToString(Date date) {
        Calendar cld = Calendar.getInstance();
        cld.setTime(date);
        
        int annee = cld.get(Calendar.YEAR);
        // Le mois du système commence par 0 et non par 1
        int mois = cld.get(Calendar.MONTH) + 1; 
        int jour = cld.get(Calendar.DAY_OF_MONTH);
        
        String str1, str2;
        
        if(mois < 10) str1 = "0" + mois;
        else str1 = "" + mois;
        if(jour < 10) str2 = "0" + jour;
        else str2 = "" + jour;
        
        String str = "" + str2 + "-" + str1 + "-"  + annee;
        
        return str;
    }
    
    /**
     * Calcule le nombre de jours entre deux dates
     * @param debut
     * Date de début
     * @param fin
     * Date de fin
     * @return
     * Retourne un entier négatif si la date de fin est antérieur 
     * à la date de début, retourne un entier positif dans le cas contraire
     */
    public static int nbJours(Date debut, Date fin) {
        long res = 0;
        Date date1, date2;
        String input1 = ConversionDate.dateToString(debut);
        String input2 = ConversionDate.dateToString(fin);
        
        date1 = ConversionDate.stringToDate(input1);
        date2 = ConversionDate.stringToDate(input2);
        
        res = (date2.getTime() - date1.getTime()) / 1000 / 60 / 60 / 24;
        
        return (int)res;
    }
    
    /**
     * Solution correction de bug 09/12/08
     * Calcule la valeur algébrique qui sépare deux dates. Pour eviter des résulats
     * innatendus, vérifier bien l'heure associée à la date
     * @param debut
     * Date de début
     * @param fin
     * Date de fin
     * @param ech
     * Echelle de la valeur algébrique
     * @return
     * Valeur en entier
     */
    public static int nbJours(Date debut, Date fin, int ech) {
        long res = 0;
        Date date1, date2;
        String input1 = ConversionDate.dateToString(debut);
        String input2 = ConversionDate.dateToString(fin);
        
        date1 = ConversionDate.stringToDate(input1);
        date2 = ConversionDate.stringToDate(input2);
        
        res = (date2.getTime() - date1.getTime()) * ech / 1000 / 60 / 60 / 24;
        
        return (int)res;
    }
}
