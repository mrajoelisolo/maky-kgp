
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package utile.restore;
import java.util.*;
import java.text.*;
import java.util.Date;
import java.text.DateFormat;
import java.util.Calendar;
/**
 *
 * @autor Ti
 */
public class DateME {
    
    private DateME m_instance;
    public static final int WEEK_DAY = GregorianCalendar.DAY_OF_WEEK;
    public static final Date date3=null;
    public static final String DateBase = "yyyy/MM/dd";
    public static final String DateUser ="dd/MM/yyyy";
    public static final Date dateNull = null;
    //DateME dt = new DateME();
    
    /**
     * instancie la classe DateME
     * @return la variable d'instance
     */
    public DateME getInstance(){
        if (m_instance == null)
            m_instance = new DateME();
        return m_instance;
    }
    public int CompareDate(Date d1, Date d2){
       return  d1.compareTo(d2);
    }
    /**
     * Teste si la chaine reÃ§ue en paramÃ¨tre est de type date valide
     * @param m_date
     * @return true si l'aspect de la chaine est de la forme date<br><br>
     * false si c'est pas vÃ©rifiÃ©
     * @throws java.lang.Exception
     */
    public  boolean IsDate(String m_date) throws Exception{
        String m_str = "(0[1-9]|[1-2][0-9]|30|31)/(0[1-9]|1[0-2])/([0-9]{4})";
        String m_str1 = "([0-9]{4})/(0[1-9]|1[0-2])/(0[1-9]|[1-2][0-9])";
        
        if(m_date.matches(m_str) || m_date.matches(m_str1))
            return true;
        else 
            return false;
    }
    
    /**
     * Teste si la chaine reÃ§ue en paramÃ¨tre supposant dÃ©finir une date est vide
     * @param date1
     * @return true si la chaine est vide<br><br>false si la chaine n'est pas vide
     */
    public  boolean IsDateNull(String m_date1){
        Date test = new Date(m_date1);
        boolean m_bool= false;
        
        try{
            if(test.equals(dateNull)) {
                m_bool=true;  
            }else 
                m_bool = false;
        }
        catch(Exception ex){}
        return m_bool;
    }

    /**
     * Retourne le format de la date reÃ§ue en paramÃ¨tre
     * @param m_date1
     * @param m_choix
     * @return variable m_format de type String<br><br>sinon retourne null
     */
    public  String getFormat(String m_date1, String m_choix) throws Exception {
        String m_format = "";  
        if(IsDate(m_date1)){
            if(m_choix.equalsIgnoreCase("user"))
                m_format = DateUser;
            else if(m_choix.equalsIgnoreCase("database"))
                m_format=DateBase;
            else m_format = "";
        }
        return m_format;
    }
    /**
     * Compare deux variables de type date et renvoie la date maximum
     * @param m_date1
     * @param m_date2
     * @return la date maximum de type string
     * @throws java.lang.Exception
     */
    public  String getDateMax(String m_date1, String m_date2) throws Exception {
        Date max = null; 
        Date max1;
        Date max2;
        String m_ret = "";
        
        if(IsDate(m_date1) && IsDate(m_date2)) {
            SimpleDateFormat dateStandard = new SimpleDateFormat(DateUser);
            
            max1 = StringToDate(m_date1,DateUser);
            max2 = StringToDate(m_date2,DateUser);
            
            if (max1.compareTo(max2)<0){
                max = max2;
            }else 
                max= max1;
            
            m_ret= dateStandard.format(max);   
        }
        else m_ret=null;
        return m_ret;
    }

    /**
     * Formatte la date selon l'utilisation<br>
     *   - Si la variable m_choix equivaut Ã  "user" -> le format de la date est "dd/MM/yyyy"<br>
     *   - Si la variable m_choix equivaut Ã  "database" -> le format de la date est "yyyy/MM/dd"<br>
     *   - Sinon retourne null
     *
     * @param m_date1
     * @param m_choix
     * @return la date formattÃ©e <br><br>sinon retourne null
     * @throws java.lang.Exception
     */
     public  String FormatteDate(String m_date1,String m_choix) throws Exception {
        Date m_dat=StringToDate(m_date1,DateUser);
        Calendar m_c = Calendar.getInstance();
        m_c.setLenient(false);
        m_c.setTime(m_dat);
        m_dat=m_c.getTime();
           
        if(m_choix.equalsIgnoreCase("user")) {
            SimpleDateFormat m_sd = new SimpleDateFormat(DateUser);
            return m_sd.format(m_dat);
        }
        if(m_choix.equalsIgnoreCase("database")) {
            SimpleDateFormat m_sd1 = new SimpleDateFormat(DateBase);
            return m_sd1.format(m_dat);}
        else 
            return null;
    }
 
    /**
     * Compare deux variables de type date et renvoie la date minimum
     * @param m_date1
     * @param m_date2
     * @return la date minimum de type string
     * @throws java.lang.Exception
     */
    public  String getDateMin(String m_date1,String m_date2) throws Exception{
        Date m_min=null; 
        Date m_min1;
        Date m_min2;
        SimpleDateFormat dateStandard = new SimpleDateFormat(DateUser);
        
        m_min1 = StringToDate(m_date1,DateUser);
        m_min2 = StringToDate(m_date2,DateUser);
        
        if (m_min1.compareTo(m_min2) < 0) {
            m_min = m_min1;
        }else 
            m_min= m_min2;
        
        return dateStandard.format(m_min);
    }

    /**Retourne le jour d'une date reÃ§ue en paramÃ¨tre
     * @param date1
     * @return variable m_nbjr de type int
     */
    public  int getJours(String m_date1) throws Exception{
         int m_nbjr=0;
         int m_ret=0;
         if(IsDate(m_date1)){
             String m_jour=m_date1.substring(0, 2);
             m_nbjr=Integer.parseInt(m_jour);
             m_ret= m_nbjr;
         }
         else m_ret=0;
         return m_ret;
    }

    /**
     * Retourne l'index du mois d'une date reÃ§ue en paramÃ¨tre
     * @param date1
     * @return variable m_indexmois de type int
     */
    public  int getMois(String m_date1) throws Exception{
        int m_indexmois=0;
        int m_ret=0;
        if(IsDate(m_date1)){
            String m_mois = m_date1.substring(3, 5);
            m_indexmois=Integer.parseInt(m_mois);
            m_ret= m_indexmois;
        }
        else m_ret=0;
        return m_ret;
    }

    /**
     * Retourne l'annÃ©e d'une date reÃ§ue en paramÃ¨tre
     * @param m_date1
     * @return variable m_indexannee de type int
     */
     public int getAnnee(String m_date1) {
        int m_ret = 0;    
        try {
                int m_indexannee = 0;
                if (IsDate(m_date1)) {
                    String m_annee = m_date1.substring(6);
                    m_indexannee = Integer.parseInt(m_annee);
                    m_ret = m_indexannee;
                } else {
                    m_ret = 0;
                }
            } catch (Exception ex) {
               
            }
             return m_ret;
    }

    /**
     * Compte tous les annÃ©es bissextiles entre deux dates reÃ§ues en paramÃ¨tre
     * @param m_a1
     * @param m_a2
     * @return le nombre d'annÃ©e bissextile entre les deux dates
     */
    public static int CompteBissex(int m_a1,int m_a2){
        return((m_a2/4)-(m_a2/100)+(m_a2/400))-((m_a1/4)-(m_a1/100)+(m_a1/400));
    }

    /**
     * Determine si l'annÃ©e reÃ§ue en paramÃ¨tre est une annÃ©e bissextile
     * @param a
     * @return
     */
    static boolean estBissextile(int a) {
        return a % 4 == 0 && (a % 100 != 0 || a % 400 == 0);
    }

    /**
     * Determine l'intervalle en jours entre deux dates reÃ§ue en paramÃ¨tre
     * @param m_date1
     * @param m_date2
     * @return le nombre de jour d'intervalle<br><br>
     * sinon retourne 0
     * @throws java.lang.Exception
     */
    public  long getIntervale(String m_date1, String m_date2) throws Exception {
        long result;
        long m_dt1 = 0;
        long m_dt2 = 0;
        long nbjour = 0;
        
        if(IsDate(m_date1) && IsDate(m_date2)) {
            Date m_dat1 = StringToDate(m_date1, DateUser);
            Date m_dat2 = StringToDate(m_date2, DateUser);
            
            m_dt1 = m_dat1.getTime();
            m_dt2 = m_dat2.getTime();
            
            result = m_dt1 - m_dt2;
            
            nbjour = result / 1000 / 60 / 60 / 24;
                if(nbjour<0)nbjour *= -1;
        }
        return nbjour;
    }
 
    /**
     * Capture la date systÃ¨me en format dd/MM/yyyy
     * @return la date et l'heure systÃ¨me
     */
     public String Localdate(){
        Date m_dat=new Date();
        SimpleDateFormat m_form=new SimpleDateFormat(DateUser);
        return m_form.format(m_dat); 
    }

    /**
     * Ajoute Ã  une date reÃ§ue en paramÃ¨tre un nombre de jour aussi reÃ§u en paramÃ¨tre
     * @param m_date4
     * @param m_jours
     * @return la date de type string avec le nombre de jours ajoutÃ©s 
     * @throws java.text.ParseException
     * @throws java.lang.Exception
     */
     public String AddJours(String m_date4,int m_jours) throws ParseException, Exception{
        Date m_date_after;
        String m_ret="";
        Date m_date6;
        m_date6= StringToDate(m_date4,DateUser);
        SimpleDateFormat m_dateStandard = new SimpleDateFormat(DateUser);
        if(IsDate(m_date4)){   
            Calendar m_calendar = Calendar.getInstance();
            m_calendar.setLenient(false);
            m_calendar.setTime(m_date6);
            Calendar m_calendar2 = Calendar.getInstance();
            m_calendar2.clear();
            m_calendar2.set(m_calendar.get(Calendar.YEAR), m_calendar.get(Calendar.MONTH),m_calendar.get(Calendar.DAY_OF_MONTH)+m_jours);
            m_date_after=m_calendar2.getTime();
            m_ret= m_dateStandard.format(m_date_after);}
        else m_ret=null;
        return m_ret;
    }

    /**
     * Supprime dans une date donnÃ©e un nombre de jours reÃ§us en paramÃ¨tre
     * @param m_date4
     * @param m_jours
     * @return la date de type string avec le nombre de jours supprimÃ©s
     * @throws java.text.ParseException
     * @throws java.lang.Exception
     */
     public String RemoveJours(String m_date4,int m_jours) throws ParseException, Exception{
        Date m_date_after;
        String m_ret="";
        Date m_date6;
        m_date6= StringToDate(m_date4,DateUser);
        SimpleDateFormat m_dateStandard = new SimpleDateFormat(DateUser);
        if(IsDate(m_date4)){
            Calendar m_calendar = Calendar.getInstance();
            m_calendar.setLenient(false);
            m_calendar.setTime(m_date6);
            Calendar m_calendar2 = Calendar.getInstance();
            m_calendar2.clear();
            m_calendar2.set(m_calendar.get(Calendar.YEAR), m_calendar.get(Calendar.MONTH),m_calendar.get(Calendar.DAY_OF_MONTH)-m_jours);
            m_date_after=m_calendar2.getTime();
            m_ret= m_dateStandard.format(m_date_after);
        }
        else m_ret=null;
        return m_ret;
    }

    /**
     * CrÃ©e une date avec l'index du jour, l'index du mois, l'index de l'annÃ©e <br><br>
     * et le format de la date
     * @param m_format
     * @param m_jours
     * @param m_mois
     * @param m_annee
     * @return la date rÃ©cemment crÃ©Ã©e
     */
     public String CreateDate(String m_format,int m_jours,int m_mois,int m_annee){
        Date m_date_created;
        int m_mois1 =0;
        m_mois1 = m_mois -1;
        Calendar m_c = Calendar.getInstance();
        m_c.setLenient(false);
        m_c.set(m_annee,m_mois1,m_jours);  
        m_date_created= m_c.getTime(); 
        SimpleDateFormat m_sdf = new SimpleDateFormat(m_format);
        return m_sdf.format(m_date_created);
    }

    /**
     * Convertit une date en chaine de caractÃ¨re
     * @param m_date
     * @return une chaine de caractÃ¨re
     * @throws java.lang.Exception
     */
    public  String DateToString(String m_date) throws Exception{
        Date m_dat;
        String m_ret="";
        if(IsDate(m_date)){
        m_dat= StringToDate(m_date,DateUser);
        Locale m_locale = Locale.getDefault();
        DateFormat m_dateFormat = DateFormat.getDateInstance(DateFormat.SHORT, m_locale);
        System.out.println(m_dateFormat.format(m_dat));
        m_ret= m_dateFormat.format(m_dat);
        }
        return m_ret;
    }

    /**
     * Convertit une chaine de caractÃ¨re en date avec le format reÃ§u en paramÃ¨tre
     * @param m_sDate
     * @param m_sFormat
     * @return une date
     * @throws java.lang.Exception
     */
    public  Date StringToDate(String m_sDate, String m_sFormat)  {
       try{
        Date m_test = new Date(); 
        if(IsDate(m_sDate)) {
            SimpleDateFormat m_sdf = new SimpleDateFormat(m_sFormat);
            
            m_test = m_sdf.parse(m_sDate);
            
            return m_test;
        }
        else return null;

       }catch(Exception e){
           
       }
       return null;
    } 

    /**
     * Retourne le libellÃ© en franÃ§ais du jour courant 
     * @return une chaine de caractÃ¨re
     */
     public String getDayOfWeekFR(){
        GregorianCalendar m_c = new GregorianCalendar();
        return JourSemaineFR(m_c.get(WEEK_DAY));
    }

    /**
     * Retourne le libellÃ© en anglais du jour courant 
     * @return une chaine de caractÃ¨re
     */
     public String getDayOfWeekEN(){
        GregorianCalendar m_c = new GregorianCalendar();
        return JourSemaineUSA(m_c.get(WEEK_DAY));
    }

    /**
     * Retourne le libellÃ© en malgache du jour courant 
     * @return une chaine de caractÃ¨re
     */
     public String getDayOfWeekMG(){
        GregorianCalendar m_c = new GregorianCalendar();
       return JourSemaineMG(m_c.get(WEEK_DAY));
    }

    /**
     * Retourne le libellÃ© en franÃ§ais du mois courant
     * @return une chaine de caractÃ¨re
     */
     public String getMonthOfYearFR(){
        GregorianCalendar m_c=new GregorianCalendar();
        return MoisAnneeFR(m_c.get(GregorianCalendar.MONTH));
    }

    /**
     * Retourne le libellÃ© en anglais du mois courant
     * @return une chaine de caractÃ¨re
     */
     public String getMonthOfYearEN(){
        GregorianCalendar m_c=new GregorianCalendar();
        return MoisAnneeEN(m_c.get(GregorianCalendar.MONTH));
    }

    /**
     * Retourne le libellÃ© en malgache du mois courant
     * @return
     */
     public String getMonthOfYearMG(){
        GregorianCalendar m_c=new GregorianCalendar();
        return MoisAnneeMG(m_c.get(GregorianCalendar.MONTH));
    }
    /**
     * convertit les libellÃ© des jours de la semaine en FranÃ§ais
     * @param m_i
     * @return le libellÃ© du jour en franÃ§ais
     */
    private  String JourSemaineFR(int m_i) {
            if (m_i == GregorianCalendar.MONDAY)
                    return "Lundi";
            if (m_i == GregorianCalendar.TUESDAY)
                    return "Mardi";
            if (m_i == GregorianCalendar.WEDNESDAY)
                    return "Mercredi";
            if (m_i == GregorianCalendar.THURSDAY)
                    return "Jeudi";
            if (m_i == GregorianCalendar.FRIDAY)
                    return "Vendredi";
            if (m_i == GregorianCalendar.SATURDAY)
                    return "Samedi";
            if (m_i == GregorianCalendar.SUNDAY)
                    return "Dimanche";
            return "<Error: "+m_i+">";
    }

    /**
     * convertit les libellÃ© des jours de la semaine en Malgache
     * @param m_i
     * @return le libellÃ© du jour en malgache
     */
    private  String JourSemaineMG(int m_i) {
            if (m_i == GregorianCalendar.MONDAY)
                    return "Alatsinainy";
            if (m_i == GregorianCalendar.TUESDAY)
                    return "Talata";
            if (m_i == GregorianCalendar.WEDNESDAY)
                    return "Alarobia";
            if (m_i == GregorianCalendar.THURSDAY)
                    return "Alakamisy";
            if (m_i == GregorianCalendar.FRIDAY)
                    return "Zoma";
            if (m_i == GregorianCalendar.SATURDAY)
                    return "Asabotsy";
            if (m_i == GregorianCalendar.SUNDAY)
                    return "Alahady";
            return "<Error: "+m_i+">";
    }

    /**
     * convertit les libellÃ© des jours de la semaine en Anglais
     * @param m_i
     * @return le libellÃ© du jour en anglais
     */
    private  String JourSemaineUSA(int m_i) {
            if (m_i == GregorianCalendar.MONDAY)
                    return "Monday";
            if (m_i == GregorianCalendar.TUESDAY)
                    return "Tuesday";
            if (m_i == GregorianCalendar.WEDNESDAY)
                    return "Wednesday";
            if (m_i == GregorianCalendar.THURSDAY)
                    return "Thursday";
            if (m_i == GregorianCalendar.FRIDAY)
                    return "Friday";
            if (m_i == GregorianCalendar.SATURDAY)
                    return "Saturday";
            if (m_i == GregorianCalendar.SUNDAY)
                    return "Sunday";
            return "<Error: "+m_i+">";
    }

    /**
     * Convertit le libellÃ© du mois de l'annÃ©e en Anglais
     * @param m_i
     * @return libellÃ© du mois en anglais
     */
    private  String MoisAnneeEN(int m_i){
        if(m_i==GregorianCalendar.JANUARY)
            return "January";
        if (m_i==GregorianCalendar.FEBRUARY)
            return "February";
        if(m_i==GregorianCalendar.MARCH)
            return "March";
        if(m_i==GregorianCalendar.APRIL)
            return "April";
        if(m_i==GregorianCalendar.MAY)
            return"May";
        if(m_i==GregorianCalendar.JUNE)
            return "June";
        if(m_i==GregorianCalendar.JULY)
            return "July";
        if(m_i==GregorianCalendar.AUGUST)
            return "August";
        if(m_i==GregorianCalendar.SEPTEMBER)
            return "September";
        if(m_i==GregorianCalendar.OCTOBER)
            return "October";
        if(m_i==GregorianCalendar.NOVEMBER)
            return "November";
        if(m_i==GregorianCalendar.DECEMBER)
            return "December";
        return "<ERROR:"+m_i+">";
   }

    /**
     * Convertit le libellÃ© du mois de l'annÃ©e en FranÃ§ais 
     * @param m_i
     * @return le libellÃ© du mois en franÃ§ais
     */
    public  String MoisAnneeFR(int m_i){
       if(m_i==GregorianCalendar.JANUARY)
            return "Janvier";
        if (m_i==GregorianCalendar.FEBRUARY)
            return "FÃ©vrier";
        if(m_i==GregorianCalendar.MARCH)
            return "Mars";
        if(m_i==GregorianCalendar.APRIL)
            return "Avril";
        if(m_i==GregorianCalendar.MAY)
            return"Mai";
        if(m_i==GregorianCalendar.JUNE)
            return "Juin";
        if(m_i==GregorianCalendar.JULY)
            return "Juillet";
        if(m_i==GregorianCalendar.AUGUST)
            return "AoÃ»t";
        if(m_i==GregorianCalendar.SEPTEMBER)
            return "Septembre";
        if(m_i==GregorianCalendar.OCTOBER)
            return "Octobre";
        if(m_i==GregorianCalendar.NOVEMBER)
            return "Novembre";
        if(m_i==GregorianCalendar.DECEMBER)
            return "Decembre";
        return "<ERROR:"+m_i+">";
    }

    /**
     * Convertit le libellÃ© du mois de l'annÃ©e en Malgache
     * @param m_i
     * @return
     */
    public  String MoisAnneeMG(int m_i){
       if(m_i==GregorianCalendar.JANUARY)
            return "Janoary";
        if (m_i==GregorianCalendar.FEBRUARY)
            return "Febroary";
        if(m_i==GregorianCalendar.MARCH)
            return "Martsa";
        if(m_i==GregorianCalendar.APRIL)
            return "Aprily";
        if(m_i==GregorianCalendar.MAY)
            return"May";
        if(m_i==GregorianCalendar.JUNE)
            return "Jona";
        if(m_i==GregorianCalendar.JULY)
            return "Jolay";
        if(m_i==GregorianCalendar.AUGUST)
            return "Aogositra";
        if(m_i==GregorianCalendar.SEPTEMBER)
            return "Septambra";
        if(m_i==GregorianCalendar.OCTOBER)
            return "Octobra";
        if(m_i==GregorianCalendar.NOVEMBER)
            return "Novambra";
        if(m_i==GregorianCalendar.DECEMBER)
            return "Desambra";
        return "<ERROR:"+m_i+">";
    }

   
}
