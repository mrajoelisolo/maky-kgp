/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package utile;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.Rectangle;
import javax.swing.JPanel;

/**
 *
 * @author FireWolf
 */
public abstract class PaneManager {
    /**
     * Obtenir les dimensions du décalage du bounds par rapport à la 
     * fenêtre mêre, c.a.d le panel
     * @param panel
     * Instance de javax.swing.jPanel;
     * @param decalage
     * Valeur du décalage
     * @return
     * Instance de java.awt.Rectangle;
     */
    public static Rectangle obtenirDecalageBounds(JPanel panel, int decalage) {
        Rectangle res;
        Rectangle b = panel.getBounds();
        
        res = new Rectangle(new Point(decalage, decalage), new Dimension(b.width - 2*decalage, b.height - 2*decalage));
        
        return res;
    }
}
