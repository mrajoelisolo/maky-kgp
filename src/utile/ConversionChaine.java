/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package utile;

/**
 *
 * @author Administrateur
 */
public abstract class ConversionChaine {
    /**
     * Convertit un tableau de caractères en String
     * @param chars - tableau de caractères
     * @return
     * Instance de la classe String
     */
    public static String charsToString(char[] chars) {
        String res = "";
        
        for(int i = 0; i < chars.length; i++)
            res += chars[i];
        
        return res;
    }
}
