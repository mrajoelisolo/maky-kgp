/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package utile;

import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Administrateur
 */
public class TableModel extends AbstractTableModel {
   private Object[][] donnees;
   private String[] titres;
   
   public TableModel(Object donnees[][], String titres[]) { 
      this.donnees = donnees; 
      this.titres = titres; 
   } 
   public int getColumnCount() { 
      return donnees[0].length; 
   }
   public Object getValueAt(int row, int col) { 
      return donnees[row][col]; 
   }
   public int getRowCount() { 
      return donnees.length; 
   }
   
   @Override
   public String getColumnName(int col){ 
      return titres[col]; 
   } 
}

