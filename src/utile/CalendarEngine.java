 /*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package utile;

import java.util.Date;
import java.util.Calendar;
/**
 *
 * @author FireWolf
 */
public abstract class CalendarEngine {
    private static final String[] ms = {"janvier", "fevrier", "mars", "avril", "mai", "juin", 
        "juillet", "aout", "septembre", "octobre", "novembre", "decembre"};
    
    private static final String[] ms2 = {"jan", "fev", "mars", "avr", "mai", "juin", 
        "juil", "aout", "sept", "oct", "nov", "dec"};
    
    private static final String[] sem = {"lundi", "mardi", "mercredi", "jeudi", "vendredi", 
        "samedi", "dimanche"};
    
    /**
     * Permet de savoir si une année est bissextile ou non
     * @param annee
     * @return 1 si l'année en question est bissextile sinon retourne 0
     */
    public static int estBissextile(int annee) {
        if(annee%4 == 0)
            if(annee%100 != 0) return 1;
            else if(annee%400 == 0) return 1;
        return 0;
    }
    
    /**
     * Obtenir le jour maximal d'un mois de l'année
     * @param mois
     * Mois de l'année, le premier indice est 0
     * @param annee
     * L'année en question
     * @return
     */
    public static int obtenirJmax(int mois, int annee) {
        int m = mois;
        if(m == 0 || m == 2 || m == 4 || m == 6 || m == 7 || m == 9 || m == 11)
            return 31;
        else if(m == 1) {
            if(estBissextile(annee) == 1) return 29;
            else return 28;
        } else return 30;
    }
    
    /**
     * Obtenir le jour de la semaine, le premier indice commence par 0
     * @param j
     * @param m
     * Le premier mois commence par 0
     * @param a
     * @return
     */
    public static int obtenirIndiceSem(int j, int m, int a) {
        int somme = j;
        
        for(int i = 0; i < m; i++)
            somme += obtenirJmax(i, a);
        somme += a + 4 + (a - 1) / 4;
        
        return (somme%7);
    }
    
    /**
     * 
     * @return
     * Retourne unt chaîne indiquant le nom du mois concerné
     * Le premier mois commence par 0
     */
    public static String obtenirNomMois(int mois) {
        return ms[mois];
    }
    
    /**
     * Retourne une chaîne indiquant le nom raccourci du mois concerné
     * Le premier mois commence par 0
     * @param mois
     * @return
     */
    public static String obtenirNomMoisRaccourci(int mois) {
        return ms2[mois];
    }
    
    /**
     * Obtenir la date du lendemain. Le mois commence par l'indice 0
     * @param date
     * @return
     */
    public static Date obtenirJourSuivant(Date date) {
        int jour, mois, annee;
        Calendar cld = Calendar.getInstance();
        cld.setTime(date);
        jour = cld.get(Calendar.DAY_OF_MONTH);
        mois = cld.get(Calendar.MONTH);
        annee = cld.get(Calendar.YEAR);
        
        if(jour == cld.getMaximum(Calendar.DAY_OF_MONTH)) {
            jour = 1;
            if(mois == 11) {
                mois = 0;
                annee++;
            }else
                mois++;
        }
        else
        {
            jour++;
        }
        
        cld.set(annee, mois, jour);
        return cld.getTime();
    }
}
