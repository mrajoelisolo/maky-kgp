 /*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package utile;

import java.util.Date;
import java.util.Calendar;
/**
 *
 * @author FireWolf
 */
public class CalendarEngine2 {
    private int jour;
    private int mois;
    private int annee;
    private String[] ms = {"janvier", "fevrier", "mars", "avril", "mai", "juin", 
        "juillet", "aout", "septembre", "octobre", "novembre", "decembre"};
    private String[] sem = {"lundi", "mardi", "mercredi", "jeudi", "vendredi", 
        "samedi", "dimanche"};
    
    private int[][] tab = new int[6][7];;
    
    public CalendarEngine2() {
        jour = 1; mois = 0; annee = 2000;
        
        initCalendrier(mois, annee);   
    }
    
    public CalendarEngine2(int jour, int mois, int annee) {
        this.jour = jour;
        this.mois = mois - 1;
        this.annee = annee;
        
        initCalendrier(mois - 1, annee);   
    }
    
    private void initCalendrier(int mois, int annee) {
        for(int i = 0; i < 6; i++)
            for(int j = 0; j < 7; j++)
                tab[i][j] = 0; 
        
        //Définition du calendrier
        
        int k = 1;
        int iJsem = obtenirIndiceSem();
        int iJmax = obtenirJmax();
        
        for(int i = iJsem; i < 7; i++) {
            tab[0][iJsem] = k;
            k++;
        }
        
        for(int i = 1; i < 6; i++) {
            for(int j = 0; j < 7; j++) {
                tab[i][j] = k;
                k++;
                if(k > iJmax) break;
            }
            if(k > iJmax) break;
        }
    }
    
    /**
     * Permet de savoir si une année est bissextile ou non
     * @param annee
     * @return 1 si l'année en question est bissextile sinon retourne 0
     */
    public static int estBissextile(int annee) {
        if(annee%4 == 0)
            if(annee%100 != 0) return 1;
            else if(annee%400 == 0) return 1;
        return 0;
    }
    
    public static int obtenirJmax(int mois, int annee) {
        int m = mois;
        if(m == 0 || m == 2 || m == 4 || m == 6 || m == 7 || m == 9 || m == 11)
            return 31;
        else if(m == 1) {
            if(estBissextile(annee) == 1) return 29;
            else return 28;
        } else return 30;
    }
    
    /**
     * 
     * @return
     * Retourne un entier indiquant le dernier jour du mois concerné
     */
    
    public int obtenirJmax() {
        return obtenirJmax(mois, annee);
    }
    
    /**
     * Obtenir le jour de la semaine, le premier indice commence par 0
     * @param j
     * @param m
     * Le premier mois commence par 0
     * @param a
     * @return
     */
    public static int obtenirIndiceSem(int j, int m, int a) {
        int somme = j;
        
        for(int i = 0; i < m; i++)
            somme += obtenirJmax(i, a);
        somme += a + 4 + (a - 1) / 4;
        
        return (somme%7);
    }
    
    /**
     * 
     * @return
     * Retourne une indice correspondant au jour de la semaine.
     * Compris entre 0 et 6
     */
    
    public int obtenirIndiceSem() {
        return obtenirIndiceSem(jour, mois, annee);
    }
    
    /**
     * 
     * @return
     * Retourne unt chaîne indiquant le nom du mois concerné
     */
    
    public String obtenirNomMois() {
        return ms[mois];
    }
    
    /**
     * 
     * @return
     * Retourne le nom du jour concerné (lundi, mardi, mercredi, jeudi...)
     */
    
    public String obtenirNomJour() {
        int jSem = obtenirIndiceSem(jour, mois, annee);
        return sem[jSem];
    }
    
    /**
     * 
     * @return
     * Retourne un entier indiquant le jour du mois, allant de 1 a 31
     */
    
    public int obtenirJour() {
        return jour;
    }
    
    /**
     * 
     * @return
     * Retourne un entier indiquant l'année concernée
     */
    
    public int obtenirAnnee() {
        return annee;
    }
    
    /**
     * Permet de redéfinir la date
     * @param jour
     * @param mois
     * @param annee
     */
    
    public void definirDate(int jour, int mois, int annee) {
        this.jour = jour;
        this.mois = mois - 1;
        this.annee = annee;
    }
    
    public void afficherCld() {
        System.out.println(tab[1][0]);
    }
    
    public Date obtenirDate() {
        Calendar cld = Calendar.getInstance();
        cld.set(annee, mois, jour);
        Date maDate = cld.getTime();
        
        return maDate;
    }
    
    /**
     * Obtenir le nom du mois, le premier mois commence par 0 et 
     * le dernier par 11
     * @param n
     * Indice du mois
     * @return
     * Nom du mois
     */
    public static String obtenirNomMois(int n) {
        String[] mois = {"janvier", "fevrier", "mars", "avril", "mai", "juin", 
        "juillet", "aout", "septembre", "octobre", "novembre", "decembre"};
        
        return mois[n];
    }
    
    /**
     * Obtenir la date du lendemain. Le mois commence par l'indice 0
     * @param date
     * @return
     */
    public static Date obtenirJourSuivant(Date date) {
        Date res = date;
        
        int jour, mois, annee;
        Calendar cld = Calendar.getInstance();
        cld.setTime(date);
        jour = cld.get(Calendar.DAY_OF_MONTH);
        mois = cld.get(Calendar.MONTH);
        annee = cld.get(Calendar.YEAR);
        
        if(jour == cld.getMaximum(Calendar.DAY_OF_MONTH)) {
            jour = 1;
            if(mois == 11) {
                mois = 0;
                annee++;
            }else
                mois++;
        }
        else
        {
            jour++;
        }
        
        cld.set(annee, mois, jour);
        return cld.getTime();
    }
}
