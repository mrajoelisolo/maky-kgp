/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package utile;

/**
 *
 * @author RAJOELISOLO Mitanjo
 * L'utilité de cette classe est équivalent à un RecordSet de VB, elle
 * permet de parcourir la liste et de faire diverses opérations
 */
public class MyRecordSet {
    private Object[] value;
    private int cursor;
    private int ncount;
    
    public MyRecordSet() {}
    
    public MyRecordSet(Object[] value) {
        this.cursor = 0;
        this.value = value;
        this.ncount = value.length;
    }
    
    public Object getValueAt(int n) {
        return this.value[n];
    }
    
    public Object getCurrentValue() {
        return this.value[cursor];
    }
    
    public int getCurrentPosition() {
        return cursor;
    }
    
    public void setCurrentPosition(int n) {
        cursor = n;
    }
    
    public void setValue(Object[] value) {
        this.cursor = 0;
        this.value = value;
        this.ncount = value.length;
    }
    
    public int size() {
        return ncount;
    }
    
    public void next() {
        if(cursor < ncount-1)
            cursor++;
    }
    
    public void previous() {
        if(cursor > 0)
            cursor--;
    }
    
    public void first() {
        cursor = 0;
    }
    
    public void last() {
        cursor = ncount;
    }
}
