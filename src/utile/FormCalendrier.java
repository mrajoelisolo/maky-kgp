package utile;

import com.toedter.calendar.JCalendar;
//import java.text.SimpleDateFormat;
import java.util.Date;

public class FormCalendrier extends javax.swing.JDialog {
    private JCalendar cal = null;
    private boolean ok = false;
    
    public FormCalendrier(javax.swing.JDialog parent, int x, int y, boolean entete, String title) {
        super(parent, true);
        this.setUndecorated(!entete);
        this.setTitle(title);
        initComponents();
        cal = new JCalendar();
        pane1.add(cal);
        cal.setBounds(5, 5, 320, 180);
        this.setBounds(parent.getX() + x, parent.getY() + y, 350, 210);
    }
    
    /*public String getDate(){
        //SimpleDateFormat _formatMY = new SimpleDateFormat("yyyy-MM-dd");
        //SimpleDateFormat _formatMY = new SimpleDateFormat("dd/MM/yyyy");
        //return _formatMY.format(cal.getDate().getTime());
        return ConversionDate.dateToString(cal.getDate());
    }*/
    
    public Date getDate() {
        return cal.getDate();
    }
            
    public boolean isOk() {
        return ok;
    }

    public void setOk(boolean ok) {
        this.ok = ok;
    }
        
    // <editor-fold defaultstate="collapsed" desc=" Generated Code ">//GEN-BEGIN:initComponents
    private void initComponents() {
        pane1 = new javax.swing.JPanel();
        cmdfermer = new javax.swing.JButton();
        cmdAnnuler = new javax.swing.JButton();

        getContentPane().setLayout(null);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("");
        pane1.setLayout(null);

        pane1.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        cmdfermer.setMnemonic('f');
        cmdfermer.setText("Ok");
        cmdfermer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmdfermerActionPerformed(evt);
            }
        });

        pane1.add(cmdfermer);
        cmdfermer.setBounds(270, 180, 50, 23);

        cmdAnnuler.setText("Annuler");
        cmdAnnuler.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmdAnnulerActionPerformed(evt);
            }
        });

        pane1.add(cmdAnnuler);
        cmdAnnuler.setBounds(185, 180, 80, 23);

        getContentPane().add(pane1);
        pane1.setBounds(0, 0, 330, 210);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void cmdAnnulerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmdAnnulerActionPerformed
        setOk(false);
        dispose();
    }//GEN-LAST:event_cmdAnnulerActionPerformed

    private void cmdfermerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmdfermerActionPerformed
        setOk(true);
        dispose();
    }//GEN-LAST:event_cmdfermerActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton cmdAnnuler;
    private javax.swing.JButton cmdfermer;
    private javax.swing.JPanel pane1;
    // End of variables declaration//GEN-END:variables
}
