/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package utile;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

/**
 *
 * @author FireWolf
 */
public class MyCalendar extends JFrame implements ActionListener {
    private int mois;
    private int annee;
    private static String[] ms = {"janvier", "fevrier", "mars", "avril", "mai",
        "juin", "juillet", "aout", "septembre", "octobre", "novembre", "decembre"};
    
    private JLabel lMois;
    private JLabel lAnnee;
    private JButton[][] bJour;
    private JLabel[] lJour = new JLabel[7];
    private Container container;
    private CalendarEngine2 myCalendar;
    
    public MyCalendar(int mois, int annee) {
        lMois = new JLabel(ms[mois]);
        lMois.setBounds(10, 5, 150, 20);
        lAnnee = new JLabel("" + annee);
        lAnnee.setBounds(200, 5, 100, 20);
        
        container = getContentPane();
        container.setLayout(null);
        
        container.add(lAnnee);
        container.add(lMois);
        
        dessinerCalendrier(mois+1, annee);
        
        setTitle("Calendrier"); setSize(450, 260);
        setVisible(true);
    }
    
    public void dessinerCalendrier(int mois, int annee) {
        int k = 1, xpos = 10, ypos = 50;
        CalendarEngine2 cld = new CalendarEngine2(1, mois, annee);
        int iJsem = cld.obtenirIndiceSem();
        int iJmax = cld.obtenirJmax();
        
        lJour[0] = new JLabel("Lun");
        lJour[1] = new JLabel("Mar");
        lJour[2] = new JLabel("Mer");
        lJour[3] = new JLabel("Jeu");
        lJour[4] = new JLabel("Ven");
        lJour[5] = new JLabel("Sam");
        lJour[6] = new JLabel("Dim");
        
        for(int i = 0; i < 7; i++) {
            lJour[i].setBounds(i * 55 + 20, 30, 50, 10);
            container.add(lJour[i]);
        }
        
        System.out.println(""+iJsem);
        bJour = new JButton[6][7];
        
        for(int i = iJsem; i < 7; i++) {
            bJour[0][iJsem] = new JButton(""+k);
            k++;
            bJour[0][iJsem].setBounds(xpos + 55 * iJsem, ypos, 50, 25);
            bJour[0][iJsem].addActionListener(this);
            container.add(bJour[0][iJsem]);
            xpos += 55;
        }
        
        for(int i = 1; i < 6; i++) {
            xpos = 10;
            for(int j = 0; j < 7; j++) {
                bJour[i][j] = new JButton(""+k);
                k++;
                bJour[i][j].setBounds(xpos + 55 * j, ypos + 30 * i, 50, 25);
                bJour[i][j].addActionListener(this);
                container.add(bJour[i][j]);
                if(k > iJmax) break;
            }
            if(k > iJmax) break;
        }
    }
    
    public void setCalendar(CalendarEngine2 myCalendar) {
        this.myCalendar = myCalendar;
    }
    
    public CalendarEngine2 getCalendar() {
        return myCalendar;
    }
    
    public void actionPerformed(ActionEvent e) {
        String str = e.getActionCommand();
         
        if(str.equals(e.getActionCommand())) {
            myCalendar.definirDate(Integer.parseInt(str), mois-2, annee);
            this.dispose();
        }     
    }
}
