-- phpMyAdmin SQL Dump
-- version 2.9.1.1
-- http://www.phpmyadmin.net
-- 
-- Serveur: localhost
-- Généré le : Dimanche 21 Décembre 2008 à 14:48
-- Version du serveur: 5.0.27
-- Version de PHP: 5.2.0
-- 
-- Base de données: `kronos`
-- 

-- --------------------------------------------------------

-- 
-- Structure de la table `agenda`
-- 

CREATE TABLE `agenda` (
  `idAgenda` int(11) NOT NULL auto_increment,
  `idProfil` int(11) NOT NULL,
  `dateAgenda` date NOT NULL,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY  (`idAgenda`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

-- 
-- Contenu de la table `agenda`
-- 

INSERT INTO `agenda` (`idAgenda`, `idProfil`, `dateAgenda`, `description`) VALUES 
(1, 1, '2008-11-23', 'Appeler Hinata : lui parles des nouvelles'),
(2, 1, '2008-11-25', 'Correction livre');

-- --------------------------------------------------------

-- 
-- Structure de la table `aressource_tache`
-- 

CREATE TABLE `aressource_tache` (
  `idARessource` int(11) NOT NULL,
  `idTache` int(11) NOT NULL,
  PRIMARY KEY  (`idARessource`,`idTache`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- 
-- Contenu de la table `aressource_tache`
-- 

INSERT INTO `aressource_tache` (`idARessource`, `idTache`) VALUES 
(5, 35),
(5, 39),
(5, 44),
(5, 64),
(7, 19),
(7, 35),
(7, 39),
(7, 44),
(7, 65);

-- --------------------------------------------------------

-- 
-- Structure de la table `autreressource`
-- 

CREATE TABLE `autreressource` (
  `idAressource` int(11) NOT NULL auto_increment,
  `nomAressource` varchar(100) NOT NULL,
  `coutAressource` float NOT NULL default '0',
  `description` varchar(255) default NULL,
  PRIMARY KEY  (`idAressource`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

-- 
-- Contenu de la table `autreressource`
-- 

INSERT INTO `autreressource` (`idAressource`, `nomAressource`, `coutAressource`, `description`) VALUES 
(5, 'Ordinateur Portable Dell', 1200, 'Ordinateur de l''entreprise'),
(6, 'Projecteur Sony', 800, 'Utilisé pour la formation, conférences'),
(7, 'Serveur Dell', 1000, 'Serveur de donnée');

-- --------------------------------------------------------

-- 
-- Structure de la table `dateferie`
-- 

CREATE TABLE `dateferie` (
  `idDateFerie` int(11) NOT NULL auto_increment,
  `nomDateFerie` varchar(255) NOT NULL,
  `jourFerie` int(11) NOT NULL,
  `moisFerie` int(11) NOT NULL,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY  (`idDateFerie`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

-- 
-- Contenu de la table `dateferie`
-- 

INSERT INTO `dateferie` (`idDateFerie`, `nomDateFerie`, `jourFerie`, `moisFerie`, `description`) VALUES 
(1, 'Fête de l''indépendance de Madagascar', 26, 6, 'Fête National, feux d''artifices'),
(2, 'Journée mondiale de la femme', 8, 3, ''),
(3, 'Noël', 25, 12, 'Fête de Noël'),
(4, 'Fête de l''indépendance Américaine', 4, 7, 'Fête Nationale'),
(5, 'Fête nationale française', 14, 7, '');

-- --------------------------------------------------------

-- 
-- Structure de la table `etataressource`
-- 

CREATE TABLE `etataressource` (
  `idEtatARessource` int(11) NOT NULL,
  `nomARessource` varchar(255) NOT NULL,
  `duree` int(11) NOT NULL,
  `cout` float NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- 
-- Contenu de la table `etataressource`
-- 


-- --------------------------------------------------------

-- 
-- Structure de la table `etatressource`
-- 

CREATE TABLE `etatressource` (
  `idEtatRessource` bigint(20) NOT NULL auto_increment,
  `nomRessource` varchar(100) NOT NULL,
  `duree` int(11) NOT NULL,
  `cout` float NOT NULL,
  PRIMARY KEY  (`idEtatRessource`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Contenu de la table `etatressource`
-- 


-- --------------------------------------------------------

-- 
-- Structure de la table `etattache`
-- 

CREATE TABLE `etattache` (
  `idEtatTache` bigint(20) NOT NULL auto_increment,
  `nomTache` varchar(255) NOT NULL,
  `debut` date NOT NULL,
  `fin` date NOT NULL,
  `cout` float NOT NULL,
  PRIMARY KEY  (`idEtatTache`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Contenu de la table `etattache`
-- 


-- --------------------------------------------------------

-- 
-- Structure de la table `poste`
-- 

CREATE TABLE `poste` (
  `idPoste` int(11) NOT NULL auto_increment,
  `nomPoste` varchar(60) NOT NULL,
  `coutPoste` float NOT NULL default '0',
  `description` varchar(255) default NULL,
  PRIMARY KEY  (`idPoste`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

-- 
-- Contenu de la table `poste`
-- 

INSERT INTO `poste` (`idPoste`, `nomPoste`, `coutPoste`, `description`) VALUES 
(2, 'Concepteur', 1500, NULL),
(3, 'Chef de projet', 2000, NULL),
(7, 'Developpeur', 1750, 'Développe des logiciels et sites web'),
(11, 'Coursier', 500, 'Faires les courses'),
(12, 'Assistant technique', 800, 'Assiste dans les domaines techniques'),
(13, 'Chef de maintenance', 1000, 'Assure la maintenance des matériels');

-- --------------------------------------------------------

-- 
-- Structure de la table `profil`
-- 

CREATE TABLE `profil` (
  `idProfil` int(11) NOT NULL auto_increment,
  `login` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `privilege` varchar(100) NOT NULL default 'STDUSER',
  PRIMARY KEY  (`idProfil`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

-- 
-- Contenu de la table `profil`
-- 

INSERT INTO `profil` (`idProfil`, `login`, `password`, `privilege`) VALUES 
(1, 'rmitanjo', 'naruto', 'SYSADMIN'),
(2, 'jerrum', 'bama', 'STDUSER'),
(3, 'hinata', 'hyuuga', 'STDUSER'),
(4, 'icedragon', 'hmyrmidon', 'SYSADMIN'),
(7, 'kotogasy', 'kgp', 'SYSADMIN'),
(8, 'kely', 'rado', 'STDUSER'),
(9, 'tsila', 'sdf', 'STDUSER'),
(10, 'sakura', 'chan', 'STDUSER'),
(11, 'artem', 'mikoyan', 'STDUSER'),
(12, 'shino', 'aburame', 'STDUSER'),
(13, 'kakashi', 'hatake', 'STDUSER'),
(14, 'shibi', 'aburame', 'STDUSER');

-- --------------------------------------------------------

-- 
-- Structure de la table `projet`
-- 

CREATE TABLE `projet` (
  `idProjet` int(11) NOT NULL auto_increment,
  `nomProjet` varchar(255) NOT NULL,
  `debutProjet` date NOT NULL,
  `finProjet` date NOT NULL,
  `description` varchar(255) default NULL,
  `idProfil` int(11) NOT NULL,
  PRIMARY KEY  (`idProjet`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

-- 
-- Contenu de la table `projet`
-- 

INSERT INTO `projet` (`idProjet`, `nomProjet`, `debutProjet`, `finProjet`, `description`, `idProfil`) VALUES 
(1, 'Kronos', '2008-11-05', '2008-11-26', 'Concevoir un logiciel de planification', 13),
(2, 'Aerodyn', '2008-10-01', '2008-10-30', 'Concevoir un logiciel de soufferie aérodynamique', 13),
(3, 'Kronos Gantt Project', '2008-10-00', '2008-11-00', 'Logiciel de gestion de planification', 13),
(4, 'Recursive', '2008-11-23', '2008-11-27', 'Test de recursivité', 13),
(6, 'NP', '2008-11-11', '2008-11-11', NULL, 3);

-- --------------------------------------------------------

-- 
-- Structure de la table `ressource`
-- 

CREATE TABLE `ressource` (
  `idRessource` int(11) NOT NULL auto_increment,
  `idPoste` int(11) NOT NULL,
  `pseudo` varchar(30) NOT NULL,
  PRIMARY KEY  (`idRessource`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

-- 
-- Contenu de la table `ressource`
-- 

INSERT INTO `ressource` (`idRessource`, `idPoste`, `pseudo`) VALUES 
(1, 2, 'MITA'),
(4, 7, 'JO'),
(3, 3, 'JRM'),
(2, 2, 'HINATA'),
(9, 3, 'TOVOH'),
(18, 7, 'MOX'),
(19, 7, 'KOTOGASY');

-- --------------------------------------------------------

-- 
-- Structure de la table `ressource_tache`
-- 

CREATE TABLE `ressource_tache` (
  `idRessource` int(11) NOT NULL,
  `idTache` int(11) NOT NULL,
  PRIMARY KEY  (`idRessource`,`idTache`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- 
-- Contenu de la table `ressource_tache`
-- 

INSERT INTO `ressource_tache` (`idRessource`, `idTache`) VALUES 
(1, 19),
(1, 35),
(1, 39),
(1, 44),
(1, 65),
(2, 19),
(2, 35),
(2, 39),
(2, 44),
(2, 64);

-- --------------------------------------------------------

-- 
-- Structure de la table `tache`
-- 

CREATE TABLE `tache` (
  `idTache` int(11) NOT NULL auto_increment,
  `idProjet` int(11) NOT NULL,
  `nomTache` varchar(255) NOT NULL,
  `debutTache` datetime NOT NULL default '2008-01-01 08:00:00',
  `finTache` datetime NOT NULL default '2008-01-01 09:00:00',
  `pred` int(11) NOT NULL,
  `niveau` int(11) NOT NULL,
  `etat` tinyint(1) NOT NULL,
  `idComparaison` int(11) NOT NULL default '0',
  PRIMARY KEY  (`idTache`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=81 ;

-- 
-- Contenu de la table `tache`
-- 

INSERT INTO `tache` (`idTache`, `idProjet`, `nomTache`, `debutTache`, `finTache`, `pred`, `niveau`, `etat`, `idComparaison`) VALUES 
(1, 1, 'racine', '2008-01-01 08:00:00', '2008-01-01 18:00:00', 1, 1, 0, 0),
(2, 1, 'A', '2008-01-01 08:00:00', '2008-01-01 18:00:00', 1, 2, 0, 0),
(3, 1, 'W', '2008-01-01 08:00:00', '2008-01-01 18:00:00', 2, 3, 0, 0),
(4, 1, 'X', '2008-01-01 08:00:00', '2008-01-01 18:00:00', 2, 3, 0, 0),
(5, 1, 'B', '2008-01-01 08:00:00', '2008-01-01 18:00:00', 1, 2, 0, 0),
(6, 1, 'F', '2008-01-01 08:00:00', '2008-01-01 18:00:00', 5, 3, 0, 0),
(7, 1, 'C', '2008-01-01 08:00:00', '2008-01-01 18:00:00', 1, 2, 0, 0),
(8, 1, 'S', '2008-01-01 08:00:00', '2008-01-01 18:00:00', 7, 3, 0, 0),
(9, 1, 'T', '2008-01-01 08:00:00', '2008-01-01 18:00:00', 7, 3, 0, 0),
(10, 1, 'U', '2008-01-01 08:00:00', '2008-01-01 18:00:00', 7, 3, 0, 0),
(11, 1, 'D', '2008-01-01 08:00:00', '2008-01-01 18:00:00', 4, 4, 0, 0),
(12, 1, 'E', '2008-01-01 08:00:00', '2008-01-01 18:00:00', 4, 4, 0, 0),
(13, 1, 'I', '2008-01-01 08:00:00', '2008-01-01 18:00:00', 9, 4, 0, 0),
(14, 1, 'J', '2008-01-01 08:00:00', '2008-01-01 18:00:00', 9, 4, 0, 0),
(15, 1, 'H', '2008-01-01 08:00:00', '2008-01-01 18:00:00', 11, 5, 0, 0),
(16, 1, 'M', '2008-01-01 08:00:00', '2008-01-01 18:00:00', 13, 5, 0, 0),
(17, 1, 'N', '2008-01-01 08:00:00', '2008-01-01 18:00:00', 13, 5, 0, 0),
(18, 1, 'O', '2008-01-01 08:00:00', '2008-01-01 18:00:00', 13, 5, 0, 0),
(19, 3, 'racine', '2008-11-01 00:00:00', '2008-11-30 00:00:00', 2, 1, 0, 0),
(35, 3, 'Programmation', '2008-11-12 00:00:00', '2008-11-25 00:00:00', 19, 2, 0, 0),
(66, 3, 'UML', '2008-11-27 00:00:00', '2008-11-30 00:00:00', 0, 3, 0, 0),
(38, 3, 'Langage', '2008-11-12 00:00:00', '2008-11-15 00:00:00', 35, 3, 0, 0),
(39, 3, 'Editeur', '2008-11-13 00:00:00', '2008-11-25 00:00:00', 35, 3, 0, 0),
(40, 3, 'UML', '2008-11-25 00:00:00', '2008-11-27 00:00:00', 36, 3, 0, 0),
(41, 3, 'Merise', '2008-11-27 00:00:00', '2008-11-30 00:00:00', 36, 3, 0, 0),
(44, 3, 'NetBeans', '2008-11-13 00:00:00', '2008-11-16 00:00:00', 39, 4, 1, 0),
(45, 3, 'Java', '2008-11-12 00:00:00', '2008-11-13 00:00:00', 38, 4, 0, 0),
(46, 3, '2D', '2008-10-10 00:00:00', '2008-10-01 00:00:00', 42, 4, 0, 0),
(47, 3, '3D', '2008-10-10 00:00:00', '2008-10-01 00:00:00', 42, 4, 0, 0),
(55, 3, 'Eclipse', '2008-11-16 00:00:00', '2008-11-20 00:00:00', 39, 4, 0, 0),
(65, 3, 'CanvasEngine', '2008-11-13 00:00:00', '2008-11-14 00:00:00', 44, 5, 0, 0),
(68, 3, 'Historique', '2008-10-15 00:00:00', '2008-10-15 00:00:00', 67, 3, 0, 0),
(64, 3, 'UML Editor', '2008-11-13 00:00:00', '2008-11-16 00:00:00', 44, 5, 0, 0),
(63, 3, 'JCreator', '2008-11-20 00:00:00', '2008-11-25 00:00:00', 39, 4, 0, 0),
(69, 3, 'Architecture', '2008-11-16 00:00:00', '2008-11-17 00:00:00', 67, 3, 0, 0),
(70, 3, 'Caractéristiques', '2008-11-11 00:00:00', '2008-11-12 00:00:00', 67, 3, 0, 0),
(71, 3, 'Hibernate', '2008-11-11 00:00:00', '2008-11-12 00:00:00', 67, 3, 0, 0),
(72, 3, 'Le logiciel', '2008-11-15 00:00:00', '2008-11-15 00:00:00', 67, 3, 0, 0),
(73, 3, 'MySql', '2008-12-11 00:00:00', '2008-12-11 00:00:00', 67, 3, 0, 0),
(74, 3, 'Java', '2008-11-11 00:00:00', '2005-11-11 00:00:00', 67, 3, 0, 0),
(75, 2, 'racine', '2008-01-01 08:00:00', '2008-01-01 09:00:00', 75, 1, 0, 0),
(76, 4, 'Recursive', '2008-01-01 08:00:00', '2008-01-01 09:00:00', 4, 1, 0, 0),
(78, 6, 'NP', '2008-11-11 00:00:00', '2008-11-11 00:00:00', 0, 1, 0, 0),
(79, 2, 'essai', '2008-10-10 00:00:00', '2008-10-11 00:00:00', 75, 2, 0, 0),
(80, 3, 'HQL', '2008-11-13 00:00:00', '2008-11-15 00:00:00', 38, 4, 0, 0);

-- --------------------------------------------------------

-- 
-- Structure de la table `tcomparable`
-- 

CREATE TABLE `tcomparable` (
  `idTComparable` int(11) NOT NULL auto_increment,
  `idProjet` int(11) NOT NULL,
  `idTache` int(11) NOT NULL,
  `valeurId` int(11) NOT NULL,
  PRIMARY KEY  (`idTComparable`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

-- 
-- Contenu de la table `tcomparable`
-- 

INSERT INTO `tcomparable` (`idTComparable`, `idProjet`, `idTache`, `valeurId`) VALUES 
(1, 3, 1, 1),
(2, 3, 10, 10),
(4, 3, 3, 3),
(5, 3, 19, 19),
(6, 3, 6, 6);
